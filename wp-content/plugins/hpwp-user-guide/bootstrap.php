<?php
/**
 * Plugin Name: HPWP User Guide
 * Plugin URI: http://hpwpconsulting.com
 * Description: This plugin adds documentation and helpful tips for users working with the HPWP site.
 * Version: 1.0.0
 * Author: Gorirra Consulting
 * License: GPL2
 */

 // prevent direct access to this file.
if(!defined('ABSPATH'))
	die("403 Forbidden");

 /* ---------- PLUGIN CONSTANTS ---------- */
 define( 'HPWP_USER_GUIDE_DIR', plugin_dir_path(__FILE__) );
 define( 'HPWP_USER_GUIDE_URL', plugins_url() . sprintf( '/%s/', basename(__DIR__) ) );

 /* ---------- PLUGIN DEPENDENCIES ---------- */
 require( 'inc/hpwp-user-guide-plugin.php' );
 add_action( 'init', array('hpwp_user_guide_plugin', 'singleton') );

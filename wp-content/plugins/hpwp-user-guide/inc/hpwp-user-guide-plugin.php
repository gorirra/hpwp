<?php

class HPWP_User_Guide_Plugin {

	static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	public function enqueue_admin_scripts() {

		$screen = get_current_screen();

		if( false !== strpos( $screen->base, 'user-guide' ) ) {
			wp_enqueue_style( 'hpwp-user-guide', HPWP_USER_GUIDE_URL .'css/style.min.css', false, false);
		}
	}

	/**
	* Add Admin Menu
	*
	* Add "User Guide" menu page to admin navigation.
	*/
	public function add_admin_menu() {

		$options_page =
			add_menu_page(
				'HPWP User Guide',
				'User Guide',
				'edit_others_posts',
				'hpwp-user-guide',
				array($this, 'generate_options_page'),
				'dashicons-book-alt'
			);

		add_submenu_page(
			'hpwp-user-guide',
			'Table of Contents',
			'Table of Contents',
			'edit_others_posts',
			'hpwp-user-guide',
			array($this, 'generate_options_page')
		);

		add_submenu_page(
			'hpwp-user-guide',
			'Site Options',
			'Site Options',
			'edit_others_posts',
			'site-options',
			array($this, 'generate_options_page')
		);

		add_submenu_page(
			'hpwp-user-guide',
			'Pages/Templates',
			'Pages/Templates',
			'edit_others_posts',
			'pages-templates',
			array($this, 'generate_options_page')
		);

		add_submenu_page(
			'hpwp-user-guide',
			'Using Custom Shortcodes',
			'Using Custom Shortcodes',
			'edit_others_posts',
			'using-custom-shortcodes',
			array($this, 'generate_options_page')
		);

		add_submenu_page(
			'hpwp-user-guide',
			'Custom Post Types',
			'Custom Post Types',
			'edit_others_posts',
			'custom-post-types',
			array($this, 'generate_options_page')
		);

	}

	/**
	* Generate Options Page
	*
	* Dynamically generates the theme options page.
	*/
	public function generate_options_page() {

		$active_tab = (isset($_GET["page"]) ? $_GET["page"] : 'table-of-contents');

		if( 'hpwp-user-guide' == $active_tab ) {
			$active_tab = 'table-of-contents';
		}

		require(HPWP_USER_GUIDE_DIR .'/inc/views/hpwp-user-guide.php');
	}

	/**
	* Singleton
	*
	* @return A single instance of the current class.
	*/
	public static function singleton() {

		if( !self::$instance )
		self::$instance = new self();

		return self::$instance;
	}

	/**
	* Add Actions
	*
	* Registers all the WordPress actions and hooks the plugin relies on.
	*/
	private function _add_actions() {

		// Enqueue Admin Stylesheet
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Register "HPWP User Guide" menu item
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );

	}
}

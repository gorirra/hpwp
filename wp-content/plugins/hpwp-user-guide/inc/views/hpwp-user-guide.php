<div class="wrap user-guide">

	<h1>HPWP User Guide</h1>

	<h2 class="nav-tab-wrapper">

		<a href="<?php echo admin_url('admin.php?page=hpwp-user-guide') ?>" class="nav-tab <?php echo ($active_tab === 'table-of-contents' ? 'nav-tab-active' : '') ?> "><?php _e('Table of Contents', 'hpwp'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=site-options') ?>" class="nav-tab <?php echo ($active_tab == 'site-options' ? 'nav-tab-active' : ''); ?>"><?php _e('Site Options', 'hpwp'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=pages-templates') ?>" class="nav-tab <?php echo ($active_tab == 'pages-templates' ? 'nav-tab-active' : ''); ?>"><?php _e('Pages/Templates', 'hpwp'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=using-custom-shortcodes') ?>" class="nav-tab <?php echo ($active_tab == 'using-custom-shortcodes' ? 'nav-tab-active' : ''); ?>"><?php _e('Using Custom Shortcodes', 'hpwp'); ?></a>
		<a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>" class="nav-tab <?php echo ($active_tab == 'custom-post-types' ? 'nav-tab-active' : ''); ?>"><?php _e('Custom Post Types', 'hpwp'); ?></a>

	</h2>

	<div class="user-guide__content">
		<?php require(HPWP_USER_GUIDE_DIR ."inc/views/{$active_tab}.php"); ?>
	</div>

</div>

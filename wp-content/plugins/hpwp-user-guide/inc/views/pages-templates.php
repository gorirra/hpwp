<?php
	/**
	 * site options
	 */
?>
<h2 class="user-guide__heading">Pages/Templates</h2>

<p>Nearly every piece of content (text/images) can be modified in the admin (Please let Gorirra Consulting know if there's something that isn't quite modifiable). Simply edit the page you'd like to modify and scroll beneath the main text editor to see the various file uploads and text editors and input fields. If you are unsure about something, just ask us and we'll be glad to help!</p>

<h3 class="user-guide__section-heading">Every Page</h3>

<ul class="user-guide__list">
	<li><strong>HPWP Page Hero Options:</strong> Set up individual page hero images and text.</li>
	<li>Image dimensions are listed: 1920 x 600 pixels for desktop and 640 x 450 pixels for mobile.</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Home Page</h3>

<ul class="user-guide__list">
	<li><strong>"What We Do?" Title:</strong> Changes the text immediately underneath the hero. Defaults to <em>"What is HPWP?"</em></li>
	<li><strong>Specialty Group:</strong> Add one for each of the pillars HPWP stands for. Include a short description, and an image (preferably SVG).</li>
	<!-- <li><strong>YouTube Video ID:</strong> Enter only the ID (the part after the '?v=' of the URL) of the featured video.</li> -->
	<li><strong>HPWP Results:</strong> Text that will appear to the right of the results image. Feel free to use <code>&lt;h3&gt;</code> tags and bullets as you wish.</li>
	<li><strong>HPWP Results Image:</strong> Image that will appear to the left of the Results content.</li>
	<li><strong>"Featured Testimonials" Title and Text:</strong> Handled in the <a href="<?php echo admin_url('admin.php?page=hpwp_options') ?>">site options page</a>.</li>
	<li><strong>Testimonial Carousel:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=testimonials') ?>">"Testimonials" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<li><strong>"Team" Title:</strong> Changes the text for the team slider section. Defaults to <em>"Team"</em></li>
	<li><strong>Team Carousel:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=team') ?>">"Team Members" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<li><strong>Bottom Call to Action Background Image:</strong> Upload a hero-like image that serves as the background to the CTA at the bottom of this page. Dimensions: 1920 x 400 pixels.</li>
	<li><strong>Bottom Call to Action Text:</strong> Enter the text for the call to action. Use <code>&lt;h2&gt;</code> tags and the <code>[button]</code> shortcode to make it impactful.</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">About Page</h3>

<ul class="user-guide__list">
	<!-- <li><strong>"Our Team" Title:</strong> Changes the text immediately underneath the hero. Defaults to <em>"Our Team"</em>.</li> -->
	<li><strong>Objective Group:</strong> Add one for each of the objectives for the HPWP team. Include a short description.</li>
	<li><strong>Team Member Tiles:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=team') ?>">"Team Members" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Book Page</h3>
<p>Coming soon.</p>

<?php /*
<ul class="user-guide__list">
	<li><strong>Main Content Editor:</strong> More descriptive content about the book, not mentioned in the hero area.</li>
	<li><strong>Book Cover:</strong> Image of the book cover, PNG file format preferred. Preferably a height no more than 530 pixels.</li>
	<li><strong>Book Stylized Text:</strong> Fancy typographical image, preferably made by a designer. SVG file format preferred.</li>
	<li><strong>Book Excerpt - PDF:</strong> Upload a PDF file here.</li>
	<li><strong>Book Download CTA:</strong> Text for download button CTA, i.e. <em>"Download an Excerpt"</em>.</li>
	<li><strong>Book Form Title:</strong> The title that appears above the form for folks to get news related to the book.</li>
	<li><strong>Form Shortcode:</strong> Enter the Gravity Form shortcode*. Currently will submit to an admin email. If this needs to change, please contact Gorirra Consulting.</li>
</ul>

*/ ?>
<p>***</p>

<h3 class="user-guide__section-heading">Charity Page</h3>

<p>This is a standard page with no special fields (save for the hero options). You can use the <code>[narrow]</code> shortcode to make the text easier to read.</p>

<p>***</p>

<h3 class="user-guide__section-heading">Clients Page</h3>

<ul class="user-guide__list">
	<li><strong>Main Editor:</strong> For the bulleted list, create a simple unordered list. The website styling will automatically make it orange and arranged in a column-like layout.</li>
	<li><strong>Client Tiles:</strong> Add a logo for each client you'd like to display. Dimensions: 100x100 pixels.</li>
	<li><strong>Case Study Module Title:</strong> Title for the case studies module. Defaults to <em>"Download a Case Study"</em>.</li>
	<li><strong>Case Study Tiles:</strong> Enter the name, a subtitle, the description, and a PDF to download.</li>
	<li><strong>"Featured Testimonials" Title and Text:</strong> Handled in the <a href="<?php echo admin_url('admin.php?page=hpwp_options') ?>">site options page</a>.</li>
	<li><strong>Testimonial Logo Carousel:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=testimonials') ?>">"Testimonials" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Contact Page</h3>

<p>This is a templated page with no special fields (save for the hero options).</p>

<p>***</p>

<h3 class="user-guide__section-heading">Events Page</h3>

<ul class="user-guide__list">
	<li><strong>Left Content:</strong> Text that will be displayed in the left column underneath the hero. Use the <code>[emphasis]</code> shortcode to add some flair.</li>
	<li><strong>Right Content:</strong> Text that will be displayed in the right (blue) column underneath the hero. Feel free to add hyperlinks to agenda PDF's.</li>
	<li><strong>Event Dates:</strong> Title for the dates section. Defaults to <em>"Event Dates"</em>.</li>
	<li><strong>Event Date Tiles:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=event') ?>">"Events" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<li><strong>Testimonials Section Title and Text:</strong> Handled in the <a href="<?php echo admin_url('admin.php?page=hpwp_options') ?>">site options page</a>.</li>
	<li><strong>Testimonials Carousel:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=testimonials') ?>">"Testimonials" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Legal Page</h3>

<p>This is a standard page with no special fields. Do not enter content for the hero options as this page should not display it.</p>

<p>***</p>

<h3 class="user-guide__section-heading">Press Page</h3>

<ul class="user-guide__list">
	<li><strong>Logos:</strong> Add multiple logs that will appear in the "As Seen in" carousel. Upload an image (100 x 100 pixels preferred) and the company name. Click <em>"Add Another Logo"</em> to add, <em>"Remove Logo"</em> to remove, and the various up and down caret arrows to rearrange their order.</li>
	<li><strong>Press Tiles:</strong> These tiles are managed through the "Press" menu (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<li><strong>Press Contact Title:</strong> Title for the press contact section. Defaults to <em>"Press Contact"</em>.</li>
	<li><strong>Press Contact Text:</strong> Freeform editor to add text. No need to center it here, the site will take care of it. Manually add point person and contact info.</li>
</ul>

<p>***</p>

<h3 class="user-guide__section-heading">Solutions Page</h3>

<ul class="user-guide__list">
	<li><strong>Solution 1 - Title:</strong> Title for the first solution. Use the <code>[strong]</code> shortcode for more impact.</li>
	<li><strong>Solution 1 - Left Content:</strong> Content that appears in the left column for the first solution. Use the <code>[emphasis]</code> shortcode for better emphasis.</li>
	<li><strong>Solution 1 - Right Content:</strong> Content that appears in the right (blue) column for the first solution. Use bullets for easier readability.</li>
	<li><strong>CTA - Left:</strong> Text for the left CTA underneath the first solution. Use <code>&lt;h3&gt;</code> tags for the main text, then the <code>[button]</code> shortcode for the button.</li>
	<li><strong>CTA - Right:</strong> Text for the right CTA underneath the first solution. Use <code>&lt;h3&gt;</code> tags for the main text, then the <code>[button]</code> shortcode for the button.</li>
	<li><strong>Event Date Tiles:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=event') ?>">"Events" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<!-- <li><strong>Mid-page Image:</strong> Upload an image that serves as a separator between the two solutions. Preferred dimensions: 1920 x 500 pixels.</li> -->
	<li><strong>"Featured Testimonials" Title and Text:</strong> Handled in the <a href="<?php echo admin_url('admin.php?page=hpwp_options') ?>">site options page</a>.</li>
	<li><strong>Testimonial Logo Carousel:</strong> These tiles are managed through the <a href="<?php echo admin_url('edit.php?post_type=testimonials') ?>">"Testimonials" menu</a> (see <a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>">Custom Post Types</a>).</li>
	<li><strong>Solution 2 - Title:</strong> Title for the second solution. Use the <code>[strong]</code> shortcode for more impact</li>
	<li><strong>Solution 2 - Left Content:</strong> Content that appears in the left column for the second solution. Use the <code>[emphasis]</code> shortcode for better emphasis.</li>
	<li><strong>Solution 2 - Right Content:</strong> Content that appears in the right (blue) column for the second solution. Use bullets for easier readability.</li>
	<li><strong>Contact Form - Title:</strong> Title for the contact form. Defaults to <em>"Let us know how we can help"</em>.</li>
	<li><strong>Form Shortcode:</strong> Enter the Gravity Form shortcode*. Currently will submit to an admin email. If this needs to change, please contact Gorirra Consulting.</li>
</ul>

<p><em>* Gravity Forms should be created by Gorirra Consulting as there are many options to make it a) look proper and b) actually post to an email properly.</em></p>
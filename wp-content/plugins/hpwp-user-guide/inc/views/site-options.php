<?php
	/**
	 * site options
	 */
?>
<h2 class="user-guide__heading">Site Options <a class="page-title-action" href="<?php echo admin_url('admin.php?page=hpwp_options') ?>">Link</a></h2>

<p>Global site options are stored here.</p>

<ul class="user-guide__list">
	<li><strong>Logos:</strong> Upload site logos, preferably in SVG format. Header Logo is the top logo, positioned in the nav. Footer Logo is in the... footer.</li>
	<li><strong>Social Media:</strong> Enter the URL's for the various social media websites. It will appear in the top nav, the bottom footer and the contact page (sidebar). If you leave one blank, it will not appear on the site.</li>
	<li><strong>Company Info:</strong> Enter address, email, phone number information. Also the legal page URL.</li>
	<li><strong>Register Pages:</strong> For the register landing pages. Upload the hero image, add the page title, and enter the Gravity Form shortcode (will currently push the submitted email to an admin email).*</li>
	<li><strong>Module Titles:</strong> Enter the various titles (and content text) that will appear throughout the various modules on the site.</li>
	<li><strong>Contact Form:</strong> Enter the Gravity Form shortcode.* Currently will push the submitted email to an admin email.</li>
	<li><strong>Constant Contact:</strong> Enter the Gravity Form shortcode.* Currently will push the submitted email into the "Website Contacts" list in Constant Contact.</li>
</ul>

<p><em>* Gravity Forms should be created by Gorirra Consulting as there are many options to make it a) look proper and b) actually post to an email properly. Please let us know if the endpoints ever need to be changed.</em></p>

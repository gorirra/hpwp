<?php
	/**
	 * table of contents
	 */
?>

<h2 class="user-guide__heading">Table of Contents</h2>

<ul class="user-guide__list user-guide__list_name_table-of-contents">
	<li>
		<a href="<?php echo admin_url('admin.php?page=site-options') ?>" target="_self">Site Options</a>

		<!-- <ul class="user-guide__list__sections">
			<li><a href="<?php echo admin_url('admin.php?page=site-options') ?>" target="_self">Global Settings</a></li>
		</ul> -->

	</li>
	<li>
		<a href="<?php echo admin_url('admin.php?page=pages-templates') ?>" target="_self">Pages/Templates</a>
	</li>
	<li>
		<a href="<?php echo admin_url('admin.php?page=using-custom-shortcodes') ?>" target="_self">Using Custom Shortcodes</a>
	</li>
	<li>
		<a href="<?php echo admin_url('admin.php?page=custom-post-types') ?>" target="_self">Custom Post Types</a>
	</li>
</ul>

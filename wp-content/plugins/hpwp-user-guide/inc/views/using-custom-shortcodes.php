<?php
	/**
	 * shortcodes
	 */
?>
<h2 class="user-guide__heading">Using Custom Shortcodes</h2>

<h3 class="user-guide__section-heading">YouTube</h3>

<p>The youtube shortcode allows you to embed YouTube videos inside WordPress pages/posts.</p>

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>id</td>
			<td>The id of the YouTube video</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[youtube id="Q0CbN8sfihY"]</pre>

<p>***</p>

<h3 class="user-guide__section-heading">Buttons (CTA's)</h3>

<p>The button shortcode allows you to create high buttons that can link to other pages/files or even external pages.</p>

<table class="user-guide__table">
	<thead>
		<tr>
			<th>Option</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>url</td>
			<td>The URL of the destination you want the user to go to.</td>
		</tr>
		<tr>
			<td>text</td>
			<td>The button text.</td>
		</tr>
		<tr>
			<td>color</td>
			<td>Optional. The color of the button. Currently, only gray (default) and orange.</td>
		</tr>
		<tr>
			<td>blank</td>
			<td>Optional. Enter <code>0</code> (default, open in same window) or <code>1</code> (open in new window)</td>
		</tr>
		<tr>
			<td>size</td>
			<td>Optional. Enter "large" for a slightly larger button.</td>
		</tr>
	</tbody>
</table>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[button url="http://google.com" text="Google"( color="orange" blank="1" size="large")]</pre>

<p>***</p>


<h3 class="user-guide__section-heading">Narrow</h3>

<p>The narrow shortcode will wrap your content around a narrow div that'll keep it slimmer than full width.</p>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[narrow]
	this is my text that will be in a narrow container.
[/narrow]</pre>

<p>***</p>


<h3 class="user-guide__section-heading">Emphasis</h3>

<p>The emphasis shortcode makes your text light blue, slightly bigger, and italicized.</p>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[emphasis]This is my text (note that it's all inline).[/emphasis]</pre>

<p>***</p>


<h3 class="user-guide__section-heading">Strong</h3>

<p>The emphasis shortcode makes your text bold.</p>

<h4 class="user-guide__sub-heading">Example</h4>
<pre>[strong]This text is strong![/strong]</pre>
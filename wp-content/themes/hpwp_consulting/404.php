<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>

    <div class="content_container">
      <div class="content_one_column">
        <h1 class="center">Page Not Found</h1>
      </div>
      <div class="bottom_nav">
        <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
      </div>
    </div>

<?php get_footer(); ?>
<?php
/*
  Template Name: Alumni Downloads
*/

get_header(); ?>
    <div class="content_container">
      <?php
        if( is_user_logged_in() ) {
          get_sidebar(); ?>
          <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <h1>[ <?php the_title(); ?> ]</h1>
              <div class="entry">
                <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            <?php endwhile; endif; ?>
            <?php
              $args = array(
                'post_type' => 'download'
              );
              $original_query = $wp_query;
              $wp_query = NULL;
              $cache_name = 'download_cache_name';
              $wp_query = wp_cache_get( $cache_name );
              if ($wp_query == TRUE) {
                if ($wp_query->have_posts() === false) {
                  $wp_query = new WP_Query($args);
                  wp_cache_replace( $cache_name, $wp_query, '', 600 );
                }
              } else {
                $wp_query = new WP_Query($args);
                wp_cache_set( $cache_name, $wp_query, '', 600 );
              }

              if ( $wp_query->have_posts()) {
                while ($wp_query->have_posts()) : $wp_query->the_post();
                  $date = get_post_meta( get_the_ID(), '_date', true );
                  $speakers = get_post_meta( get_the_ID(), '_speakers', true );
                  $summary = get_post_meta( get_the_ID(), '_summary', true );
                  $keypoint_summary = get_post_meta( get_the_ID(), '_keypoint_summary', true );
                  $extra_pdf_text = get_post_meta( get_the_ID(), '_extra_pdf_text', true );
                  $extra_pdf = get_post_meta( get_the_ID(), '_extra_pdf', true );
                  $recording = get_post_meta( get_the_ID(), '_recording', true );
                  $purchase_link_text = get_post_meta( get_the_ID(), '_purchase_link_text', true );
                  $purchase_link = get_post_meta( get_the_ID(), '_purchase', true );

                  echo '
                    <div class="download_group">
                      <p><strong class="blue">'.get_the_title().'</strong></p>
                      <p><strong class="green">'.$date.'</strong></p>
                      <p><strong>Speakers:</strong> '.$speakers.'</p>
                      <p><strong>Summary:</strong> '.$summary.'</p>
                      <p><strong>Downloads:</strong></p>
                  ';
                  if ($keypoint_summary) {
                    echo '<img alt="PDF Icon" src="'.get_bloginfo("stylesheet_directory").'/images/pdf-icon.png" /> <a href="'.$keypoint_summary.'" target="_blank">Keypoint Summary</a><br />';
                  }
                  if ($extra_pdf) {
                    echo '<img alt="PDF Icon" src="'.get_bloginfo("stylesheet_directory").'/images/pdf-icon.png" /> <a href="'.$extra_pdf.'" target="_blank">'.$extra_pdf_text.'</a><br />';
                  }
                  if ($recording) {
                    echo '<img alt="Recording Icon" src="'.get_bloginfo("stylesheet_directory").'/images/recording.png" /> <a href="'.$recording.'" target="_blank">Recording</a><br />';
                  }
                  if ($purchase_link) {
                    echo '<img alt="Hand Cursor" src="'.get_bloginfo("stylesheet_directory").'/images/hand.png" /> <a href="'.$purchase_link.'" target="_blank">'.$purchase_link_text.'</a>';
                  }
                  echo '</div>';
                endwhile;
              }
              $wp_query  = NULL;
              $wp_query = $original_query;
              wp_reset_postdata();
            ?>
            </div>
          </div>
      <?php } else {
          echo '<div class="content"><div class="entry">';
            wp_login_form();
            echo '<p>';
            wp_register('', '');
            echo ' | <a href="'. wp_lostpassword_url() .'" title="Lost Password">Lost Password</a>';
            echo '</p>';
          echo '</div></div>';
        } ?>
      <div class="bottom_nav">
        <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
      </div>
    </div>
<?php get_footer(); ?>



<?php
/*
  Template Name: Alumni
*/

get_header(); ?>
    <div class="content_container">
      <?php
        if( is_user_logged_in() ) {
          get_sidebar('alumni'); ?>
          <div class="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="entry">
              <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
            </div>
            <?php endwhile; endif;
          echo '</div>';
        } else {
          echo '<div class="content"><div class="entry">';
            wp_login_form();
            echo '<p>';
            wp_register('', '');
            echo ' | <a href="'. wp_lostpassword_url() .'" title="Lost Password">Lost Password</a>';
            echo '</p>';
          echo '</div></div>';
        }
      ?>
      <div class="bottom_nav">
        <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
      </div>
    </div>
<?php get_footer(); ?>

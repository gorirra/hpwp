<?php
/*
  Template Name: Events
*/

get_header(); ?>
    <div class="content_container">
      <?php get_sidebar(); ?>
      <div class="content">
        <?php
          $query = new WP_Query( 'category_name=events' );
        ?>
        <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
        <div class="post">
          <h1 class="post_title">[ <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a> ]</h1>
          <?php the_excerpt(); ?>
          <p><a href="<?php echo get_permalink(); ?>">Read More...</a></p>
        </div>
        <?php endwhile; endif; ?>
      </div>
      <div class="bottom_nav">
        <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
      </div>
    </div>
<?php get_footer(); ?>

<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

    <div class="footer" role="contentinfo">
      <p>&copy; 2010-<?php echo date('Y'); ?> HPWP Consulting, LLC. All Rights Reserved.</p>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>

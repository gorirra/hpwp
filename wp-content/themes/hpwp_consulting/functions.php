<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
add_theme_support( 'post-thumbnails' );

function hpwp_enqueue_scripts() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', 'http://code.jquery.com/jquery-1.9.1.js');
  wp_enqueue_script('jquery');
  wp_deregister_script('jquery-ui-core');
  wp_register_script('jquery-ui-core', 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');
  wp_enqueue_script('jquery-ui-core');
  wp_register_script('jquery-ui-accordion', '/wp-includes/js/jquery/ui/jquery.ui.accordion.min.js');
  wp_enqueue_script('jquery-ui-accordion');
}
add_action ('wp_enqueue_scripts', 'hpwp_enqueue_scripts' );
function hpwp_head() {

  echo '<script type="text/javascript">
  jQuery(function() {
    jQuery("#accordion").accordion({
      heightStyle: "content"
    });
  });

  </script>';
}
add_action('wp_head', 'hpwp_head' );


if ( function_exists('register_sidebar') ) {
  register_sidebar(array('name'=>'Home Sidebar'));
}
register_sidebar(array('name'=>'custom widget',
  'before_widget' => '',
  'after_widget' => '',
  'before_title' => '<span style="display:none;">',
  'after_title' => '</span>',
));
register_sidebar(array('name'=>'Blog Sidebar',
  'before_widget' => '',
  'after_widget' => '',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
));

// HPWP META BOXES
  add_action( 'add_meta_boxes', 'hpwp_add_post_meta_boxes' );
  add_action( 'save_post', 'hpwp_save_post_class_meta', 10, 2 );

  function hpwp_add_post_meta_boxes() {
    add_meta_box(
      'hpwp',      // Unique ID
      esc_html__( 'Page Options', 'example' ),    // Title
      'hpwp_post_class_meta_box',   // Callback function
      'page',         // Admin page (or post type)
      'normal',         // Context
      'default'         // Priority
    );
  }
  /* Display the post meta box. */
  function hpwp_post_class_meta_box( $object, $box ) {
    wp_register_script('my-uploader', get_bloginfo('template_directory') . '/js/uploader.js', array('jquery','media-upload','thickbox'));
    wp_enqueue_script('my-uploader');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_nonce_field( basename( __FILE__ ), 'hpwp_post_class_nonce' ); ?>
    <p>
      <label for="hpwp_featured_quote"><?php _e( "Featured Quote", 'example' ); ?></label>
      <br />
      <textarea class="widefat" rows="5" cols="80" name="hpwp_featured_quote" id="hpwp_featured_quote"><?php echo esc_attr( get_post_meta( $object->ID, '_hpwp_featured_quote', true ) ); ?></textarea>
    </p>
    <p>
      <label for="hpwp_extra_text"><?php _e( "Extra Text", 'example' ); ?></label>
      <br />
      <textarea class="widefat" rows="5" cols="80" name="hpwp_extra_text" id="hpwp_extra_text"><?php echo esc_attr( get_post_meta( $object->ID, '_hpwp_extra_text', true ) ); ?></textarea>
    </p>
    <p>
      <input class="button" name="extra_featured_image_button" id="extra_featured_image_button" value="Choose Extra Featured Image" />
      <input type="text" size="75" id="extra_featured_image" name="extra_featured_image" value="<?php echo get_post_meta($object->ID, '_extra_featured_image', true); ?>" class="large-text" />
    </p>
    <?php
    $checked = get_post_meta( $object->ID, '_hpwp_overflow', true ); 
    ?>
    <p>
      <input style="margin-right:20px;" type="checkbox" name="hpwp_overflow" id="hpwp_overflow" <?php checked( $checked, 'on' ); ?> />
      <label for="hpwp_overflow"><?php _e( "Make Extra Featured Image Overflow?", 'example' ); ?></label>
    </p>
  <?php }
  add_action( 'save_post', 'hpwp_save_post_class_meta', 10, 2 );

  /* Save the meta box's post metadata. */
  function hpwp_save_post_class_meta( $post_id, $post ) {

    /* Verify the nonce before proceeding. */
    if ( !isset( $_POST['hpwp_post_class_nonce'] ) || !wp_verify_nonce( $_POST['hpwp_post_class_nonce'], basename( __FILE__ ) ) )
      return $post_id;

    /* Get the post type object. */
    $post_type = get_post_type_object( $post->post_type );

    //
    $new_meta_value = ( isset( $_POST['hpwp_featured_quote'] ) ? $_POST['hpwp_featured_quote'] : '' );
    $meta_value = get_post_meta( $post_id, '_hpwp_featured_quote', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_hpwp_featured_quote', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_hpwp_featured_quote', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_hpwp_featured_quote', $meta_value );
    //
    $new_meta_value = ( isset( $_POST['hpwp_extra_text'] ) ? $_POST['hpwp_extra_text'] : '' );
    $meta_value = get_post_meta( $post_id, '_hpwp_extra_text', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_hpwp_extra_text', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_hpwp_extra_text', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_hpwp_extra_text', $meta_value );
    //
    $new_meta_value = ( isset( $_POST['extra_featured_image'] ) ? $_POST['extra_featured_image'] : '' );
    $meta_value = get_post_meta( $post_id, '_extra_featured_image', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_extra_featured_image', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_extra_featured_image', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_extra_featured_image', $meta_value );
    //
    $new_meta_value = ( isset( $_POST['hpwp_overflow'] ) ? $_POST['hpwp_overflow'] : '' );
    $meta_value = get_post_meta( $post_id, '_hpwp_overflow', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_hpwp_overflow', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_hpwp_overflow', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_hpwp_overflow', $meta_value );
  }
// END HPWP

// BEGIN PRAISE TAXONOMY
add_action( 'init', 'create_praise_taxonomy', 0 );
function create_praise_taxonomy() {
  $labels = array(
    'name' => _x( 'Praises', 'taxonomy general name' ),
    'singular_name' => _x( 'Praise', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Praises' ),
    'all_items' => __( 'All Praises' ),
    'parent_item' => __( 'Parent Praise' ),
    'parent_item_colon' => __( 'Parent Praise:' ),
    'edit_item' => __( 'Edit Praise' ),
    'update_item' => __( 'Update Praise' ),
    'add_new_item' => __( 'Add New Praise' ),
    'new_item_name' => __( 'New Genre Praise' ),
    'menu_name' => __( 'Praises' ),
  );
}

add_action( 'init', 'register_praise_custom_post' );
function register_praise_custom_post() {
  $labels = array(
    'name' => _x('Praises', 'post type general name'),
    'singular_name' => _x('Praise', 'post type singular name'),
    'add_new' => _x('Add New', 'praise'),
    'add_new_item' => __('Add New Praise'),
    'edit_item' => __('Edit Praise'),
    'new_item' => __('New Praise'),
    'all_items' => __('All Praises'),
    'view_item' => __('View Praise'),
    'search_items' => __('Search Praises'),
    'not_found' =>  __('No praises found'),
    'not_found_in_trash' => __('No praises found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'Praises'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null
  );
  register_post_type('praise', $args);
}
add_action( 'add_meta_boxes', 'praise_add_post_meta_boxes' );
add_action( 'save_post', 'praise_save_post_class_meta', 10, 2 );

function praise_add_post_meta_boxes() {
  add_meta_box(
    'praise',      // Unique ID
    esc_html__( 'Praise', 'example' ),    // Title
    'praise_post_class_meta_box',   // Callback function
    'praise',         // Admin page (or post type)
    'normal',         // Context
    'default'         // Priority
  );
}
/* Display the post meta box. */
function praise_post_class_meta_box( $object, $box ) {
  wp_nonce_field( basename( __FILE__ ), 'praise_post_class_nonce' ); ?>
  <?php
    $author = get_post_meta( $object->ID, '_author', true );
    $title = get_post_meta( $object->ID, '_title', true );
    $company = get_post_meta( $object->ID, '_company', true );
  ?>
  <p>
    <label for="author"><?php _e( "Author", 'example' ); ?></label>
    <br />
    <input type="text" name="author" id="author" value="<?php echo esc_attr( get_post_meta( $object->ID, '_author', true ) ); ?>" size="30" />
  </p>
  <p>
    <label for="title"><?php _e( "Title", 'example' ); ?></label>
    <br />
    <input type="text" name="title" id="title" value="<?php echo esc_attr( get_post_meta( $object->ID, '_title', true ) ); ?>" size="30" />
  </p>
  <p>
    <label for="company"><?php _e( "Company", 'example' ); ?></label>
    <br />
    <input type="text" name="company" id="company" value="<?php echo esc_attr( get_post_meta( $object->ID, '_company', true ) ); ?>" size="30" />
  </p>
<?php }
add_action( 'save_post', 'praise_save_post_class_meta', 10, 2 );

/* Save the meta box's post metadata. */
function praise_save_post_class_meta( $post_id, $post ) {
  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['praise_post_class_nonce'] ) || !wp_verify_nonce( $_POST['praise_post_class_nonce'], basename( __FILE__ ) ) )
    return $post_id;
  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );

  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  // author
  $new_meta_value = ( isset( $_POST['author'] ) ? $_POST['author'] : '' );
  $meta_value = get_post_meta( $post_id, '_author', true );
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, '_author', $new_meta_value, true );
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, '_author', $new_meta_value );
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, '_author', $meta_value );
  // title
  $new_meta_value = ( isset( $_POST['title'] ) ? $_POST['title'] : '' );
  $meta_value = get_post_meta( $post_id, '_title', true );
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, '_title', $new_meta_value, true );
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, '_title', $new_meta_value );
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, '_title', $meta_value );
  // company
  $new_meta_value = ( isset( $_POST['company'] ) ? $_POST['company'] : '' );
  $meta_value = get_post_meta( $post_id, '_company', true );
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, '_company', $new_meta_value, true );
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, '_company', $new_meta_value );
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, '_company', $meta_value );
}

// END PRAISE TAXONOMY

// BEGIN ALUMNI DOWNLOADS TAXONOMY
  add_action( 'init', 'create_download_taxonomy', 0 );
  function create_download_taxonomy() {
    $labels = array(
      'name' => _x( 'Alumni Downloads', 'taxonomy general name' ),
      'singular_name' => _x( 'Alumni Download', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Alumni Downloads' ),
      'all_items' => __( 'All Alumni Downloads' ),
      'parent_item' => __( 'Parent Alumni Download' ),
      'parent_item_colon' => __( 'Parent Alumni Download:' ),
      'edit_item' => __( 'Edit Alumni Download' ),
      'update_item' => __( 'Update Alumni Download' ),
      'add_new_item' => __( 'Add New Alumni Download' ),
      'new_item_name' => __( 'New Genre Alumni Download' ),
      'menu_name' => __( 'Alumni Downloads' ),
    );
  }

  add_action( 'init', 'register_download_custom_post' );
  function register_download_custom_post() {
    $labels = array(
      'name' => _x('Alumni Downloads', 'post type general name'),
      'singular_name' => _x('Alumni Download', 'post type singular name'),
      'add_new' => _x('Add New', 'Alumni Download'),
      'add_new_item' => __('Add New Alumni Download'),
      'edit_item' => __('Edit Alumni Download'),
      'new_item' => __('New Alumni Download'),
      'all_items' => __('All Alumni Downloads'),
      'view_item' => __('View Alumni Download'),
      'search_items' => __('Search Alumni Download'),
      'not_found' =>  __('No Alumni Downloads found'),
      'not_found_in_trash' => __('No Alumni Downloads found in Trash'),
      'parent_item_colon' => '',
      'menu_name' => 'Alumni Downloads'
    );
    $args = array(
      'labels' => $labels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'has_archive' => true,
      'hierarchical' => false,
      'menu_position' => null
    );
    register_post_type('download', $args);
  }
  add_action( 'add_meta_boxes', 'download_add_post_meta_boxes' );
  add_action( 'save_post', 'download_save_post_class_meta', 10, 2 );

  function download_add_post_meta_boxes() {
    add_meta_box(
      'download',      // Unique ID
      esc_html__( 'Alumni Download', 'example' ),    // Title
      'download_post_class_meta_box',   // Callback function
      'download',         // Admin page (or post type)
      'normal',         // Context
      'default'         // Priority
    );
  }
  /* Display the post meta box. */
  function download_post_class_meta_box( $object, $box ) {
    wp_register_script('my-uploader', get_bloginfo('template_directory') . '/js/uploader.js', array('jquery','media-upload'));
    wp_enqueue_script('my-uploader');
    wp_enqueue_script('media-upload');

    wp_nonce_field( basename( __FILE__ ), 'download_post_class_nonce' ); ?>
    <?php
      $date = get_post_meta( $object->ID, '_date', true );
      $speakers = get_post_meta( $object->ID, '_speakers', true );
      $summary = get_post_meta( $object->ID, '_summary', true );
      $keypoint_summary = get_post_meta( $object->ID, '_keypoint_summary', true );
      $extra_pdf_text = get_post_meta( $object->ID, '_extra_pdf_text', true );
      $extra_pdf = get_post_meta( $object->ID, '_extra_pdf', true );
      $recording = get_post_meta( $object->ID, '_recording', true );
      $purchase_link_text = get_post_meta( $object->ID, '_purchase_link_text', true );
      $purchase_link = get_post_meta( $object->ID, '_purchase', true );
    ?>
    <p>
      <label for="date"><?php _e( "Date", 'example' ); ?></label>
      <br />
      <input type="text" name="date" id="date" value="<?php echo $date; ?>" size="30" />
    </p>
    <p>
      <label for="speakers"><?php _e( "Speakers", 'example' ); ?></label>
      <br />
      <input type="text" name="speakers" id="speakers" value="<?php echo $speakers; ?>" size="30" />
    </p>
    <p>
      <label for="summary"><?php _e( "Summary", 'example' ); ?></label>
      <br />
      <textarea class="widefat" rows="5" cols="80" name="summary" id="summary"><?php echo $summary; ?></textarea>
    </p>
    <p>
      <input class="button" name="keypoint_summary_button" id="keypoint_summary_button" value="Keypoint Summary" />
      <input type="text" size="75" id="keypoint_summary" name="keypoint_summary" value="<?php echo $keypoint_summary; ?>" class="large-text" /><br />
      <span class="description">PDF</span>
    </p>
    <hr />
    <p>
      <label for="extra_pdf_text"><?php _e( "Extra PDF Text", 'example' ); ?></label>
      <br />
      <input type="text" name="extra_pdf_text" id="extra_pdf_text" value="<?php echo $extra_pdf_text; ?>" size="30" />
    </p>
    <p>
      <input class="button" name="extra_pdf_button" id="extra_pdf_button" value="Extra PDF" />
      <input type="text" size="75" id="extra_pdf" name="extra_pdf" value="<?php echo $extra_pdf; ?>" class="large-text" /><br />
      <span class="description">PDF</span>
    </p>
    <hr />
    <p>
      <input class="button" name="recording_button" id="recording_button" value="Recording" />
      <input type="text" size="75" id="recording" name="recording" value="<?php echo $recording; ?>" class="large-text" /><br />
      <span class="description">MP4</span>
    </p>
    <hr />
    <p>
      <label for="purchase_link_text"><?php _e( "Purchase Link Text", 'example' ); ?></label>
      <br />
      <input type="text" name="purchase_link_text" id="purchase_link_text" value="<?php echo $purchase_link_text; ?>" size="30" />
    </p>
    <p>
      <input class="button" name="purchase_button" id="purchase_button" value="Purchase Link" />
      <input type="text" size="75" id="purchase" name="purchase" value="<?php echo $purchase_link; ?>" class="large-text" /><br />
      <span class="description">PDF</span>
    </p>
  <?php }
  add_action( 'save_post', 'download_save_post_class_meta', 10, 2 );

  /* Save the meta box's post metadata. */
  function download_save_post_class_meta( $post_id, $post ) {
    /* Verify the nonce before proceeding. */
    if ( !isset( $_POST['download_post_class_nonce'] ) || !wp_verify_nonce( $_POST['download_post_class_nonce'], basename( __FILE__ ) ) )
      return $post_id;
    /* Get the post type object. */
    $post_type = get_post_type_object( $post->post_type );

    /* Check if the current user has permission to edit the post. */
    if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
      return $post_id;

    // date
    $new_meta_value = ( isset( $_POST['date'] ) ? $_POST['date'] : '' );
    $meta_value = get_post_meta( $post_id, '_date', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_date', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_date', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_date', $meta_value );
    // speakers
    $new_meta_value = ( isset( $_POST['speakers'] ) ? $_POST['speakers'] : '' );
    $meta_value = get_post_meta( $post_id, '_speakers', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_speakers', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_speakers', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_speakers', $meta_value );
    // summary
    $new_meta_value = ( isset( $_POST['summary'] ) ? $_POST['summary'] : '' );
    $meta_value = get_post_meta( $post_id, '_summary', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_summary', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_summary', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_summary', $meta_value );
    // keypoint_summary
    $new_meta_value = ( isset( $_POST['keypoint_summary'] ) ? $_POST['keypoint_summary'] : '' );
    $meta_value = get_post_meta( $post_id, '_keypoint_summary', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_keypoint_summary', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_keypoint_summary', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_keypoint_summary', $meta_value );
    // extra_pdf_text
    $new_meta_value = ( isset( $_POST['extra_pdf_text'] ) ? $_POST['extra_pdf_text'] : '' );
    $meta_value = get_post_meta( $post_id, '_extra_pdf_text', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_extra_pdf_text', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_extra_pdf_text', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_extra_pdf_text', $meta_value );
    // extra_pdf
    $new_meta_value = ( isset( $_POST['extra_pdf'] ) ? $_POST['extra_pdf'] : '' );
    $meta_value = get_post_meta( $post_id, '_extra_pdf', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_extra_pdf', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_extra_pdf', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_extra_pdf', $meta_value );
    // recording
    $new_meta_value = ( isset( $_POST['recording'] ) ? $_POST['recording'] : '' );
    $meta_value = get_post_meta( $post_id, '_recording', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_recording', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_recording', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_recording', $meta_value );
    // purchase_link_text
    $new_meta_value = ( isset( $_POST['purchase_link_text'] ) ? $_POST['purchase_link_text'] : '' );
    $meta_value = get_post_meta( $post_id, '_purchase_link_text', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_purchase_link_text', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_purchase_link_text', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_purchase_link_text', $meta_value );
    // purchase_link
    $new_meta_value = ( isset( $_POST['purchase'] ) ? $_POST['purchase'] : '' );
    $meta_value = get_post_meta( $post_id, '_purchase', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_purchase', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_purchase', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_purchase', $meta_value );
  }
// END ALUMNI DOWNLOADS TAXONOMY


add_action( 'admin_head', 'admin_css' );
function admin_css(){
  echo '<style type="text/css">
    .praise_options {
      list-style-type:none;
      margin-left:0;
    }
    .praise_options label {
      width:70px;
      float:left;
    }
  </style>';
}

// POST META BOXES
  function hpwp2_add_post_meta_boxes() {
    add_meta_box(
      'hpwp2',      // Unique ID
      esc_html__( 'Post Options', 'example' ),    // Title
      'hpwp2_post_class_meta_box',   // Callback function
      'post',         // Admin page (or post type)
      'normal',         // Context
      'default'         // Priority
    );
  }
  add_action( 'add_meta_boxes', 'hpwp2_add_post_meta_boxes' );

  /* Display the post meta box. */
  function hpwp2_post_class_meta_box( $object, $box ) {

    wp_nonce_field( basename( __FILE__ ), 'hpwp2_post_class_nonce' ); ?>
    <p>
      <label for="hpwp2_link_text"><?php _e( "Link Text", 'example' ); ?></label>
      <br />
      <input type="text" class="widefat" name="hpwp2_link_text" id="hpwp2_link_text" value="<?php echo esc_attr( get_post_meta( $object->ID, '_hpwp2_link_text', true ) ); ?>" size="30" />
    </p>
    <p>
      <label for="hpwp2_link_url"><?php _e( "Link URL (File or Outside URL)", 'example' ); ?></label>
      <br />
      <input type="text" class="widefat" name="hpwp2_link_url" id="hpwp2_link_url" value="<?php echo esc_attr( get_post_meta( $object->ID, '_hpwp2_link_url', true ) ); ?>" size="30" />
      <br />
      <span class="description">If linking to a PDF, make sure you upload it first to the Media section to the left and copy the URL</span>
    </p>
  <?php }

  /* Save the meta box's post metadata. */
  function hpwp2_save_post_class_meta( $post_id, $post ) {

    /* Verify the nonce before proceeding. */
    if ( !isset( $_POST['hpwp2_post_class_nonce'] ) || !wp_verify_nonce( $_POST['hpwp2_post_class_nonce'], basename( __FILE__ ) ) )
      return $post_id;

    /* Get the post type object. */
    $post_type = get_post_type_object( $post->post_type );

    //
    $new_meta_value = ( isset( $_POST['hpwp2_link_text'] ) ? $_POST['hpwp2_link_text'] : '' );
    $meta_value = get_post_meta( $post_id, '_hpwp2_link_text', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_hpwp2_link_text', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_hpwp2_link_text', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_hpwp2_link_text', $meta_value );
    //
    $new_meta_value = ( isset( $_POST['hpwp2_link_url'] ) ? $_POST['hpwp2_link_url'] : '' );
    $meta_value = get_post_meta( $post_id, '_hpwp2_link_url', true );
    if ( $new_meta_value && '' == $meta_value )
      add_post_meta( $post_id, '_hpwp2_link_url', $new_meta_value, true );
    elseif ( $new_meta_value && $new_meta_value != $meta_value )
      update_post_meta( $post_id, '_hpwp2_link_url', $new_meta_value );
    elseif ( '' == $new_meta_value && $meta_value )
      delete_post_meta( $post_id, '_hpwp2_link_url', $meta_value );
  }
  add_action( 'save_post', 'hpwp2_save_post_class_meta', 10, 2 );
// END hpwp2

function custom_login_message() {
$message = "<p class='message register'>Register For This Site.<br /><br />After you register, your request will be sent to the site administrator for approval. You will then receive an email with further instructions.</p>";
return $message;
}
add_filter('login_message', 'custom_login_message');

function remove_private_prefix($title) {
$title = str_replace(
'Private:',
'',
$title);
return $title;
}
add_filter('the_title','remove_private_prefix');

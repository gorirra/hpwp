<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
  <head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <script type="text/javascript" src="//use.typekit.net/hmt7vzx.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  </head>
  <body <?php body_class(); ?>>
    <div class="social_group">
      <ul>
        <li><a href="https://www.facebook.com/HPWPConsulting/" target="_blank"><img class="social_icon facebook" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif"></a></li>
        <li><a href="http://www.linkedin.com/company/hpwp-consulting-llc?trk=cp_followed_name_hpwp-consulting-llc" target="_blank"><img class="social_icon linkedin" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif"></a></li>
        <li><a href="https://twitter.com/hpwpconsulting" target="_blank"><img class="social_icon twitter" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif"></a></li>
        <li><a href="https://www.youtube.com/user/HPWPCONSULTING" target="_blank"><img class="social_icon youtube" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif"></a></li>
        <li><a href="<?php bloginfo('rss_url'); ?>"><img class="social_icon rss" src="<?php bloginfo('stylesheet_directory'); ?>/images/blank.gif"></a></li>
      </ul>
    </div>
    <div class="top_wrapper_container">
      <div class="top_wrapper">
        <div class="tagline"><span class="first_part">Are You</span> <span class="second_part">Ready To...</span></div>
        <a href="<?php echo get_option('home'); ?>/"><img alt="<?php bloginfo('description'); ?>" title="<?php bloginfo('description'); ?>" src="<?php bloginfo('stylesheet_directory'); ?>/images/hpwp-logo.png" class="logo" /></a>
      </div>
    </div>
    <div class="nav">
      <?php wp_nav_menu(array('menu' => 'menu')); ?>
    </div>
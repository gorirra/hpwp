<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>
  <div class="content_container">
    <?php get_sidebar(); ?>
    <div class="content">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="entry">
        <h1>[ <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> ]</h1>
        <p><?php the_time('F jS, Y') ?></p>
        <?php if ( has_post_thumbnail() ) {
          the_post_thumbnail('thumbnail', array('class' => 'alignleft'));
        } ?>
        <?php the_excerpt(); ?>
      </div>
      <!--<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>-->
      <?php endwhile; endif; ?>
    </div>
    <div class="bottom_nav">
      <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
    </div>
  </div>
<?php get_footer(); ?>

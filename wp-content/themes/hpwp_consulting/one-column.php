<?php
/*
  Template Name: One column
*/

get_header(); ?>
    <div class="content_container">
      <div class="content_one_column">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <!--<h1>[ <?php the_title(); ?> ]</h1>-->
        <div class="entry">
          <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        </div>
        <?php endwhile; endif; ?>
      </div>
      <div class="bottom_nav">
        <?php wp_nav_menu(array('menu' => 'bottom_nav')); ?> 
      </div>
    </div>
<?php get_footer(); ?>

<?php
  $featured_quote = get_post_meta (get_the_ID(), '_hpwp_featured_quote', true);
?>
<div class="side_content_container">
  <div class="side_content">
    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/hpwp-alumni-community.png">
    <div class="featured_image">
      <?php
        if ( has_post_thumbnail() ) {
          the_post_thumbnail();
        } 
      ?>
    </div>
    <?php if ($featured_quote != '') { ?>
      <div class="featured_quote_container">
        <div class="featured_quote">
          <?php echo $featured_quote; ?>
        </div>
      </div>
    <?php } ?>
    <div class="custom_widget">
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('custom widget') ) : else : endif; ?>
    </div>
  </div>
</div>
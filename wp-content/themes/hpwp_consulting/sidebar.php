<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

$post_id = get_the_ID();
$event = get_post_meta ($post_id, '_hpwp_featured_quote', true);
$extra_text = get_post_meta ($post_id, '_hpwp_extra_text', true);
$extra_featured_image = get_post_meta ($post_id, '_extra_featured_image', true);
$extra_featured_image_overflow = get_post_meta ($post_id, '_hpwp_overflow', true);
?>
<?php if (is_front_page()) {
  query_posts( array('post_type' => 'praise', 'orderby' => 'ID', 'order' => 'ASC'));
  ?>

  <div class="home_side_content_container">
    <div class="side_content">
      <ol>
        <?php while ( have_posts() ) : the_post();
          $author_value = get_post_meta($post->ID, '_author', true);
          $title_value = get_post_meta($post->ID, '_title', true);
          $company_value = get_post_meta($post->ID, '_company', true);
          echo '<li><span class="big_quote">&#8220;</span>';
            the_content();
            echo '<p>
              &mdash; <span class="author_name">' . $author_value . '</span>, <span class="author_title">' . $title_value . '</span>, <span class="company_name">' . $company_value . '</span>';
         echo '</p></li>';
        endwhile; ?>
      </ol>
      <p><a href="<?php echo get_option('home'); ?>/results/">Ready to see more?</a></p>
    </div>
    <div class="side_content_bottom">
    </div>
  </div>
  <?php wp_reset_query(); 
} else { ?>
  <div class="side_content_container">
    <div class="side_content">
      <div class="custom_widget">
        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('custom widget') ) : else : endif; ?>
      </div>
      <?php if (!is_home() && !is_category() && !is_tag()) { ?>
        <div class="featured_image">
          <?php
            if ( has_post_thumbnail() ) {
              the_post_thumbnail();
            } 
          ?>
        </div>
      <?php } ?>
      <?php if ($event != '') { ?>
        <div class="featured_quote_container">
          <div class="featured_quote">
            <?php echo $event; ?>
          </div>
        </div>
      <?php } ?>
      <?php if ($extra_text != '') { ?>
        <div class="extra_text_container">
          <div class="extra_text">
            <?php echo $extra_text; ?>
          </div>
        </div>
      <?php } ?>
      <?php if ($extra_featured_image != '') { 
        $overflow_class = '';
        if ($extra_featured_image_overflow == 'on') {
          $overflow_class = ' extra_featured_image_overflow';
        }
      ?>
        <div class="extra_featured_image_container<?php echo $overflow_class; ?>">
          <div class="extra_featured_image">
            <img src="<?php echo $extra_featured_image; ?>" />
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
<?php } ?>
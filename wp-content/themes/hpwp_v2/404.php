<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package hpwp_v2
 */

get_header(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'err-404' ); ?>>
	<section class="module module-white">
		<div class="container">
			<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'hpwp_v2' ); ?></h1>
			<p>It looks like nothing was found at this location.</p>
		</div>
	</section>
</div>

<?php get_footer();

<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


		<?php get_template_part( 'template-parts/content', 'archive' ); ?>

		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();

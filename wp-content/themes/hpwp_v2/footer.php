<?php
	/**
	 * The template for displaying the footer
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$pageid	= get_the_ID();
?>

<!-- footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-4 footer-nav">
				<p>Company</p>
				<?php
					$args = array(
						'menu'				=> 'footer-nav',
						'menu_class'		=> 'footer-nav__menu',
						'fallback_cb'		=> false,
					);
					wp_nav_menu( $args );
				?>
			</div>
			<div class="col-md-4">
				<p><a href="<?php echo site_url( 'contact' ); ?>">Contact Us</a></p>

				<?php
					echo apply_filters( 'the_content', hpwp_get_option( 'address' ) ) .'
					<p>'. hpwp_get_option( 'hpwp_phone' ) .'</p>';
				?>

			</div>
			<div class="col-md-4">
				<p>Follow us</p>

				<?php
					// social stuffs
					$social_array = implode( ' ', display_social_media() );

					echo '<p class="social">'.
						$social_array .'
					</p>';

					$legalpage_url = hpwp_get_option( 'legal_url' ) != '' ? hpwp_get_option( 'legal_url' ) : '';

					if( $legalpage_url != '' ) {
						echo '<p><a href="'. $legalpage_url .'">Legal</a></p>';
					}
				?>
			</div>
<!-- 			<div class="col-md-3">
				<p>Subscribe</p>
				<?php /*
					<form action="/" class="subscribeform">
						<div class="form-group">
							<label for="subscribe_email" class="sr-only">Subscribe</label>
							<input type="text" id="subscribe_email" class="subscribeform__text form-control" placeholder="Email">
							<button type="submit" class="subscribeform__btn btn btn-default">Submit</button>
						</div>
					</form>
				*/ ?>

				<div class="form-inline">
					<?php //echo apply_filters( 'the_content', hpwp_get_option( 'hpwp_cc_subscribe_form' ) ); ?>
				</div>

			</div> -->
		</div>
		<p>&copy; <?php echo date( 'Y' ); ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved. <a href="https://hpwpgroup.com/sitemap/">Sitemap</a></p>
		
		</p>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>

<?php
require( 'inc/hpwp-theme-options.php' );
require( 'inc/hpwp-nav-walker.php' );
require( 'inc/hpwp-footer-nav-walker.php' );
require( 'inc/hpwp-shortcodes.php' );
require( 'inc/hpwp-testimonial-query.php' );
require( 'inc/hpwp-events-query.php' );
require( 'inc/hpwp-webinars-query.php' );
require( 'inc/hpwp-team-query.php' );

require( 'inc/metadata/hpwp-metadata-everypage.php' );
require( 'inc/metadata/hpwp-metadata-homepage.php' );
require( 'inc/metadata/hpwp-metadata-homepage-book.php' );
require( 'inc/metadata/hpwp-metadata-team.php' );
require( 'inc/metadata/hpwp-metadata-about.php' );
require( 'inc/metadata/hpwp-metadata-alumni-landing.php' );
require( 'inc/metadata/hpwp-metadata-singlepress.php' );
require( 'inc/metadata/hpwp-metadata-testimonials.php' );
require( 'inc/metadata/hpwp-metadata-events.php' );
require( 'inc/metadata/hpwp-metadata-webinars.php' );
require( 'inc/metadata/hpwp-metadata-singleevent.php' );
require( 'inc/metadata/hpwp-metadata-singlewebinar.php' );
require( 'inc/metadata/hpwp-metadata-solutions.php' );
require( 'inc/metadata/hpwp-metadata-press.php' );
require( 'inc/metadata/hpwp-metadata-book.php' );
require( 'inc/metadata/hpwp-metadata-clients.php' );
require( 'inc/metadata/hpwp-metadata-hiringteam.php' );
require( 'inc/metadata/hpwp-metadata-master-class.php' );
require( 'inc/metadata/hpwp-metadata-speaker.php' );
require( 'inc/metadata/hpwp-metadata-speaker-registration.php' );
require( 'inc/metadata/hpwp-metadata-singlecoachingevent.php' );

require( 'inc/post-types/hpwp-cpt-team.php' );
require( 'inc/post-types/hpwp-cpt-press.php' );
require( 'inc/post-types/hpwp-cpt-testimonials.php' );
require( 'inc/post-types/hpwp-cpt-events.php' );
require( 'inc/post-types/hpwp-cpt-coaching-events.php' );
require( 'inc/post-types/hpwp-cpt-webinars.php' );

add_action( 'init', array( 'HPWP_Metadata_EveryPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_HomePage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_HomePageBook', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_Team', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_AboutPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_AlumniLandingPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SinglePress', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_Testimonials', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SingleEvent', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SingleWebinar', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_Solutions', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_PressPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_BookPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_EventsPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_WebinarsPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_ClientsPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_HiringTeamPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SpeakerPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SpeakerRegistrationPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_MasterClassPage', 'singleton' ) );
add_action( 'init', array( 'HPWP_Metadata_SingleCoachingEvent', 'singleton' ) );

add_action( 'init', array( 'HPWP_CPT_Team', 'singleton' ) );
add_action( 'init', array( 'HPWP_CPT_Press', 'singleton' ) );
add_action( 'init', array( 'HPWP_CPT_Testimonials', 'singleton' ) );
add_action( 'init', array( 'HPWP_CPT_Events', 'singleton' ) );
add_action( 'init', array( 'HPWP_CPT_Coaching_Events', 'singleton' ) );
add_action( 'init', array( 'HPWP_CPT_Webinars', 'singleton' ) );

add_action( 'init', array( 'HPWP_Shortcodes', 'singleton' ) );


/**
 * hpwp_v2 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hpwp_v2
 */

if ( ! function_exists( 'hpwp_v2_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hpwp_v2_setup() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );


		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'hpwp_v2' ),
		) );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/**
		 * add image sizes
		 */
		add_image_size( 'event-tiles', 300, 300, true );
		add_image_size( 'tile-img', 400, 200, true );
		add_image_size( 'heromobile_img', 640, 450, true );
		add_image_size( 'hero_img', 1920, 600, true );

	}
endif;
add_action( 'after_setup_theme', 'hpwp_v2_setup' );


/**
 * Enqueue scripts and styles.
 */
function hpwp_v2_scripts() {
	wp_enqueue_style( 'hpwp_v2-style', get_template_directory_uri() . '/style.min.css', false, filemtime(get_template_directory() . '/style.min.css'), 'all' );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }

	// footer scripts
	//wp_deregister_script( 'jquery' );
	//wp_register_script( 'jquery', '//code.jquery.com/jquery-1.11.2.min.js', array(), false, false );
	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'hpwp_v2-script', get_template_directory_uri() . '/js/hpwp.min.js?v=7', array('jquery'), true );
}
add_action( 'wp_enqueue_scripts', 'hpwp_v2_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';


/**
 * remove generator and emoji head tags
 */
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/**
 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
 */
function add_fonts_asynchronously() { ?>

	<script>
		WebFontConfig = {
			classes: false,
			events: false,
			google: {
				families: ['Open+Sans:400,700', 'Roboto:300,900']
			},
		};

		(function(d) {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})(document);
	</script>

	<?php
}
add_action( 'wp_head', 'add_fonts_asynchronously', 0 );


/**
 * allow editor to upload svg files
 */
function custom_upload_mimes ( $existing_mimes = array() ) {
	// add the file extension to the array
	$existing_mimes['svg'] = 'mime/type';
	// call the modified list of extensions
	return $existing_mimes;
}
add_filter('upload_mimes', 'custom_upload_mimes');


/**
 * Get Meta
 *
 * Utility function used to consolidate the quering of multiple meta values
 * for the given object.
 *
 * @param int	 	$id ID of the current object.
 * @param mixed		$fields Array/string containing meta field(s) to retrieve from database.
 * @param string	$type Type of metadata request. Options: post/term/user
 * @param constant	$output pre-defined constant for return type (OBJECT/ARRAY_A)
 *
 * @return mixed	MySQL object/Associative Array containing returned post metadata.
 */
function get_meta( $id = null, $fields = array(), $type = 'post', $output = ARRAY_A ) {
	global $wpdb;

	$fields		= esc_sql( $fields );
	$values_arr	= array();
	$values_obj	= new stdClass();
	$dbtable	= $wpdb->{$type.'meta'};
	$column_id	= $type . '_id';
	$id			= $id == null ? get_the_ID() : $id ;

	$query		= "SELECT meta_key, meta_value FROM {$dbtable} WHERE {$column_id} = {$id}";

	if ( !empty( $fields ) ) {

		if ( is_array( $fields ) ) {
			$query .= " AND meta_key IN ('". implode("','", $fields) ."')";
		} else {
			$query .= " AND meta_key = '{$fields}'";
		}
	}

	$results	=  $wpdb->get_results( $query, OBJECT_K );


	foreach ( $results as $key => $result ) {
		$values_arr[$key]	= $result->meta_value;
		$values_obj->{$key}	= $result->meta_value;
	}

	if ( !is_array( $fields ) && !empty( $values_arr[$fields] ) ) {

		return $output == ARRAY_A ? $values_arr[$fields] : $values_obj[$fields];

	}

	return $output == ARRAY_A ? $values_arr : $values_obj;
}


/**
 * Filters the content to remove any extra paragraph or break tags
 * caused by shortcodes.
 *
 * @since 1.0.0
 *
 * @param string $content  String of HTML content.
 * @return string $content Amended string of HTML content.
 */
function shortcode_empty_paragraph_fix( $content ) {
	$array = array(
		'<p>['		=> '[',
		']</p>'		=> ']',
		']<br />'	=> ']'
	);
	return strtr( $content, $array );

}
add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );


/**
 * date logic
 */
function get_display_date( $eventid ) {

	$prefix = '_hpwp_';

	$start_date		= get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ) ? get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ) : '';
	$start_date_m	= date( 'm', $start_date );
	$start_date_d	= date( 'd', $start_date );
	$start_date_y	= date( 'Y', $start_date );

	$end_date	= get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ) ? get_post_meta( $eventid, $prefix . 'eventdate_end', 1 ) : '';
	$end_date_m	= $end_date != '' ? date( 'm', $end_date ) : '';
	$end_date_d	= $end_date != '' ? date( 'd', $end_date ) : '';
	$end_date_y = $end_date != '' ? date( 'Y', $end_date ) : '';

	if( $end_date == '' ) {	// no end date (single day)
		$display_date = date( 'M. j, Y', $start_date );
	} elseif( $start_date_y != $end_date_y) {	//
		$display_date = date( 'M. j, Y', $start_date ) .' &ndash; '. date( 'M. j, Y', $end_date );
	} elseif( ($start_date_m == $end_date_m) && ($start_date_d == $end_date_d) ) {
		$display_date = date( 'M. j, Y', $start_date );
	} elseif( $start_date_m == $end_date_m ) {
		$display_date = date( 'M. j', $start_date ) .' &ndash; '. date( 'M. j, Y', $end_date );
	} elseif( $start_date_m != $end_date_m ) {
		$display_date = date( 'M. j', $start_date ) .' &ndash; '. date( 'M. j, Y', $end_date );
	}

	return $display_date;

}

function get_display_time( $eventid ) {
	$prefix = '_hpwp_';

	$start_time = get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ) ? get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ) : '';
	$start_time_gia = date('g:i A', $start_time);
	$end_time = get_post_meta( $eventid, $prefix . 'eventdate_end', 1 ) ? get_post_meta( $eventid, $prefix . 'eventdate_end', 1 ) : '';
	$end_time_gia = date('g:i A', $end_time);

	$display_time = $start_time_gia .' &ndash; '. $end_time_gia;

	return $display_time;

}

/**
 * function: cmb2 filter to exclude a metabox from a template
 */
function be_metabox_exclude_on_template( $display, $meta_box ) {

	if ( !isset( $meta_box['show_on']['key'] ) )
		return $display;

	if( isset( $meta_box['show_on']['key'] ) && 'exclude_template' !== $meta_box['show_on']['key'] )
		return $display;

	// Get the current ID
	if( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
	elseif( isset( $_POST['post_ID'] ) ) $post_id = $_POST['post_ID'];
	if( !isset( $post_id ) ) return false;

	$template_name = get_page_template_slug( $post_id );
	// $template_name = substr($template_name, 0, -4);

	// If value isn't an array, turn it into one
	if( isset( $meta_box['show_on']['value'] ) ) {
		if( !is_array( $meta_box['show_on']['value'] ) ) {
			$showonvalue = array( $meta_box['show_on']['value'] );
		} else {
			$showonvalue = $meta_box['show_on']['value'];
		}
	} else {
		$showonvalue = array();
	}

	// See if there's a match
	if( in_array( $template_name, $showonvalue ) ) {
		return false;
	} else {
		return true;
	}
}
add_filter( 'cmb2_show_on', 'be_metabox_exclude_on_template', 10, 2 );


/**
 * gravity forms
 * prepopulate data
 */
add_filter( 'gform_field_value_location_date', 'populate_location_date' );
function populate_location_date( $value ) {
	$location	= get_the_title();
	$date		= get_post_meta( get_the_ID(), '_hpwp_eventdate_start', 1 );

	return $location .' - '. $date;
}

add_filter( 'gform_field_value_webinar_date', 'populate_webinar_date' );
function populate_webinar_date( $value ) {
	$date = get_post_meta( get_the_ID(), '_hpwp_eventdate_start', 1 );

	return date('M. j, Y', $date);
}


/**
 * display svg
 */
function display_svg( $file ) {
	$arrContextOptions=array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		),
	);

	return file_get_contents( $file, false, stream_context_create($arrContextOptions) );
}


/**
 * get social media icons
 * grabs url's from those set in site options
 */
function display_social_media() {
	$social_array = [];
	$social_array[]	= hpwp_get_option( 'linkedin_url' ) != '' ? '<a target="_blank" href="'. hpwp_get_option( 'linkedin_url' ) .'"><i class="icon-linkedin-squared"></i></a>' : '';
	$social_array[]	= hpwp_get_option( 'googleplus_url' ) != '' ? '<a target="_blank" href="'. hpwp_get_option( 'googleplus_url' ) .'"><i class="icon-gplus"></i></a>' : '';
	$social_array[]	= hpwp_get_option( 'facebook_url' ) != '' ? '<a target="_blank" href="'. hpwp_get_option( 'facebook_url' ) .'"><i class="icon-facebook-official"></i></a>' : '';
	$social_array[]	= hpwp_get_option( 'twitter_url' ) != '' ? '<a target="_blank" href="'. hpwp_get_option( 'twitter_url' ) .'"><i class="icon-twitter"></i></a>' : '';
	$social_array[]	= hpwp_get_option( 'instagram_url' ) != '' ? '<a target="_blank" href="'. hpwp_get_option( 'instagram_url' ) .'"><i class="icon-instagram"></i></a>' : '';

	return $social_array;
}


/**
 * display favicon
 * use https://realfavicongenerator.net/ to create assets
 */
function hpwp_display_favicon() {
	$pathtofavicon = get_template_directory_uri() .'/images/favicon'; ?>

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $pathtofavicon; ?>/apple-touch-icon.png?v=qA3bYqYMnY">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $pathtofavicon; ?>/favicon-32x32.png?v=qA3bYqYMnY">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $pathtofavicon; ?>/favicon-16x16.png?v=qA3bYqYMnY">
	<link rel="manifest" href="<?php echo $pathtofavicon; ?>/site.webmanifest?v=qA3bYqYMnY">
	<link rel="mask-icon" href="<?php echo $pathtofavicon; ?>/safari-pinned-tab.svg?v=qA3bYqYMnY" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo $pathtofavicon; ?>/favicon.ico?v=qA3bYqYMnY">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="theme-color" content="#ffffff">

<?php }

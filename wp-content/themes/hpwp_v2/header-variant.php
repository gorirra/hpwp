<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$pageid	= get_the_ID();
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="msvalidate.01" content="FC1C65A0B2BADDD09D0345B9082E4AAF" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script type="text/javascript" id="ico-key" data-key="eb5f3c5ec6c3e9eab95c73d848915dc4" src="//analytics.influenceandco.com/ico.min.js"></script>
	<?php
		hpwp_display_favicon();
		wp_head();
	?>

</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hpwp_v2' ); ?></a>

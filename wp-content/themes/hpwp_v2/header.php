<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$pageid	= get_the_ID();
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="msvalidate.01" content="FC1C65A0B2BADDD09D0345B9082E4AAF" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script type="text/javascript" id="ico-key" data-key="eb5f3c5ec6c3e9eab95c73d848915dc4" src="//analytics.influenceandco.com/ico.min.js"></script>
	<?php
		hpwp_display_favicon();
		wp_head();
	?>

</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hpwp_v2' ); ?></a>


	<!-- nav -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-left">
					<a class="navbar-brand" href="<?php echo site_url(); ?>">

						<?php
							$main_logo = hpwp_get_option( 'main_logo' ) != '' ? hpwp_get_option( 'main_logo' ) : get_bloginfo( 'stylesheet_directory') . '/images/hpwp-logo.svg';

							echo display_svg( $main_logo );
						?>

					</a>
				</div>

			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">

					<?php
						$args = array(
							'menu'				=> 'primary',
							'menu_id'			=> '',
							'menu_class'		=> '',
							'container'			=> '',
							'container_class'	=> '',
							'container_id'		=> '',
							'items_wrap'		=> '%3$s',
							'fallback_cb'		=> false,
							'walker'			=> new HPWP_Walker_Nav_Menu(),
						);
						wp_nav_menu( $args );


						// social stuffs
						$social_array = implode( ' ', display_social_media() );

						echo '<li class="social">'.
							$social_array .'
						</li>';
					?>

				</ul>
			</div>
		</div>

	</nav>

	<?php include( locate_template( 'inc/hpwp-jumbotron-logic.php' ) );

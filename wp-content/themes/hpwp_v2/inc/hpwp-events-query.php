<?php
    /**
     * Get Events
     *
     * Reusable function to get the events tiles
     * Used on events and solutions
     *
     * Only grabs dates that are in the future
     *
     * transient set to 15 minutes for all queries
     */
    function hpwp_events_query() {

        if( false === ( $event_query = get_transient( 'event_query_results' ) ) ) {

            $prefix = '_hpwp_';

            $current_year = date('Y');
            $next_next_year = date('Y', strtotime('+1 year'));

            $new_years_day = strtotime("$current_year-01-01");
            $new_years_eve = strtotime("$next_next_year-12-31");

            $args = array(
                'post_type'              => 'event',
                'posts_per_page'         => -1,
                'post_status'            => 'publish',
                'order'                  => 'ASC',
                'orderby'                => 'meta_value_num',
                'ignore_sticky_posts'    => true,
                'no_found_rows'          => false,
                'update_post_meta_cache' => false,
                'meta_query'             => array(
                    'relation' => 'AND',
                    array(
                        'key'     => $prefix . 'eventdate_start',
                        'value'   => array($new_years_day, $new_years_eve),
                        'compare' => 'BETWEEN',
                    ),
                    array(
                        'key'     => $prefix . 'events_no_show',
                        'compare' => 'NOT EXISTS'
                    )
                )
            );

            $event_query = new WP_Query( $args );
            set_transient( 'event_query_results', $event_query, 1 * MINUTE_IN_SECONDS );

        }

        return $event_query;

    }


    /**
     * get (display) events
     *
     * since the events are shown the same on multiple pages,
     * created a reusable function for displaying
     */
    function hpwp_get_events( $event_query, $no_past_events = false ) {

        $prefix = '_hpwp_';
        $is_past = false;

        if( $event_query->have_posts() ) {
            $past_ids = array();
            $future_ids = array();

            while( $event_query->have_posts() ) {
                $event_query->the_post();

                $eventid      = get_the_ID();
                $event_date   = get_post_meta( $eventid, $prefix . 'eventdate_start', 1 );
                $current_date = strtotime('now');

                if($event_date < $current_date) {
                    array_push($past_ids, $eventid);
                } else {
                    array_push($future_ids, $eventid);
                }
            }

            wp_reset_postdata();

            echo '<div class="module-evenrow">
                <div class="row">';

                    if(!empty($future_ids)) {
                        foreach($future_ids as $future_id) {
                            $display_date = get_display_date( $future_id );

                            echo '<div class="col-md-3">
                                <div class="tile-bg text-center text-mid tile-'. $future_id .'" style="background-image: linear-gradient( rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25) ), url( '. wp_get_attachment_image_url( get_post_meta( $future_id, $prefix . 'event_img_id', 1 ), 'event-tiles' ) .' );">
                                    <p class="text-bigger"><strong>'. $display_date .'</strong></p>
                                    <p>'. get_the_title($future_id) .'<br>'.
                                    get_post_meta( $future_id, $prefix . 'event_location', 1 ) .'</p>
                                </div>';

                                if( get_post_meta( $future_id, $prefix . 'sold_out', 1 ) ) {
                                    echo '<p class="text-center"><span class="redtext"><strong>SOLD OUT</strong></span></p>';
                                } else {
                                    echo '<p class="text-center"><a class="orangetext" href="'. get_permalink($future_id) .'"><strong>REGISTER</strong></a></p>';
                                }

                            echo '</div>';
                        }
                    } else {
                        echo '<p class="text-center">There are no scheduled events at this time.</p>';
                    }

                    /**
                     * if event is in the past, set `$is_past` var to true
                     * close row, and start new one
                     */
                    if(!empty($past_ids) && $no_past_events != true) {
                            echo '</div>
                        </div>
                        <h3 class="text-center">Past Events</h3>
                        <div class="module-evenrow">
                            <div class="row">';

                                foreach($past_ids as $past_id) {
                                    $display_date = get_display_date( $past_id );

                                    echo '<div class="col-md-3">
                                        <div class="tile-bg text-center text-mid tile-'. $past_id .'">
                                            <p class="text-bigger"><strong>'. $display_date .'</strong></p>
                                            <p>'. get_the_title($past_id) .'<br>'.
                                            get_post_meta( $past_id, $prefix . 'event_location', 1 ) .'</p>
                                        </div>
                                    </div>';
                                }
                    }

                echo '</div>
            </div>';
        }
    }

    /**
     * Get Coaching Events
     *
     * Reusable function to get the events tiles
     * Used on events and solutions
     *
     * Only grabs dates that are in the future
     *
     * transient set to 15 minutes for all queries
     */
    function hpwp_coaching_events_query() {

        if( false === ( $coaching_event_query = get_transient( 'coaching_event_query_results' ) ) ) {

            $prefix = '_hpwp_';

            $current_year = date('Y');
            $next_next_year = date('Y', strtotime('+1 year'));

            $new_years_day = strtotime("$current_year-01-01");
            $new_years_eve = strtotime("$next_next_year-12-31");

            $args = array(
                'post_type'              => 'coaching-event',
                'posts_per_page'         => -1,
                'post_status'            => 'publish',
                'order'                  => 'ASC',
                'orderby'                => 'meta_value_num',
                'ignore_sticky_posts'    => true,
                'no_found_rows'          => false,
                'update_post_meta_cache' => false,
                'meta_query'             => array(
                    array(
                        'key'     => $prefix . 'eventdate_start',
                        'value'   => array($new_years_day, $new_years_eve),
                        'compare' => 'BETWEEN',
                    ),
                )
            );

            $coaching_event_query = new WP_Query( $args );
            set_transient( 'coaching_event_query_results', $coaching_event_query, 1 * MINUTE_IN_SECONDS );

        }

        return $coaching_event_query;

    }

    /**
     * get (display) coaching events
     *
     * since the events are shown the same on multiple pages,
     * created a reusable function for displaying
     */
    function hpwp_get_coaching_events( $coaching_event_query, $no_past_events = false ) {

        $prefix = '_hpwp_';
        $is_past = false;

        if( $coaching_event_query->have_posts() ) {
            $past_ids = array();
            $future_ids = array();

            while( $coaching_event_query->have_posts() ) {
                $coaching_event_query->the_post();

                $eventid      = get_the_ID();
                $event_date   = intval(get_post_meta( $eventid, $prefix . 'eventdate_start', 1 ));
                $event_date_ymd = date('Ymd', $event_date);
                $current_date = strtotime('now');
                $current_date_ymd = date('Ymd', $current_date);

                if($event_date_ymd < $current_date_ymd) {
                    array_push($past_ids, $eventid);
                } else {
                    array_push($future_ids, $eventid);
                }
            }

            wp_reset_postdata();

            echo '<div class="module-evenrow">
                <div class="row">';

                    if(!empty($future_ids)) {
                        foreach($future_ids as $future_id) {
                            $display_date = get_display_date( $future_id );
                            $start_time = get_post_meta( $future_id, $prefix. 'eventtime_start', 1 ) ;
                            $end_time = get_post_meta( $future_id, $prefix. 'eventtime_end', 1 );
                            $event_time = '';

                            if( $start_time && $end_time ) {
                                $event_time = '<p class="text-smaller">'. $start_time .' &ndash; '. $end_time .'</p>';
                            }

                            echo '<div class="col-md-3">
                                <div class="tile-blue text-center text-mid tile-'. $future_id .'">
                                    <p class="text-bigger"><strong>'. $display_date .'</strong></p>'.
                                    $event_time .'
                                    <p class="margin-bottom-none">'. get_the_title($future_id) .'</p>
                                </div>';

                                if( get_post_meta( $future_id, $prefix . 'sold_out', 1 ) ) {
                                    echo '<p class="text-center"><span class="redtext"><strong>SOLD OUT</strong></span></p>';
                                } else {
                                    echo '<p class="text-center"><a class="orangetext" href="'. get_permalink($future_id) .'"><strong>REGISTER</strong></a></p>';
                                }

                            echo '</div>';
                        }
                    } else {
                        echo '<p class="text-center">There are no scheduled events at this time.</p>';
                    }

                    /**
                     * if event is in the past, set `$is_past` var to true
                     * close row, and start new one
                     */
                    /*
                    if(!empty($past_ids) && $no_past_events != true) {
                            echo '</div>
                        </div>
                        <h3 class="text-center">Past Events</h3>
                        <div class="module-evenrow">
                            <div class="row">';

                                foreach($past_ids as $past_id) {
                                    $display_date = get_display_date( $past_id );

                                    echo '<div class="col-md-3">
                                        <div class="tile-bg text-center text-mid tile-'. $past_id .'">
                                            <p class="text-bigger"><strong>'. $display_date .'</strong></p>
                                            <p>'. get_the_title($past_id) .'<br>'.
                                            get_post_meta( $past_id, $prefix . 'event_location', 1 ) .'</p>
                                        </div>
                                    </div>';
                                }
                    }
                    */

                echo '</div>
            </div>';
        }
    }

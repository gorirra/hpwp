<?php
/**
 * Jumbotron Logic
 */


if( is_404() ) {

	// don't show jumbotron

} elseif( is_page_template( 'page-templates/book.php' ) ) {	// is it a book page?

	// don't show jumbotron

} elseif( is_page_template( 'page-templates/landingpage.php' ) ) {	// is it a landing page?

	// don't show jumbotron

} elseif(
	get_post_type() !== 'event' &&
	get_post_type() !== 'webinar' &&
	get_post_type() !== 'coaching-event'
) {	// if it's NOT an event post (aka any other page)

	/**
	 * hiring team has a gradient background with a group of text
	 */
	if(
		is_page_template( 'page-templates/hiringteam.php' ) ||
		is_page_template( 'page-templates/hiringteam-v2.php' ) ||
		is_page_template( 'page-templates/hp-coaching.php' )
	) { ?>

		<section class="jumbotron jumbotron-hiringteam">
			<div class="container">
				<div class="content">

					<?php
						echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hero_headline', 1 ) ) .'

						<div class="jumbotron-hiringteam__txt">'.

							apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hero_txt', 1 ) ) .'

						</div>';
					?>

				</div>
			</div>
		</section>

	<?php } elseif(is_single()) {
		//echo "post";

		$hero_mobile = get_the_post_thumbnail_url(get_the_ID(), 'heromobile_img');
		//echo $hero_mobile;
		$hero_desktop = get_the_post_thumbnail_url(get_the_ID(), 'hero_img');
		//echo $hero_img;
		$hero_crop = get_the_post_thumbnail_url(get_the_ID(), 'hero_img');
		//echo $hero_crop;

		if( $hero_desktop != '' ) { ?>

			<style>

				<?php if( $hero_mobile != '' ) { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_mobile; ?> );
					}
					@media screen and (min-width: 640px ) {
						.jumbotron {
							background-image: url( <?php echo $hero_desktop; ?> );
						}
					}

				<?php } else { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_desktop; ?> );
					}

				<?php } ?>

			</style>

			<section class="jumbotron" style="background-image: url( <?php echo $hero_desktop; ?> );">
				<div class="container">
					<div class="content">

						<?php echo '<h1>'. get_the_title() .'</h1>';
							echo '<p>' . the_date() . '</p>';
						?>

					</div>
				</div>
			</section>

		<?php }

	} elseif(is_archive()) {
		$hero_mobile = get_the_post_thumbnail_url(get_the_ID(), 'heromobile_img');
		//echo $hero_mobile;
		$hero_desktop = get_the_post_thumbnail_url(get_the_ID(), 'hero_img');
		//echo $hero_img;
		$hero_crop = get_the_post_thumbnail_url(get_the_ID(), 'hero_img');
		//echo $hero_crop;


		if( $hero_desktop != '' ) { ?>

			<style>

				<?php if( $hero_mobile != '' ) { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_mobile; ?> );
					}
					@media screen and (min-width: 640px ) {
						.jumbotron {
							background-image: url( <?php echo $hero_desktop; ?> );
						}
					}

				<?php } else { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_desktop; ?> );
					}

				<?php } ?>

			</style>

			<section class="jumbotron" style="background-image: url( <?php echo $hero_desktop; ?> );">
				<div class="container">
					<div class="content">

						<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>

					</div>
				</div>
			</section>

		<?php }
	}

	else {

		$hero_mobile	= get_post_meta( $pageid, $prefix . 'heromobile_img', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'heromobile_img', 1 ) : '';
		$hero_desktop	= get_post_meta( $pageid, $prefix . 'hero_img', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'hero_img', 1 ) : '';
		$hero_crop = get_post_meta( $pageid, $prefix . 'hero_crop', 1 ) ? true : false;

		if( $hero_desktop != '' ) { ?>

			<style>

				<?php if( $hero_mobile != '' ) { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_mobile; ?> );

						<?php if( $hero_crop ) { ?>
							background-position: center bottom;
							background-size: contain;
							background-color: <?php echo get_post_meta( $pageid, $prefix . 'hero_bg_color', 1); ?>
						<?php } ?>
					}
					@media screen and (min-width: 640px ) {
						.jumbotron {
							background-image: url( <?php echo $hero_desktop; ?> );
						}
					}

				<?php } else { ?>

					.jumbotron {
						background-image: url( <?php echo $hero_desktop; ?> );
					}

				<?php } ?>

			</style>

			<?php
				$home_class = is_home() || is_front_page() ? ' jumbotron-home' : '';
				$masterclass_class = is_page_template('page-templates/master-class.php') ? ' jumbotron-masterclass' : '';
			?>

			<section class="jumbotron<?php echo $home_class; ?><?php echo $masterclass_class; ?>">
				<div class="container">
					<div class="content">

						<?php
							echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hero_headline', 1 ) );
							echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'hero_txt', 1 ) );

							if(is_page_template( 'page-templates/master-class.php' )) {
								$hero_left_col = get_post_meta($pageid, $prefix . 'hero_left_col', 1);
								$hero_right_col = get_post_meta($pageid, $prefix . 'hero_right_col', 1);

								if($hero_left_col != '') {
									echo '<div class="row">
										<div class="col-md-6">'.
											$hero_left_col .'
										</div>
										<div class="col-md-6">'.
											$hero_right_col .'
										</div>
									</div>';
								}
							}
						?>

					</div>
				</div>
			</section>

		<?php }
	}

} else {	// otherwise

	$hero_mobile	= hpwp_get_option( 'hpwp_reg_page_hero_mobile' ) != '' ? hpwp_get_option( 'hpwp_reg_page_hero_mobile' ) : '';
	$hero_desktop	= hpwp_get_option( 'hpwp_reg_page_hero' ) != '' ? hpwp_get_option( 'hpwp_reg_page_hero' ) : '';

	if( $hero_desktop != '' ) { ?>

		<style>

			<?php if( $hero_mobile != '' ) { ?>

				.jumbotron {
					background-image: url( <?php echo $hero_mobile; ?> );
				}
				@media screen and (min-width: 640px ) {
					.jumbotron {
						background-image: url( <?php echo $hero_desktop; ?> );
					}
				}

			<?php } else { ?>

				.jumbotron {
					background-image: url( <?php echo $hero_desktop; ?> );
				}

			<?php } ?>

		</style>

		<section class="jumbotron" style="background-image: url( <?php echo $hero_desktop; ?> );">
			<div class="container">
				<div class="content">

					<?php echo '<h1>'. hpwp_get_option( 'hpwp_reg_page_title' ) .'</h1>'; ?>

				</div>
			</div>
		</section>

	<?php }
}
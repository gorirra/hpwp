<?php
/**
 * Custom walker class.
 */
class HPWP_Walker_Nav_Menu extends Walker_Nav_Menu {

	/**
	 * Starts the list before the elements are added.
	 *
	 * Adds classes to the unordered list sub-menus.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// Depth-dependent classes.
		$indent			= ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth	= ( $depth + 1 ); // because it counts the first submenu as 0

		$classes = array(
			'dropdown-menu',
		);
		$class_names = implode( ' ', $classes );

		// Build HTML for output.
		$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
	}


	/**
	 * Start the element output.
	 *
	 * Adds main/sub-classes to the list items and links.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		// global $wp_query;
		$indent	= ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
		$caret	= '';

		if( $args->walker->has_children == 1 ) {
			$classes[]		= 'dropdown';
			$has_children	= 1;
		} else {
			// $classes[]	= '';
			$has_children	= 0;
		}

		$classes	= empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[]	= 'menu-item-' . $item->ID;

		$class_names	= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names	= $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


		// Build HTML.
		$output .= $indent . '<li' . $class_names . '>';

		// Link attributes.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		if( $has_children == 1 ) {
			$attributes .= ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';
			$caret = ' <span class="caret"></span>';
		}

		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$caret,
			$args->link_after,
			$args->after
		);
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
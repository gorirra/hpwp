<?php
/**
 * Shortcodes
 *
 * Adds custom shortcodes to the WordPress API.
 *
 */

class HPWP_Shortcodes {

	public static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}


	/**
	 * YouTube Embed
	 *
	 * Embeds the given YouTube video in the current post.
	 *
	 * Example:
	 * [youtube id="GjUXUN-F9Pg" /]
	 *
	 * @param array $atts User defined attributes in shortcode tag.
	 */
	public function youtube_embed( $atts = array() ) {

		$a = shortcode_atts(
			array(
				'id' => ''
			),
			$atts
		);

		if ( empty( $a['id'] ) ) {
			return;
		}

		return sprintf(
			'<div class="video-container">
				<iframe width="1280" height="720" src="https://www.youtube.com/embed/%s?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>',
			$a['id']
		);

	}


	/**
	 * Button/CTA Shortcode
	 *
	 * Adds a button
	 *
	 * Example:
	 * [button url="http://google.com" text="Google" (color="orange" blank="1" size="large")]
	 */
	public function button_shortcode( $atts ) {
		$a = shortcode_atts( array(
			'url'	=> '',
			'text'	=> '',
			'color' => 'default',
			'blank'	=> 0,
			'size'	=> '',
			'video' => 0
		), $atts );

		$blank = $a['blank'] == 1 ? ' target="_blank"' : '';
		$size = $a['size'] == 'large' ? ' btn-lg' : '';
		$video = $a['video'] == 1 ? ' data-fancybox' : '';

		return sprintf(
			'<a href="'. $a['url'] .'" class="btn btn-'. $a['color'] . $size .'"'. $blank . $video .'>'. $a['text'] .'</a>'
		);
	}


	/**
	 * Narrow Div Shortcode
	 *
	 * Surrounds content within a narrow-classed div
	 *
	 */
	public function narrow_shortcode( $atts, $content = null ) {
		return '<div class="narrow">'. do_shortcode( $content ) .'</div>';
	}


	/**
	 * Text Emphasis Shortcode
	 *
	 * Surrounds content within a text-emphasis-classed paragraph tag
	 *
	 */
	public function emphasis_shortcode( $atts, $content = null ) {
		return '<p class="text-emphasis">'. do_shortcode( $content ) .'</p>';
	}

	/**
	 * Text Bold Shortcode
	 *
	 * Surrounds content within a strong tag
	 *
	 */
	public function strong_shortcode( $atts, $content = null ) {
		return '<strong>'. do_shortcode( $content ) .'</strong>';
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		// register "youtube" shortcode
		add_shortcode( 'youtube', array( $this, 'youtube_embed' ) );

		// register cta shortcode
		add_shortcode( 'button', array( $this, 'button_shortcode' ) );

		// register narrow shortcode
		add_shortcode( 'narrow', array( $this, 'narrow_shortcode' ) );

		// register emphasis shortcode
		add_shortcode( 'emphasis', array( $this, 'emphasis_shortcode' ) );

		// register strong shortcode
		add_shortcode( 'strong', array( $this, 'strong_shortcode' ) );
	}
}

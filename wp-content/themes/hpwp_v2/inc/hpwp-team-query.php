<?php
	/**
	 * Get Team Members
	 *
	 * Reusable function to get the team member tiles
	 * Used on home and about pages
	 *
	 * transient set to 15 minutes for all queries
	 */
	function hpwp_get_team() {

		if( false === ( $team_query = get_transient( 'team_query_results' ) ) ) {

			$prefix = '_hpwp_';

			$args = array(
				'post_type'					=> 'team',
				'posts_per_page'			=> -1,
				'post_status'				=> 'publish',
				'order'						=> 'ASC',
				'orderby'					=> 'meta_value_num',
				'meta_key'					=> $prefix . 'order',
				'ignore_sticky_posts'		=> true,
				'no_found_rows'				=> false,
				'update_post_meta_cache'	=> false,
			);

			$team_query = new WP_Query( $args );
			set_transient( 'team_query_results', $team_query, 1 * MINUTE_IN_SECONDS );

		}

		return $team_query;

	}
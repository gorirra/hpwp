<?php
	/**
	 * Get Testimonials
	 *
	 * Reusable function to get the testimonials slider
	 * Used on home page and events
	 *
	 * @param  string  $type  either "home" or "leadership"
	 *
 	 * transient set to 1 minute for all queries
	 */
	function hpwp_testimonial_query( $type = '' ) {

		if( false === ( $testi_query = get_transient( 'testi_query_'. $type .'_results' ) ) ) {

			$args = array(
				'post_type'					=> 'testimonials',
				'posts_per_page'			=> -1,
				'post_status'				=> 'publish',
				'ignore_sticky_posts'		=> true,
				'no_found_rows'				=> false,
				'update_post_meta_cache'	=> false,
				'meta_query'				=> array(
					array(
						'key'	=> '_hpwp_testi_type',
						'value'	=> $type,
					)
				),
			);

			$testi_query = new WP_Query( $args );
			set_transient( 'testi_query_'. $type .'_results', $testi_query, 1 * MINUTE_IN_SECONDS );

		}

		return $testi_query;

	}


	/**
	 * display full testimonials in carousel
	 */
	function hpwp_get_testimonials( $testi_query ) {

		$prefix = '_hpwp_';

		if( $testi_query->have_posts() ) {

			echo '<div class="row">
				<div class="owl-carousel owl-carousel__multicolortile">';

					while( $testi_query->have_posts() ) {
						$testi_query->the_post();

						$testiid = get_the_ID();

						$logo = get_post_meta( $testiid, $prefix . 'testi_logo', 1 ) != '' ? '<p>'. wp_get_attachment_image( get_post_meta( $testiid, $prefix . 'testi_logo_id', 1 ), 'thumbnail', null, array( 'class' => 'aligncenter' ) ) .'</p>' : '';

						echo '<div class="tile-white">'.
							$logo .
							apply_filters( 'the_content', get_post_meta( $testiid, $prefix . 'testimonial', 1 ) ) .'
						</div>';

					}

				echo '</div>
				<div class="owl-carousel__customnav"></div>
			</div>';

			wp_reset_postdata();

		}
	}


	/**
	 * display only testimonial logos in carousel
	 * not needed anymore
	 */
	/*function hpwp_get_testimonial_logos( $testi_query ) {

		$prefix = '_hpwp_';

		if( $testi_query->have_posts() ) {

			echo '<section class="module module-white">
				<div class="container">
					<div class="owl-carousel owl-carousel__five">';

						while( $testi_query->have_posts() ) {
							$testi_query->the_post();

							$testiid = get_the_ID();

							$logo = get_post_meta( $testiid, $prefix . 'testi_logo', 1 ) != '' ? wp_get_attachment_image( get_post_meta( $testiid, $prefix . 'testi_logo_id', 1 ), 'thumbnail', null, array( 'class' => 'img-responsive aligncenter' ) ) : '';

							echo '<div>'.
								$logo .'
							</div>';

						}

					echo '</div>
					<div class="owl-carousel__customnav"></div>
				</div>
			</section>';

			wp_reset_postdata();

		}
	}*/
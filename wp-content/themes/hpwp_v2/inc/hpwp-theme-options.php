<?php
/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'hpwp_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function hpwp_register_theme_options_metabox() {

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'hpwp_option_metabox',
		'title'        => esc_html__( 'Site Options', 'hpwp' ),
		'object_types' => array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'hpwp_options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'hpwp' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'hpwp' ), // The text for the options-page save button. Defaults to 'Save'.
	) );

	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */
	$cmb_options->add_field( array(
		'name'	=> __( 'Logos', 'cmb2' ),
		'id'	=> 'logo_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'    => 'Header Logo (Main)',
		'desc'    => 'Upload an image or enter an URL. SVG file type preferred.',
		'id'      => 'main_logo',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
	) );
	$cmb_options->add_field( array(
		'name'    => 'Footer Logo (White)',
		'desc'    => 'Upload an image or enter an URL. SVG file type preferred.',
		'id'      => 'footer_logo',
		'type'    => 'file',
		// Optional:
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
	) );


	$cmb_options->add_field( array(
		'name'	=> __( 'Social Media', 'cmb2' ),
		'id'	=> 'socialmedia_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Google+ URL', 'cmb2' ),
		'id'	=> 'googleplus_url',
		'type'	=> 'text_url',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Facebook URL', 'cmb2' ),
		'id'	=> 'facebook_url',
		'type'	=> 'text_url',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Twitter URL', 'cmb2' ),
		'id'	=> 'twitter_url',
		'type'	=> 'text_url',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Instagram URL', 'cmb2' ),
		'id'	=> 'instagram_url',
		'type'	=> 'text_url',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'LinkedIn URL', 'cmb2' ),
		'id'	=> 'linkedin_url',
		'type'	=> 'text_url',
	) );


	$cmb_options->add_field( array(
		'name'	=> __( 'Company Info', 'cmb2' ),
		'id'	=> 'company_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Address', 'cmb2' ),
		'id'	=> 'address',
		'type'	=> 'textarea',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Email Address', 'cmb2' ),
		'id'	=> 'hpwp_email',
		'type'	=> 'text_email',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Phone Number', 'cmb2' ),
		'id'	=> 'hpwp_phone',
		'type'	=> 'text',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Legal Page URL', 'cmb2' ),
		'id'	=> 'legal_url',
		'type'	=> 'text_url',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Register Pages', 'cmb2' ),
		'id'	=> 'register_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'    => 'Register Page Hero Image',
		'desc'    => 'Upload an image or enter an URL. Dimensions: 1920x600 pixels.',
		'id'      => 'hpwp_reg_page_hero',
		'type'    => 'file',
	) );
	$cmb_options->add_field( array(
		'name'    => 'Register Page Hero Image (MOBILE)',
		'desc'    => 'Upload an image or enter an URL. Dimensions: 640x450 pixels.',
		'id'      => 'hpwp_reg_page_hero_mobile',
		'type'    => 'file',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Register Page Title', 'cmb2' ),
		'id'	=> 'hpwp_reg_page_title',
		'type'	=> 'text',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Register Page Gravity Form', 'cmb2' ),
		'id'	=> 'hpwp_reg_page_form',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Register Page Gravity Form (Sans Cancellation)', 'cmb2' ),
		'id'	=> 'hpwp_reg_page_form_nocancel',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Register Page Gravity Form - Coaching', 'cmb2' ),
		'id'	=> 'hpwp_reg_page_form_coach',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Register Page Gravity Form - Coaching (Sans Cancellation)', 'cmb2' ),
		'id'	=> 'hpwp_reg_page_form_coach_nocancel',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Webinar Register Page Gravity Form', 'cmb2' ),
		'id'	=> 'hpwp_webinar_reg_page_form',
		'type'	=> 'textarea_code',
	) );

	// $cmb_options->add_field( array(
	// 	'name'	=> __( 'Featured Testimonials Module', 'cmb2' ),
	// 	'id'	=> 'featured_testi_title',
	// 	'type'	=> 'title',
	// ) );

	// $cmb_options->add_field( array(
	// 	'name'		=> __( 'Title', 'cmb2' ),
	// 	'id'		=> 'hpwp_testimonials_title',
	// 	'type'		=> 'text',
	// 	'default'	=> 'Featured Testimonials',
	// ) );

	// $cmb_options->add_field( array(
	// 	'name'	=> __( 'Text', 'cmb2' ),
	// 	'id'	=> 'hpwp_testimonials_text',
	// 	'type'	=> 'wysiwyg',
	// ) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Module Titles', 'cmb2' ),
		'id'	=> 'module_titles',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Workshop Schedule Title', 'cmb2' ),
		'id'	=> 'hpwp_event_dates_title',
		'type'	=> 'text',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Coaching Schedule Title', 'cmb2' ),
		'id'	=> 'hpwp_coachingevent_dates_title',
		'type'	=> 'text',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Testimonials Title', 'cmb2' ),
		'id'	=> 'hpwp_testimonials_title',
		'type'	=> 'text',
	) );

	$cmb_options->add_field( array(
		'name'		=> __( 'Testimonials Text', 'cmb2' ),
		'id'		=> 'hpwp_testimonials_text',
		'type'		=> 'wysiwyg',
		'options'	=> array(
			'textarea_rows' => 5,
		)
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Contact Form', 'cmb2' ),
		'id'	=> 'contact_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Contact Gravity Form', 'cmb2' ),
		'id'	=> 'hpwp_contact_form',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Constant Contact', 'cmb2' ),
		'id'	=> 'cc_subscribe_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Subscribe Form', 'cmb2' ),
		'id'	=> 'hpwp_cc_subscribe_form',
		'type'	=> 'textarea_code',
	) );

	$cmb_options->add_field( array(
		'name'	=> __( 'Misc.', 'cmb2' ),
		'id'	=> 'misc_title',
		'type'	=> 'title',
	) );
	$cmb_options->add_field( array(
		'name'	=> __( 'Client Page ID', 'cmb2' ),
		'id'	=> 'hpwp_client_page_id',
		'type'	=> 'text',
	) );
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function hpwp_get_option( $key = '', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( 'hpwp_options', $key, $default );
	}

	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( 'hpwp_options', $default );

	$val = $default;

	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}

	return $val;
}

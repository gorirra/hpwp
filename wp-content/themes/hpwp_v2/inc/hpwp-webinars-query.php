<?php
    /**
     * Get Webinars
     *
     * Reusable function to get the webinars tiles
     * Used on webinars
     *
     * Only grabs dates that are in the future
     *
     * transient set to 15 minutes for all queries
     */
    function hpwp_webinars_query() {

        if( false === ( $webinar_query = get_transient( 'webinar_query_results' ) ) ) {

            $prefix = '_hpwp_';

            $current_year = date('Y');
            $next_next_year = date('Y', strtotime('+1 year'));

            $new_years_day = strtotime("$current_year-01-01");
            $new_years_eve = strtotime("$next_next_year-12-31");

            $args = array(
                'post_type'              => 'webinar',
                'posts_per_page'         => -1,
                'post_status'            => 'publish',
                'order'                  => 'ASC',
                'orderby'                => 'meta_value_num',
                'ignore_sticky_posts'    => true,
                'no_found_rows'          => false,
                'update_post_meta_cache' => false,
                'meta_query'             => array(
                    'relation' => 'AND',
                    array(
                        'key'     => $prefix . 'eventdate_start',
                        'value'   => array($new_years_day, $new_years_eve),
                        'compare' => 'BETWEEN',
                    ),
                    array(
                        'key'     => $prefix . 'events_no_show',
                        'compare' => 'NOT EXISTS'
                    )
                )
            );

            $webinar_query = new WP_Query( $args );
            set_transient( 'webinar_query_results', $webinar_query, 1 * MINUTE_IN_SECONDS );

        }

        return $webinar_query;

    }


    /**
     * Get (display) events
     *
     * since the events are shown the same on multiple pages,
     * created a reusable function for displaying
     */
    function hpwp_get_webinars( $webinar_query, $no_past_events = false ) {

        $prefix = '_hpwp_';
        $is_past = false;

        if( $webinar_query->have_posts() ) {
            $past_ids = array();
            $future_ids = array();

            while( $webinar_query->have_posts() ) {
                $webinar_query->the_post();

                $eventid      = get_the_ID();
                $event_date   = get_post_meta( $eventid, $prefix . 'eventdate_start', 1 );
                $current_date = strtotime('now');

                if($event_date < $current_date) {
                    array_push($past_ids, $eventid);
                } else {
                    array_push($future_ids, $eventid);
                }
            }

            wp_reset_postdata();

            echo '<div class="module-evenrow">
                <div class="row">';

                    if(!empty($future_ids)) {
                        foreach($future_ids as $future_id) {
                            $display_date = get_display_date( $future_id );

                            echo '<div class="col-md-3">
                                <div class="tile-bg text-center text-mid tile-'. $future_id .'" style="background-image: linear-gradient( rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25) ), url( '. wp_get_attachment_image_url( get_post_meta( $future_id, $prefix . 'event_img_id', 1 ), 'event-tiles' ) .' );">
                                    <p class="text-bigger"><strong>'. $display_date .'</strong></p>
                                    <p class="margin-top-small">'. get_the_title($future_id) .'</p>
                                </div>';

                                if( get_post_meta( $future_id, $prefix . 'sold_out', 1 ) ) {
                                    echo '<p class="text-center"><span class="redtext"><strong>SOLD OUT</strong></span></p>';
                                } else {
                                    echo '<p class="text-center"><a class="orangetext" href="'. get_permalink($future_id) .'"><strong>REGISTER</strong></a></p>';
                                }

                            echo '</div>';
                        }
                    } else {
                        echo '<p class="text-center">There are no scheduled events at this time.</p>';
                    }

                    /**
                     * if event is in the past, set `$is_past` var to true
                     * close row, and start new one
                     */
                    if(!empty($past_ids) && $no_past_events != true) {
                            echo '</div>
                        </div>
                        <h3 class="text-center">Past Events</h3>
                        <div class="module-evenrow">
                            <div class="row">';

                                foreach($past_ids as $past_id) {
                                    $display_date = get_display_date( $past_id );

                                    echo '<div class="col-md-3">
                                        <div class="tile-bg text-center text-mid tile-'. $past_id .'">
                                            <p class="text-bigger"><strong>'. $display_date .'</strong></p>
                                            <p>'. get_the_title($past_id) .'<br>'.
                                            get_post_meta( $past_id, $prefix . 'event_location', 1 ) .'</p>
                                        </div>
                                    </div>';
                                }
                    }

                echo '</div>
            </div>';
        }
    }

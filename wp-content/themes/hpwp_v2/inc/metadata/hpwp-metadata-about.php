<?php
/**
 * Custom Metaboxes | About Page
 */
class HPWP_Metadata_AboutPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_aboutpage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'about_page',
			'title'			=> __( 'About Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/about.php', 'page-templates/about-v2.php' ) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		// $cmb->add_field( array(
		// 	'name'		=> '"Our Team" Title',
		// 	'type'		=> 'text',
		// 	'id'		=> $prefix . 'ourteam_txt',
		// 	'default'	=> 'Our Team',
		// ) );

		$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'teamobjectives_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Objective {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Objective', 'cmb2' ),
				'remove_button' => __( 'Remove Objective', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		// $cmb->add_group_field( $group_field_id, array(
		// 	'name' => 'Objective Image',
		// 	'id'   => 'image',
		// 	'type' => 'file',
		// ) );

		$cmb->add_group_field( $group_field_id, array(
			'name'		=> 'Objective Description',
			'id'		=> 'description',
			'type'		=> 'wysiwyg',
			'options'	=> array(
				'textarea_rows' => 5,
			)
		) );

		$cmb->add_field( array(
			'name'    => 'Clients List Intro',
			'id'      => $prefix . 'clients_list_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_aboutpage_metaboxes' ) );
	}
}

<?php
/**
 * Custom Metaboxes | Alumni Landing Page
 */
class HPWP_Metadata_AlumniLandingPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_alumnilandingpage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'alumni_landing',
			'title'			=> __( 'Alumni Landing Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/alumni-landing.php' ) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field(array(
            'name' => 'Background Image',
            'desc' => 'Upload an image or enter an URL.',
            'id'   => $prefix . 'bg',
            'type' => 'file',
        ));

		$cmb->add_field( array(
			'name'		=> 'First Line',
			'type'		=> 'text',
			'id'		=> $prefix . 'first',
			'default'	=> 'We Are So Excited!',
		) );

		$cmb->add_field( array(
			'name'    => 'Body Content',
			'id'      => $prefix . 'content',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'		=> 'Link 1 - Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'link1_title',
		) );

		$cmb->add_field( array(
			'name' => 'Link 1 - URL',
			'id'   => $prefix . 'link1_url',
			'type' => 'text_url',
		) );

		$cmb->add_field( array(
			'name'		=> 'Link 2 - Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'link2_title',
		) );

		$cmb->add_field( array(
			'name' => 'Link 2 - URL',
			'id'   => $prefix . 'link2_url',
			'type' => 'text_url',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_alumnilandingpage_metaboxes' ) );
	}
}

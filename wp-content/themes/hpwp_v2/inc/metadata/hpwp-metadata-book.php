<?php
/**
 * Custom Metaboxes | Book Page
 */
class HPWP_Metadata_BookPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_bookpage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * body
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'book_page',
			'title'			=> __( 'Book Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/book.php', 'page-templates/landingpage.php' ) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name'		=> 'Available Date - Callout',
			'type'		=> 'text',
			'id'		=> $prefix . 'avail_date_txt',
			'default'	=> 'Available July 9',
		) );

		$cmb->add_field( array(
			'name'    => 'Book Section Word Art',
			'desc'    => 'Upload an image or enter an URL.',
			'id'      => $prefix . 'book_word_art',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Under Word Art Text',
			'id'      => $prefix . 'book_word_art_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'    => 'Book Cover',
			'desc'    => 'tilted book cover',
			'id'      => $prefix . 'book_tilt',
			'type'    => 'file',
		) );

		$stores = $cmb->add_field( array(
			'id'          => $prefix . 'stores',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Store {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Store', 'cmb2' ),
				'remove_button' => __( 'Remove Store', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

			$cmb->add_group_field( $stores, array(
				'name' => 'Store Logo',
				'id'   => 'image',
				'type' => 'file',
			) );

			$cmb->add_group_field( $stores, array(
				'name' => 'Store URL',
				'id'   => 'url',
				'type' => 'text_url',
			) );

		$cmb->add_field( array(
			'name'    => 'Book Cover',
			'desc'    => 'straight-on book cover',
			'id'      => $prefix . 'book_straight',
			'type'    => 'file',
		) );

		// $cmb->add_field( array(
		// 	'name'    => 'Free Chapter PDF',
		// 	'id'      => $prefix . 'free_chapter',
		// 	'type'    => 'file',
		// 	'query_args' => array(
		// 		'type' => 'application/pdf', // Make library only display PDFs.
		// 	),
		// ) );

		$cmb->add_field( array(
			'name'    => '"Download a Free Chapter" Button',
			'id'      => $prefix . 'download_free',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'    => '"About the Book" Text',
			'id'      => $prefix . 'aboutbook_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'    => '"What can you Expect" Text',
			'desc'    => 'include unordered list and cta',
			'id'      => $prefix . 'expect_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'    => '"What can you Expect" Right Image',
			'id'      => $prefix . 'expect_img',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Authors Title',
			'default' => 'Get To Know the Authors',
			'id'      => $prefix . 'authors_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Author Image Left',
			'id'      => $prefix . 'author_left_img',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Author Image Right',
			'id'      => $prefix . 'author_right_img',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Authors Text',
			'id'      => $prefix . 'authors_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

		$cmb->add_field( array(
			'name'    => 'Quotes Title',
			'id'      => $prefix . 'quotes_title',
			'default' => 'Praise for <em>Creating the High Performance Workplace</em>',
			'type'    => 'textarea_code',
		) );

		$quotes = $cmb->add_field( array(
			'id'          => $prefix . 'praise',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Quote {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Quote', 'cmb2' ),
				'remove_button' => __( 'Remove Quote', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

			$cmb->add_group_field( $quotes, array(
				'name' => 'Photo',
				'id'   => 'image',
				'type' => 'file',
			) );

			$cmb->add_group_field( $quotes, array(
				'name' => 'Name',
				'id'   => 'fullname',
				'type' => 'text',
			) );

			$cmb->add_group_field( $quotes, array(
				'name' => 'Title',
				'id'   => 'jobtitle',
				'type' => 'text',
			) );

			$cmb->add_group_field( $quotes, array(
				'name' => 'Quote',
				'id'   => 'quote',
				'type' => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5
				)
			) );

		// $cmb->add_field( array(
		// 	'name'    => '"Order Now" CTA',
		// 	'default' => 'Order Now',
		// 	'id'      => $prefix . 'ordernow_cta',
		// 	'type'    => 'text',
		// ) );

		// $cmb->add_field( array(
		// 	'name'    => '"Who Needs This Book" Title',
		// 	'default' => 'Who Needs This Book?',
		// 	'id'      => $prefix . 'who_title',
		// 	'type'    => 'text',
		// ) );

		$cmb->add_field( array(
			'name'    => '"Why" Text',
			'id'      => $prefix . 'who_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			)
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_bookpage_metaboxes' ) );
	}
}

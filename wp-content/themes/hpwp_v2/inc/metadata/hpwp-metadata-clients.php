<?php
/**
 * Custom Metaboxes | Clients Page
 */
class HPWP_Metadata_ClientsPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_clientspage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'clients_page',
			'title'			=> __( 'Clients Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => 'page-templates/clients.php' ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$group_field_id = $cmb->add_field( array(
			'id'			=> $prefix . 'client_group',
			'type'			=> 'group',
			'desc'			=> 'Clients',
			// 'repeatable'	=> false, // use false if you want non-repeatable group
			'options'		=> array(
				'group_title'	=> __( 'Client {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'	=> __( 'Add Another Client', 'cmb2' ),
				'remove_button'	=> __( 'Remove Client', 'cmb2' ),
				'sortable'		=> true, // beta
				// 'closed'		=> true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'	=> 'Client Logo',
			'id'	=> 'image',
			'desc'	=> 'dimensions: 200x150 pixels',
			'type'	=> 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Case Study Module Title',
			'default' => 'Download a Case Study',
			'id'      => $prefix . 'casestudy_title',
			'type'    => 'text',
		) );

		$group_field_id = $cmb->add_field( array(
			'id'			=> $prefix . 'casestudy_group',
			'type'			=> 'group',
			'desc'			=> 'Case Studies',
			// 'repeatable'	=> false, // use false if you want non-repeatable group
			'options'		=> array(
				'group_title'	=> __( 'Case Study {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'	=> __( 'Add Another Case Study', 'cmb2' ),
				'remove_button'	=> __( 'Remove Case Study', 'cmb2' ),
				'sortable'		=> true, // beta
				// 'closed'		=> true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'	=> 'Company Name',
			'id'	=> 'name',
			'type'	=> 'text',
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'	=> 'Subtitle',
			'id'	=> 'subtitle',
			'type'	=> 'text',
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'		=> 'Description',
			'id'		=> 'description',
			'type'		=> 'wysiwyg',
			'options'	=> array(
				'textarea_rows' => 5,
			)
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'	=> 'PDF',
			'id'	=> 'pdf',
			'type'	=> 'file',
			'query_args' => array(
				'type' => 'application/pdf', // Make library only display PDFs.
			),
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_clientspage_metaboxes' ) );
	}
}

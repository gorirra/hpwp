<?php
/**
 * Custom Metaboxes | Events Page
 */
class HPWP_Metadata_EventsPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_eventspage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'events_page',
			'title'			=> __( 'Events Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/events.php', 'page-templates/events-v2.php' ) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name'    => 'Left Content - Title',
			'id'      => $prefix . 'left_content_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name' => 'Left Content',
			'id'   => $prefix . 'left_content',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name' => 'Right Content',
			'id'   => $prefix . 'right_content',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name'    => 'CTA - Left',
			'desc'    => 'use [button] shortcode to create orange cta',
			'id'      => $prefix . 'cta_left',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'CTA - Right',
			'desc'    => 'use [button] shortcode to create orange cta',
			'id'      => $prefix . 'cta_right',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Sub-Hero Image',
			'desc'    => 'Upload an image or enter an URL. Dimensions: 1920x600 px',
			'id'      => $prefix . 'subhero_img',
			'type'    => 'file',
			'options' => array(
				'url' => false, // Hide the text input for the url
			),
			'preview_size' => 'medium', // Image size to use when previewing in the admin.
		) );

		$cmb->add_field( array(
			'name'    => 'Sub-Hero Image (MOBILE)',
			'desc'    => 'Upload an image or enter an URL. Dimensions: 640x450 px',
			'id'      => $prefix . 'subheromobile_img',
			'type'    => 'file',
			'options' => array(
				'url' => false, // Hide the text input for the url
			),
			'preview_size' => 'medium', // Image size to use when previewing in the admin.
		) );

		$cmb->add_field( array(
			'name'    => 'Sub-Hero Headline',
			'desc'    => 'wrap in an h1 tag',
			'id'      => $prefix . 'subhero_headline',
			'type'    => 'wysiwyg',
			'options' => array(
				'media_buttons' => false,
				'textarea_rows' => 1
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Sub-Hero Text',
			'desc'    => '',
			'id'      => $prefix . 'subhero_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'media_buttons' => false,
				'textarea_rows' => 3
			),
		) );

		$cmb->add_field( array(
			'name'		=> 'Event Dates',
			'id'		=> $prefix . 'event_dates_title',
			'type'		=> 'text',
			'default'	=> 'Event Dates'
		) );

		/*$cmb->add_field( array(
			'name'		=> 'Testimonials Section Title',
			'id'		=> $prefix . 'testi_title',
			'type'		=> 'text',
			'default'	=> 'What Others Are Saying'
		) );

		$cmb->add_field( array(
			'name'		=> 'Testimonials Section Text',
			'id'		=> $prefix . 'testi_txt',
			'type'		=> 'wysiwyg',
		) );*/

		$cmb->add_field( array(
			'name'		=> 'YouTube ID',
			'id'		=> $prefix . 'youtube_id',
			'type'		=> 'text'
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_eventspage_metaboxes' ) );
	}
}

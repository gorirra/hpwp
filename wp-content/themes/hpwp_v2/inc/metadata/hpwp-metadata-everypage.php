<?php
/**
 * Custom Metaboxes | Every Page
 */
class HPWP_Metadata_EveryPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_everypage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'every_page',
			'title'			=> __( 'HPWP Page Hero Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'exclude_template', 'value' => array( 'page-templates/book.php', 'page-templates/alumni-landing.php') ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name'    => 'Hero Image',
			'desc'    => 'Upload an image or enter an URL. Dimensions: 1920x600 px',
			'id'      => $prefix . 'hero_img',
			'type'    => 'file',
			'options' => array(
				'url' => false, // Hide the text input for the url
			),
			'preview_size' => 'medium', // Image size to use when previewing in the admin.
		) );

		$cmb->add_field( array(
			'name'    => 'Hero Image (MOBILE)',
			'desc'    => 'Upload an image or enter an URL. Dimensions: 640x450 px',
			'id'      => $prefix . 'heromobile_img',
			'type'    => 'file',
			'options' => array(
				'url' => false, // Hide the text input for the url
			),
			'preview_size' => 'medium', // Image size to use when previewing in the admin.
		) );

		$cmb->add_field( array(
			'name'    => 'Heads getting cropped?',
			'desc'    => 'check ON if hero is cropping off heads',
			'id'      => $prefix . 'hero_crop',
			'type'    => 'checkbox',
		) );

		$cmb->add_field( array(
			'name'    => 'Hero Background Color',
			'desc'    => 'if the above option is checked ON, choose a color that suits the hero image',
			'id'      => $prefix . 'hero_bg_color',
			'type'    => 'colorpicker',
			'default' => '#ffffff',
		) );

		$cmb->add_field( array(
			'name'    => 'Hero Headline',
			'desc'    => 'wrap in an h1 tag',
			'id'      => $prefix . 'hero_headline',
			'type'    => 'wysiwyg',
			'options' => array(
				'media_buttons' => false,
				'textarea_rows' => 1
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Hero Text',
			'desc'    => '',
			'id'      => $prefix . 'hero_txt',
			'type'    => 'wysiwyg',
			'options' => array(
				'media_buttons' => false,
				'textarea_rows' => 3
			),
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_everypage_metaboxes' ) );
	}
}

<?php
/**
 * Custom Metaboxes | Hiring Team Page
 */
class HPWP_Metadata_HiringTeamPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_hiringteampage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'hiringteam_page',
			'title'			=> __( 'Hiring Team Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array(
				'page-templates/hiringteam.php',
				'page-templates/hiringteam-v2.php',
				'page-templates/hp-coaching.php',
			) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name' => 'Section 1 - Left Content',
			'id'   => $prefix . 'left_content_s1',
			'desc'	=> 'First grouping: left content, right image',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name' => 'Section 1 - Right Image',
			'id'   => $prefix . 'right_img_s1',
			'type' => 'file',
		) );

		// $cmb->add_field( array(
		// 	'name' => 'Section 2 - Left Image',
		// 	'id'   => $prefix . 'left_img_s2',
		// 	'desc'	=> 'Second grouping: left image, right content',
		// 	'type' => 'file',
		// ) );

		$cmb->add_field( array(
			'name' => 'Section 2 - Left Video',
			'id'   => $prefix . 'left_video_s2',
			'desc'	=> 'Enter YouTube ID',
			'type' => 'text',
		) );

		$cmb->add_field( array(
			'name' => 'Section 2 - Right Content',
			'id'   => $prefix . 'right_content_s2',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name' => 'Section 3 - Left Content',
			'desc' => 'Third grouping: left content, right form',
			'id'   => $prefix . 'left_content_s3',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name' => 'Section 3 - Form',
			'id'   => $prefix . 'form_s3',
			'type' => 'textarea_code',
		) );

		$cmb->add_field( array(
			'name' => 'Section 4 - Title',
			'desc' => 'Fourth (last) grouping: title with 3 columns',
			'id'   => $prefix . 'title_s4',
			'type' => 'text',
		) );

		$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'group_s4',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Column {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Column', 'cmb2' ),
				'remove_button' => __( 'Remove Column', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

			$cmb->add_group_field( $group_field_id, array(
				'name' => 'Column Image',
				'id'   => 'image',
				'type' => 'file',
			) );

			$cmb->add_group_field( $group_field_id, array(
				'name'        => 'Column Text',
				'id'          => 'column_txt',
				'type'        => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 5
				)
			) );


	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_hiringteampage_metaboxes' ) );
	}
}

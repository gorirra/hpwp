<?php
/**
 * Custom Metaboxes | Home Page Book
 */
class HPWP_Metadata_HomePageBook
{

    static $instance = false;

    public function __construct()
    {

        $this->_add_actions();

    }

    public function hpwp_homepagebook_metaboxes()
    {

        // Start with an underscore to hide fields from custom fields list
        $prefix = '_hpwp_';

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box(array(
            'id'           => 'home_page_book',
            'title'        => __('Home Page (Book) Options', 'hpwp'),
            'object_types' => array('page'), // Post type
            'show_on'      => array('key' => 'page-template', 'value' => array('page-templates/homepage-book.php')),
            'context'      => 'advanced',
            'priority'     => 'high',
            'show_names'   => true, // Show field names on the left
            'closed'       => false,
        ));

        $cmb->add_field(array(
            'name' => __('Announcement', 'cmb2'),
            'id'   => $prefix . 'banner_title',
            'type' => 'title',
        ));

        $cmb->add_field(array(
            'name' => __('Announcement Text', 'cmb2'),
            'id'   => $prefix . 'banner_txt',
            'type' => 'textarea_small',
        ));
        $cmb->add_field(array(
            'name' => __('Announcement CTA', 'cmb2'),
            'id'   => $prefix . 'banner_cta',
            'type' => 'text',
        ));
        $cmb->add_field(array(
            'name' => __('Announcement URL', 'cmb2'),
            'id'   => $prefix . 'banner_url',
            'type' => 'text_url',
        ));

        $cmb->add_field(array(
            'name' => 'Book Section Word Art',
            'desc' => 'Upload an image or enter an URL.',
            'id'   => $prefix . 'book_word_art',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name' => 'YouTube ID',
            'id'   => $prefix . 'youtube_id',
            'type' => 'text',
        ));

        // $cmb->add_field( array(
        //     'name'        => 'Form Title',
        //     'type'        => 'text',
        //     'id'        => $prefix . 'form_title',
        //     'default'    => 'Stay Updated',
        // ) );

        // $cmb->add_field( array(
        //     'name'        => 'Form Shortcode',
        //     'type'        => 'textarea_code',
        //     'id'        => $prefix . 'book_form',
        // ) );
        $cmb->add_field(array(
            'name' => 'Book Cover',
            'desc' => 'Upload an image or enter an URL.',
            'id'   => $prefix . 'book_cover',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name'    => '"What We Do?" Title',
            'type'    => 'text',
            'id'      => $prefix . 'whatwedo_txt',
            'default' => 'What We Do?',
        ));

        $group_field_id = $cmb->add_field(array(
            'id'      => $prefix . 'whatwedo_group',
            'type'    => 'group',
            // 'description' => __( 'Generates reusable form entries', 'cmb2' ),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options' => array(
                'group_title'   => __('Specialty {#}', 'cmb2'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Specialty', 'cmb2'),
                'remove_button' => __('Remove Specialty', 'cmb2'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        ));

        $cmb->add_group_field($group_field_id, array(
            'name'    => 'Specialty Description',
            'id'      => 'description',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ));

        $cmb->add_group_field($group_field_id, array(
            'name' => 'Specialty Image',
            'id'   => 'image',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name'    => 'HPWP "Results"',
            'desc'    => 'use h3 and bullet points',
            'id'      => $prefix . 'results',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ));

        $cmb->add_field(array(
            'name' => 'HPWP "Results" Image',
            'desc' => 'dimensions: 1920x400',
            'id'   => $prefix . 'results_img',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name'    => 'Available Date - Callout',
            'type'    => 'text',
            'id'      => $prefix . 'avail_date_txt',
            'default' => 'Available July 9',
        ));

        $cmb->add_field(array(
            'name'    => 'Under Word Art Text',
            'id'      => $prefix . 'book_word_art_txt',
            'type'    => 'wysiwyg',
            'options' => array(
                'textarea_rows' => 5,
            ),
        ));

        $cmb->add_field(array(
            'name' => 'Book Cover',
            'desc' => 'tilted book cover',
            'id'   => $prefix . 'book_tilt',
            'type' => 'file',
        ));

        $stores = $cmb->add_field(array(
            'id'      => $prefix . 'stores',
            'type'    => 'group',
            'options' => array(
                'group_title'   => __('Store {#}', 'cmb2'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Store', 'cmb2'),
                'remove_button' => __('Remove Store', 'cmb2'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        ));

        $cmb->add_group_field($stores, array(
            'name' => 'Store Logo',
            'id'   => 'image',
            'type' => 'file',
        ));

        $cmb->add_group_field($stores, array(
            'name' => 'Store URL',
            'id'   => 'url',
            'type' => 'text_url',
        ));

        $cmb->add_field(array(
            'name' => 'Book Cover',
            'desc' => 'straight-on book cover',
            'id'   => $prefix . 'book_straight',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name'    => '"Team" Title',
            'type'    => 'text',
            'id'      => $prefix . 'team_title',
            'default' => 'Team',
        ));

        $cmb->add_field(array(
            'name' => 'Bottom Call to Action Background Image',
            'desc' => 'dimensions: 1920x400',
            'id'   => $prefix . 'bottom_bg',
            'type' => 'file',
        ));

        $cmb->add_field(array(
            'name' => 'Bottom Call to Action Text',
            'type' => 'wysiwyg',
            'desc' => 'use h2 tag, use the [button] shortcode to create a cta',
            'id'   => $prefix . 'bottom_cta',
        ));
    }

    /**
     * Singleton
     *
     * Returns a single instance of the current class.
     */
    public static function singleton()
    {

        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Add Actions
     *
     * Defines all the WordPress actions and filters used by this class.
     */
    protected function _add_actions()
    {
        add_action('cmb2_admin_init', array($this, 'hpwp_homepagebook_metaboxes'));
    }
}

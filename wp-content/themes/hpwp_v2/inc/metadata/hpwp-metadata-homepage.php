<?php
/**
 * Custom Metaboxes | Home Page
 */
class HPWP_Metadata_HomePage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_homepage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'home_page',
			'title'			=> __( 'Home Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/homepage.php', 'page-templates/homepage-v2.php' ) ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		/*$cmb->add_field( array(
			'name'		=> '"What is HPWP?" Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'whatis_txt',
			'default'	=> 'What is HPWP?',
		) );

		$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'whatis_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Pillar {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Pillar', 'cmb2' ),
				'remove_button' => __( 'Remove Pillar', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		// Id's for group's fields only need to be unique for the group. Prefix is not needed.
		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Pillar Title',
			'id'   => 'title',
			'type' => 'text',
			// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Pillar Description',
			'id'   => 'description',
			'type' => 'textarea_small',
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Pillar Image',
			'id'   => 'image',
			'type' => 'file',
		) );*/

		$cmb->add_field( array(
			'name'		=> '"What We Do?" Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'whatwedo_txt',
			'default'	=> 'What We Do?',
		) );

		$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'whatwedo_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Specialty {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Specialty', 'cmb2' ),
				'remove_button' => __( 'Remove Specialty', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name'		=> 'Specialty Description',
			'id'		=> 'description',
			'type'		=> 'wysiwyg',
			'options'	=> array(
				'textarea_rows' => 5,
			)
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Specialty Image',
			'id'   => 'image',
			'type' => 'file',
		) );

		/*$cmb->add_field( array(
			'name' => 'YouTube Video ID',
			'type' => 'text_small',
			'id'   => $prefix . 'youtube_id'
		) );*/

		/*$cmb->add_field( array(
			'name'    => 'HPWP Processes',
			'desc'    => 'use h3 and bullet points',
			'id'      => $prefix . 'processes',
			'type'    => 'wysiwyg',
			'options' => array(),
		) );*/

		$cmb->add_field( array(
			'name'    => 'HPWP "Results"',
			'desc'    => 'use h3 and bullet points',
			'id'      => $prefix . 'results',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5,
			),
		) );

		$cmb->add_field( array(
			'name' => 'HPWP "Results" Image',
			'desc' => 'dimensions: 1920x400',
			'id'   => $prefix . 'results_img',
			'type' => 'file',
		) );

		/*$cmb->add_field( array(
			'name'		=> '"Featured Testimonials" Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'testimonials_title',
			'default'	=> 'Featured Testimonials',
		) );

		$cmb->add_field( array(
			'name'		=> 'Featured Testimonials Text',
			'type'		=> 'wysiwyg',
			'id'		=> $prefix . 'testimonials_txt',
		) );*/

		/*$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'tesimonials_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Testimonial {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Testimonial', 'cmb2' ),
				'remove_button' => __( 'Remove Testimonial', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Testimonial Logo',
			'id'   => 'logo',
			'type' => 'file',
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Testimonial Text',
			'id'   => 'text',
			'type' => 'textarea',
		) );*/

		$cmb->add_field( array(
			'name'		=> '"Team" Title',
			'type'		=> 'text',
			'id'		=> $prefix . 'team_title',
			'default'	=> 'Team',
		) );

		$cmb->add_field( array(
			'name' => 'Bottom Call to Action Background Image',
			'desc' => 'dimensions: 1920x400',
			'id'   => $prefix . 'bottom_bg',
			'type' => 'file',
		) );

		$cmb->add_field( array(
			'name'		=> 'Bottom Call to Action Text',
			'type'		=> 'wysiwyg',
			'desc'		=> 'use h2 tag, use the [button] shortcode to create a cta',
			'id'		=> $prefix . 'bottom_cta',
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_homepage_metaboxes' ) );
	}
}

<?php
/**
 * Custom Metaboxes | Master Class Page
 */
class HPWP_Metadata_MasterClassPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_masterclasspage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'masterclass_page',
			'title'			=> __( 'Master Class Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array(
                'key' => 'page-template',
                'value' => array( 'page-templates/master-class.php')
            ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name' => 'Hero Text - Left Column',
			'id'   => $prefix . 'hero_left_col',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

		$cmb->add_field( array(
			'name' => 'Hero Text - Right Column',
			'id'   => $prefix . 'hero_right_col',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

		$cmb->add_field( array(
			'name'    => 'Left Stat',
			'id'      => $prefix . 'left_stat',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name' => 'Left Stat Content',
			'id'   => $prefix . 'left_stat_content',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

		$cmb->add_field( array(
			'name' => 'Right Stat',
			'id'   => $prefix . 'right_stat',
			'type' => 'text',
        ) );

        $cmb->add_field( array(
			'name' => 'Right Stat Content',
			'id'   => $prefix . 'right_stat_content',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

		$cmb->add_field( array(
			'name' => 'Copy Under Stats',
			'id'   => $prefix . 'under_stats_copy',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
        ) );

		$cmb->add_field( array(
			'name' => 'Quotes',
			'id'   => $prefix . 'quotes',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

		$cmb->add_field( array(
			'name' => 'Workplace of the Future - Content',
			'id'   => $prefix . 'rhetorical_questions',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
		) );

        $cmb->add_field( array(
			'name' => 'Offset Content - Left',
			'id'   => $prefix . 'offset_left_content',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
        ) );

        $cmb->add_field( array(
			'name' => 'Offset Content - Right',
			'id'   => $prefix . 'offset_right_content',
            'type' => 'wysiwyg',
            'options' => array('textarea_rows' => 5)
        ) );

		$cmb->add_field( array(
			'name'		=> 'Agenda PDF',
			'id'		=> $prefix . 'agenda_pdf',
			'type'		=> 'file'
		) );

        $cmb->add_field( array(
			'name'		=> 'YouTube ID',
			'id'		=> $prefix . 'youtube_id',
			'type'		=> 'text'
		) );

		$cmb->add_field( array(
			'name'		=> 'Placeholder Image',
			'id'		=> $prefix . 'placeholder_img',
			'type'		=> 'file'
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_masterclasspage_metaboxes' ) );
	}
}

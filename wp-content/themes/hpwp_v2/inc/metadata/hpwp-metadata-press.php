<?php
/**
 * Custom Metaboxes | Press
 */
class HPWP_Metadata_PressPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_press_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'press_page',
			'title'			=> __( 'Press Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => 'page-templates/press.php' ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'logo_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Logo {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Logo', 'cmb2' ),
				'remove_button' => __( 'Remove Logo', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Logo',
			'desc' => 'Preferred image size: 100x100 pixels.',
			'id'   => 'image',
			'type' => 'file',
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Logo Name',
			'desc' => 'company name',
			'id'   => 'name',
			'type' => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Press Contact Title',
			'default' => 'Press Contact',
			'id'      => $prefix . 'press_contact_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Press Contact Text',
			'id'      => $prefix . 'press_contact_txt',
			'type'    => 'wysiwyg',
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_press_metaboxes' ) );
	}
}

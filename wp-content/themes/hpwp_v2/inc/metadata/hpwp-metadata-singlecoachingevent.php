<?php
/**
 * Custom Metaboxes | Single Coaching Events Page
 */
class HPWP_Metadata_SingleCoachingEvent {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_coachingeventspage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'coaching_events_single',
			'title'			=> __( 'Single Coaching Event Options', 'hpwp' ),
			'object_types'	=> array( 'coaching-event', ), // Post type
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name' => 'Event Date - Start',
			'id'   => $prefix . 'eventdate_start',
			'type' => 'text_date_timestamp',
			'column' => 1,
		) );

		$cmb->add_field( array(
			'name' => 'Event Date - End',
			'id'   => $prefix . 'eventdate_end',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Event Time - Start',
			'id' => $prefix . 'eventtime_start',
			'type' => 'text_time'
		) );

		$cmb->add_field( array(
			'name' => 'Event Time - End',
			'id' => $prefix . 'eventtime_end',
			'type' => 'text_time'
		) );

		$cmb->add_field( array(
			'name' => 'No Cancellation Policy',
			'desc' => 'check ON to disable the cancellation policy',
			'id'   => $prefix . 'no_cancel',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Sold Out',
			'desc' => 'check ON to mark as SOLD OUT',
			'id'   => $prefix . 'sold_out',
			'type' => 'checkbox',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_coachingeventspage_metaboxes' ) );
	}
}

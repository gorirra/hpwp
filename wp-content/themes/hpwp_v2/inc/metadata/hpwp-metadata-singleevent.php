<?php
/**
 * Custom Metaboxes | Single Events Page
 */
class HPWP_Metadata_SingleEvent {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_eventspage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'events_single',
			'title'			=> __( 'Single Event Options', 'hpwp' ),
			'object_types'	=> array( 'event', ), // Post type
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name' => 'Event Date - Start',
			'id'   => $prefix . 'eventdate_start',
			'type' => 'text_date_timestamp',
			'column' => 1,
		) );

		$cmb->add_field( array(
			'name' => 'Event Date - End',
			'id'   => $prefix . 'eventdate_end',
			'type' => 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name' => 'Location (Hotel)',
			'id'   => $prefix . 'event_location',
			'type' => 'text',
			'column' => 2,
		) );

		// $cmb->add_field( array(
		// 	'name' => 'Location (Hotel) URL',
		// 	'id'   => $prefix . 'event_location_url',
		// 	'type' => 'text_url',
		// ) );

		$cmb->add_field( array(
			'name'    => 'Location Image',
			'desc'    => 'Upload an image or enter an URL. Preferred size: 300x300',
			'id'      => $prefix . 'event_img',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name' => 'No Cancellation Policy',
			'desc' => 'check ON to disable the cancellation policy',
			'id'   => $prefix . 'no_cancel',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Don\'t Display on Events Page',
			'desc' => 'check ON to prevent display on events landing page',
			'id'   => $prefix . 'events_no_show',
			'type' => 'checkbox',
		) );

		$cmb->add_field( array(
			'name' => 'Sold Out',
			'desc' => 'check ON to mark as SOLD OUT',
			'id'   => $prefix . 'sold_out',
			'type' => 'checkbox',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_eventspage_metaboxes' ) );
	}
}

<?php
/**
 * Custom Metaboxes | Press
 */
class HPWP_Metadata_SinglePress {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_singlepress_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'           => 'press_cpt',
			'title'        => __( 'Press Options', 'hpwp' ),
			'object_types' => array( 'press', ), // Post type
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );

		// $cmb->add_field( array(
		// 	'name'    => 'Is Featured?',
		// 	'desc'    => 'check ON if press piece is featured',
		// 	'id'      => $prefix . 'press_featured',
		// 	'type'    => 'checkbox',
		// ) );

		$cmb->add_field( array(
			'name'    => 'Stock Image',
			'desc'    => 'Upload an image or enter an URL. Minimum: 400x200 px',
			'id'      => $prefix . 'press_stock',
			'type'    => 'file',
		) );

		// $cmb->add_field( array(
		// 	'name'    => 'Logo',
		// 	'desc'    => 'Upload an image or enter an URL. Minimum: 100x50 px',
		// 	'id'      => $prefix . 'press_logo',
		// 	'type'    => 'file',
		// ) );

		$cmb->add_field( array(
			'name'    => 'Press Name',
			'desc'    => 'Name of company featuring HPWP',
			'id'      => $prefix . 'press_name',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Press Date', 'cmb2' ),
			'id'	=> $prefix . 'press_date',
			'type'	=> 'text_date_timestamp',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'URL', 'cmb2' ),
			'id'	=> $prefix . 'press_url',
			'type'	=> 'text_url',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_singlepress_metaboxes' ) );
	}
}

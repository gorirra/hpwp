<?php
/**
 * Custom Metaboxes | Solutions
 */
class HPWP_Metadata_Solutions {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_solutions_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'           => 'solutions_page',
			'title'        => __( 'Solutions Page Options', 'hpwp' ),
			'object_types' => array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/solutions.php', 'page-templates/solutions-v2.php' ) ),
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );

		$cmb->add_field( array(
			'name'    => 'Solution 1 - Title',
			'id'      => $prefix . 'solution1_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Solution 1 - Left Content',
			'id'      => $prefix . 'solution1_left',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Solution 1 - Right Content',
			'id'      => $prefix . 'solution1_right',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'CTA - Left',
			'desc'    => 'use [button] shortcode to create orange cta',
			'id'      => $prefix . 'cta_left',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'CTA - Right',
			'desc'    => 'use [button] shortcode to create orange cta',
			'id'      => $prefix . 'cta_right',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		// $cmb->add_field( array(
		// 	'name'    => 'Mid-page Image',
		// 	'desc'    => 'Upload an image or enter an URL.',
		// 	'id'      => $prefix . 'midpage_img',
		// 	'type'    => 'file',
		// ) );

		// $cmb->add_field( array(
		// 	'name'    => 'Testimonial',
		// 	'id'      => $prefix . 'testimonial',
		// 	'type'    => 'wysiwyg',
		// 	'options' => array(
		// 		'textarea_rows' => 7
		// 	),
		// ) );

		// $cmb->add_field( array(
		// 	'name'		=> '"Featured Testimonials" Title',
		// 	'type'		=> 'text',
		// 	'id'		=> $prefix . 'testimonials_title',
		// 	'default'	=> 'Featured Testimonials',
		// ) );

		// $cmb->add_field( array(
		// 	'name'		=> 'Featured Testimonials Text',
		// 	'type'		=> 'wysiwyg',
		// 	'id'		=> $prefix . 'testimonials_txt',
		// ) );

		$cmb->add_field( array(
			'name'    => 'Solution 2 - Title',
			'id'      => $prefix . 'solution2_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Solution 2 - Left Content',
			'id'      => $prefix . 'solution2_left',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Solution 2 - Right Content',
			'id'      => $prefix . 'solution2_right',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Contact Form - Title',
			'id'      => $prefix . 'contact_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Form Shortcode',
			'id'      => $prefix . 'book_form',
			'type'    => 'textarea_code',
		) );

		/*$group_field_id = $cmb->add_field( array(
			'id'          => $prefix . 'client_group',
			'type'        => 'group',
			// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Client {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Client', 'cmb2' ),
				'remove_button' => __( 'Remove Client', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'name' => 'Client Logo',
			'id'   => 'image',
			'desc' => 'preferred width: 100px',
			'type' => 'file',
		) );*/
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_solutions_metaboxes' ) );
	}
}

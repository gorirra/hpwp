<?php
/**
 * Custom Metaboxes | Speaker Registration Page
 */
class HPWP_Metadata_SpeakerRegistrationPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_speakerregistrationpage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * body
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'speaker_registration_page',
			'title'			=> __( 'Speaker Registration Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/speaker-registration.php') ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name'    => 'Registration Form Shortcode',
			'id'      => $prefix . 'reg_form_shortcode',
			'type'    => 'text',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_speakerregistrationpage_metaboxes' ) );
	}
}

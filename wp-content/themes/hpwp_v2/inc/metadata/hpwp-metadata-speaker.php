<?php
/**
 * Custom Metaboxes | Book Page
 */
class HPWP_Metadata_SpeakerPage {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_speakerpage_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * body
		 */
		$cmb = new_cmb2_box( array(
			'id'			=> 'speaker_page',
			'title'			=> __( 'Speaker Page Options', 'hpwp' ),
			'object_types'	=> array( 'page', ), // Post type
			'show_on'		=> array( 'key' => 'page-template', 'value' => array( 'page-templates/speaker.php') ),
			'context'		=> 'advanced',
			'priority'		=> 'high',
			'show_names'	=> true, // Show field names on the left
			'closed'		=> false,
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Banner Text', 'cmb2' ),
			'id'	=> $prefix . 'banner_txt',
			'type'	=> 'textarea',
		) );
		$cmb->add_field( array(
			'name'	=> __( 'Banner URL', 'cmb2' ),
			'id'	=> $prefix . 'banner_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'    => 'Book Cover',
			'desc'    => 'tilted book cover',
			'id'      => $prefix . 'book_tilt',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'    => 'Speaker Section - Title',
			'id'      => $prefix . 'speaking_section_title',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Speaker Section - Intro',
			'id'      => $prefix . 'speaking_section_intro',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 5
			),
		) );

		for($i = 1; $i < 3; $i++) {
			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Name',
				'id'      => $prefix . 'speaker'. $i .'_name',
				'type'    => 'text',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Designations',
				'id'      => $prefix . 'speaker'. $i .'_designations',
				'type'    => 'text',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Image',
				'desc'    => 'Upload an image or enter an URL.',
				'id'      => $prefix . 'speaker'. $i .'_img',
				'type'    => 'file',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Content',
				'id'      => $prefix . 'speaker'. $i .'_content',
				'type'    => 'wysiwyg',
				'options' => array(
					'textarea_rows' => 7
				),
			) );

			$cmb->add_field( array(
				'name' => 'Speaker '. $i .' - Email',
				'id'   => $prefix . 'speaker'. $i .'_email',
				'type' => 'text_email',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Twitter',
				'id'      => $prefix . 'speaker'. $i .'_twitter',
				'type'    => 'text_url',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Linkedin',
				'id'      => $prefix . 'speaker'. $i .'_linkedin',
				'type'    => 'text_url',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Facebook',
				'id'      => $prefix . 'speaker'. $i .'_facebook',
				'type'    => 'text_url',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Vimeo ID',
				'id'      => $prefix . 'speaker'. $i .'_vimeo',
				'type'    => 'text',
			) );

			$cmb->add_field( array(
				'name'    => 'Speaker '. $i .' - Youtube ID',
				'id'      => $prefix . 'speaker'. $i .'_youtube',
				'type'    => 'text',
			) );

		}

        $cmb->add_field( array(
			'name'    => 'Speaking Events - Title',
			'id'      => $prefix . 'speaking_events_title',
			'type'    => 'text',
		) );

		$events = $cmb->add_field( array(
			'id'          => $prefix . 'events',
			'type'        => 'group',
			'options'     => array(
				'group_title'   => __( 'Event {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Event', 'cmb2' ),
				'remove_button' => __( 'Remove Event', 'cmb2' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

			$cmb->add_group_field( $events, array(
				'name' => 'Date Start',
				'id'   => 'date_start',
				'type' => 'text_date_timestamp',
			) );

			$cmb->add_group_field( $events, array(
				'name' => 'Date End',
				'id'   => 'date_end',
				'type' => 'text_date_timestamp',
            ) );

            $cmb->add_group_field( $events, array(
				'name' => 'Location',
				'id'   => 'location',
				'type' => 'text',
			) );

			$cmb->add_group_field( $events, array(
				'name' => 'Location URL - optional',
				'id'   => 'location_url',
				'type' => 'text_url',
			) );

		$cmb->add_field( array(
			'name'    => 'Book Us - Intro 1',
			'id'      => $prefix . 'book_us_content',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Book Us - Intro 2',
			'id'      => $prefix . 'book_us_content2',
			'type'    => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 7
			),
		) );

		$cmb->add_field( array(
			'name'    => 'Book Us - CTA Text',
			'id'      => $prefix . 'book_us_txt',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => 'Book Us - CTA URL',
			'id'      => $prefix . 'book_us_url',
			'type'    => 'text_url',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_speakerpage_metaboxes' ) );
	}
}

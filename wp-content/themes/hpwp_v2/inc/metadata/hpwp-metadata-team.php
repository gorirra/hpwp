<?php
/**
 * Custom Metaboxes | Team
 */
class HPWP_Metadata_Team {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_team_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'           => 'team_cpt',
			'title'        => __( 'Team Member Options', 'hpwp' ),
			'object_types' => array( 'team', ), // Post type
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Title', 'cmb2' ),
			'id'	=> $prefix . 'title',
			'type'	=> 'text',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Google+ URL', 'cmb2' ),
			'id'	=> $prefix . 'googleplus_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Facebook URL', 'cmb2' ),
			'id'	=> $prefix . 'facebook_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Twitter URL', 'cmb2' ),
			'id'	=> $prefix . 'twitter_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Instagram URL', 'cmb2' ),
			'id'	=> $prefix . 'instagram_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'LinkedIn URL', 'cmb2' ),
			'id'	=> $prefix . 'linkedin_url',
			'type'	=> 'text_url',
		) );

		$cmb->add_field( array(
			'name'		=> 'Order',
			'id'		=> $prefix . 'order',
			'type'		=> 'text',
			'default'	=> 0,
			'column'	=> array(
				'position'	=> 2,
				'name'		=> 'Order',
			),
			'desc'		=> 'Use multiples of 5.',
		) );

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_team_metaboxes' ) );
	}
}

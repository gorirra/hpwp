<?php
/**
 * Custom Metaboxes | Testimonials
 */
class HPWP_Metadata_Testimonials {

	static $instance = false;

	public function __construct() {

		$this->_add_actions();

	}


	public function hpwp_testimonials_metaboxes() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = '_hpwp_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
			'id'           => 'testimonials_cpt',
			'title'        => __( 'Testimonial Options', 'hpwp' ),
			'object_types' => array( 'testimonials', ), // Post type
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
			'closed'       => false,
		) );

		$cmb->add_field( array(
			'name'    => 'Testimonial Type',
			'id'      => $prefix . 'testi_type',
			'type'    => 'radio_inline',
			'options' => array(
				'home'			=> __( 'Home Page', 'cmb2' ),
				'leadership'	=> __( 'Leadership Workshop', 'cmb2' ),
			),
			'column' => true
		) );

		$cmb->add_field( array(
			'name'    => 'Logo',
			'desc'    => 'Upload an image or enter an URL.',
			'id'      => $prefix . 'testi_logo',
			'type'    => 'file',
		) );

		$cmb->add_field( array(
			'name'	=> __( 'Testimonial', 'cmb2' ),
			'id'	=> $prefix . 'testimonial',
			'type'	=> 'wysiwyg',
		) );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this class.
	 */
	protected function _add_actions() {
		add_action( 'cmb2_admin_init', array( $this, 'hpwp_testimonials_metaboxes' ) );
	}
}

<?php
/**
 * Custom Post Types | Events
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class HPWP_CPT_Events {

	static $instance	= false;

	public function __construct() {

		$this->_register_cpt_events();

	}

	/**
	 * Register Meta Boxes
	 *
	 * Defines all the meta boxes with CMB2 used by this custom post type.
	 *
	 */
	protected function _register_cpt_events() {

		$labels = array(
			'name'					=> __( 'Events', 'hpwp_v2' ),
			'singular_name'			=> __( 'Event', 'hpwp_v2' ),
			'add_new'				=> __( 'Add New Event', 'hpwp_v2' ),
			'add_new_item'			=> __( 'Add Event', 'hpwp_v2' ),
			'edit_item'				=> __( 'Edit Event', 'hpwp_v2' ),
			'new_item'				=> __( 'New Event', 'hpwp_v2' ),
			'view_item'				=> __( 'View Event', 'hpwp_v2' ),
			'search_items'			=> __( 'Search Events', 'hpwp_v2' ),
			'not_found'				=> __( 'No Events Found', 'hpwp_v2' ),
			'not_found_in_trash'	=> __( 'No Events Found in the Trash', 'hpwp_v2' ),
			'parent_item_colon'		=> __( 'Parent Event:', 'hpwp_v2' ),
			'menu_name'				=> __( 'Events', 'hpwp_v2' ),
		);

		$args = array(
			'labels'				=> $labels,
			'supports'				=> array( 'title', 'editor' ),
			'public'				=> true,
			// 'show_ui'				=> true,
			// 'show_in_menu'			=> true,
			'menu_icon'				=> 'dashicons-tickets-alt',
			'publicly_queryable'	=> true,
			// 'exclude_from_search'	=> true,
			'menu_position'			=> 25,
			'has_archive'			=> true,
			// 'show_in_nav_menus'		=> true,
			'query_var'				=> true,
			// 'taxonomies'			=> array( 'category' ),
			'can_export'			=> true,
			'rewrite'				=> true,
			'capability_type'		=> 'post',
		);

		register_post_type( 'event', $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


}

<?php
/**
 * Custom Post Types | Team
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class HPWP_CPT_Team {

	static $instance	= false;

	public function __construct() {

		$this->_register_cpt_team();

		if( isset( $_GET ) && !empty( $_GET['post_type'] ) ) {
			if( $_GET['post_type'] == 'team' ) {
				$this->default_orderby_cpt_team();
			}
		}

	}

	/**
	 * Register Meta Boxes
	 *
	 * Defines all the meta boxes with CMB2 used by this custom post type.
	 *
	 */
	protected function _register_cpt_team() {

		$labels = array(
			'name'					=> __( 'Team Members', 'hpwp_v2' ),
			'singular_name'			=> __( 'Team Member', 'hpwp_v2' ),
			'add_new'				=> __( 'Add New Team Member', 'hpwp_v2' ),
			'add_new_item'			=> __( 'Add Team Member', 'hpwp_v2' ),
			'edit_item'				=> __( 'Edit Team Member', 'hpwp_v2' ),
			'new_item'				=> __( 'New Team Member', 'hpwp_v2' ),
			'view_item'				=> __( 'View Team Member', 'hpwp_v2' ),
			'search_items'			=> __( 'Search Team Members', 'hpwp_v2' ),
			'not_found'				=> __( 'No Team Members Found', 'hpwp_v2' ),
			'not_found_in_trash'	=> __( 'No Team Members Found in the Trash', 'hpwp_v2' ),
			'parent_item_colon'		=> __( 'Parent Team Member:', 'hpwp_v2' ),
			'menu_name'				=> __( 'Team Members', 'hpwp_v2' ),
		);

		$args = array(
			'labels'				=> $labels,
			'supports'				=> array( 'thumbnail', 'title', 'editor' ),
			'public'				=> true,
			// 'show_ui'				=> true,
			// 'show_in_menu'			=> true,
			'menu_icon'				=> 'dashicons-groups',
			'publicly_queryable'	=> true,
			// 'exclude_from_search'	=> true,
			'menu_position'			=> 25,
			'has_archive'			=> true,
			// 'show_in_nav_menus'		=> true,
			'query_var'				=> true,
			// 'taxonomies'			=> array( 'category' ),
			'can_export'			=> true,
			'rewrite'				=> false,
			'capability_type'		=> 'post',
		);

		register_post_type( 'team', $args );
	}


	/**
	 * https://code.tutsplus.com/articles/quick-tip-make-your-custom-column-sortable--wp-25095
	 */
	protected function default_orderby_cpt_team() {

		// add content
		add_action( 'manage_team_posts_custom_column', 'hpwp_team_column_content', 10, 2 );
		function hpwp_team_column_content( $column_name, $post_id ) {
			if( 'pos_order' != $column_name )
				return;
			// Get number of order from post meta
			$order = get_post_meta( $post_id, '_hpwp_order', 1 );
			echo intval( $order );
		}


		// make it sortable
		add_filter( 'manage_edit-team_sortable_columns', 'hpwp_sortable_team_column' );
		function hpwp_sortable_team_column( $columns ) {
			$columns['_hpwp_order'] = '_hpwp_order';

			return $columns;
		}


		// default sort
		add_action( 'pre_get_posts', 'hpwp_order_orderby' );
		function hpwp_order_orderby( $query ) {
			if( !is_admin() )
				return;

			$orderby	= $query->get( 'orderby' );
			$order		= $query->get( 'order' );

			if( $orderby == '' || 'pos_order' == $orderby ) {
				$query->set( 'meta_key', '_hpwp_order' );
				$query->set( 'orderby', 'meta_value_num' );

				if( $order == '' ) {

					$query->set( 'order', 'ASC' );

				}
			}
		}

	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


}

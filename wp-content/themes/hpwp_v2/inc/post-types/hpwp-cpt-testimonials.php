<?php
/**
 * Custom Post Types | Testimonials
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://themergency.com/generators/wordpress-custom-post-types/
 */

class HPWP_CPT_Testimonials {

	static $instance	= false;

	public function __construct() {

		$this->_register_cpt_testimonials();

	}

	/**
	 * Register Meta Boxes
	 *
	 * Defines all the meta boxes with CMB2 used by this custom post type.
	 *
	 */
	protected function _register_cpt_testimonials() {

		$labels = array(
			'name'					=> __( 'Testimonials', 'hpwp_v2' ),
			'singular_name'			=> __( 'Testimonial', 'hpwp_v2' ),
			'add_new'				=> __( 'Add New Testimonial', 'hpwp_v2' ),
			'add_new_item'			=> __( 'Add Testimonial', 'hpwp_v2' ),
			'edit_item'				=> __( 'Edit Testimonial', 'hpwp_v2' ),
			'new_item'				=> __( 'New Testimonial', 'hpwp_v2' ),
			'view_item'				=> __( 'View Testimonial', 'hpwp_v2' ),
			'search_items'			=> __( 'Search Testimonials', 'hpwp_v2' ),
			'not_found'				=> __( 'No Testimonials Found', 'hpwp_v2' ),
			'not_found_in_trash'	=> __( 'No Testimonials Found in the Trash', 'hpwp_v2' ),
			'parent_item_colon'		=> __( 'Parent Testimonial:', 'hpwp_v2' ),
			'menu_name'				=> __( 'Testimonials', 'hpwp_v2' ),
		);

		$args = array(
			'labels'				=> $labels,
			'supports'				=> array( 'title' ),
			'public'				=> true,
			// 'show_ui'				=> true,
			// 'show_in_menu'			=> true,
			'menu_icon'				=> 'dashicons-editor-quote',
			'publicly_queryable'	=> true,
			// 'exclude_from_search'	=> true,
			'menu_position'			=> 25,
			'has_archive'			=> true,
			// 'show_in_nav_menus'		=> true,
			'query_var'				=> true,
			// 'taxonomies'			=> array( 'testimonial-cat' ),
			'can_export'			=> true,
			'rewrite'				=> false,
			'capability_type'		=> 'post',
		);

		register_post_type( 'testimonials', $args );
	}


	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( ! self::$instance )
			self::$instance	= new self;

		return self::$instance;
	}


}

<?php
	/**
	 * The main template file
	 *
	 * This is the most generic template file in a WordPress theme
	 * and one of the two required files for a theme (the other being style.css).
	 * It is used to display a page when nothing more specific matches a query.
	 * E.g., it puts together the home page when no home.php file exists.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$pageid	= get_the_ID();

	get_header();

	get_template_part( 'template-parts/content', 'blog' );

	get_footer();

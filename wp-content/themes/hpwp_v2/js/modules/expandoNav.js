$(document).ready(function(){
  var scrollTop = 0;

  $(window).scroll(function(){
    scrollTop = $(window).scrollTop();
    $('.counter').html(scrollTop);

    if (scrollTop >= 150) {
      $('.navbar').addClass('nav-short');
    } else if (scrollTop < 150) {
      $('.navbar').removeClass('nav-short');
    }
  });
});

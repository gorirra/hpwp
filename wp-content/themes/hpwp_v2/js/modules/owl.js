$.noConflict(); 
//Greg
$(document).ready(function() {
  var $carousel_five = $('.owl-carousel__five'),
      $carousel_three = $('.owl-carousel__three-padding'),
      $carousel_testimonials = $('.module-testimonials .owl-carousel');

  if($carousel_five.length) {
    // client list slider
    $('.owl-carousel__five').owlCarousel({
      navContainer: '.owl-carousel__customnav',
      nav: true,
      navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
      dots: false,
      loop: true,
      autoplay: true,
      responsive: {
        0: {
          items: 1,
        },
        480: {
          items: 2,
        },
        768: {
          items: 3,
        },
        992: {
          items: 4,
        },
        1200: {
          items: 5,
        },
      },
    });
  }


  // team list slider
  if($carousel_three.length) {
    $('.owl-carousel__three-padding').owlCarousel({
      items: 4,
      nav: false,
      dots: false,
      loop: true,
      stagePadding: 50,
      margin: 30,
      autoplay: true,
      responsive: {
        0: {
          items: 1,
        },
        480: {
          items: 2,
        },
        768: {
          items: 4,
          stagePadding: 100,
        },
        1200: {
          stagePadding: 200,
        },
      },
    });
  }


  // testimonials slider
  if($carousel_testimonials.length) {
    $('.module-testimonials .owl-carousel').owlCarousel({
      navContainer: '.owl-carousel__customnav',
      nav: true,
      navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
      loop: true,
      responsive: {
        0: {
          items: 1,
        },
        640: {
          items: 2,
        },
        1200: {
          items: 3,
        },
      },
    });
  }
});

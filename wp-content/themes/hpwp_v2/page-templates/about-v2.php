<?php
	/**
	 * Template Name: About Page - v2
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'about-v2' );

	get_footer();
<?php
	/**
	 * Template Name: Alumni Landing Page
	 *
	 * @package hpwp_v2
	 */

	get_header('variant');

	get_template_part('template-parts/content', 'alumni-landing');

	get_footer('variant');
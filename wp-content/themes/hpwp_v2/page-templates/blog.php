<?php
	/**
	 * Template Name: Blog
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'blog' );
 
	get_footer();
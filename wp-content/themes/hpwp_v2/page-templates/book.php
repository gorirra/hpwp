<?php
	/**
	 * Template Name: Book Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'book' );

	get_footer();
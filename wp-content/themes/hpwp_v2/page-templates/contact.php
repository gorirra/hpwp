<?php
	/**
	 * Template Name: Contact Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'contact' );

	get_footer();
<?php
	/**
	 * Template Name: Events Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'events' );

	get_footer();
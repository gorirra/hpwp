<?php
	/**
	 * Template Name: Hiring Team Page - v2
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'hiringteam-v2' );

	get_footer();
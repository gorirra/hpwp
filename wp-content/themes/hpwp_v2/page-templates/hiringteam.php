<?php
	/**
	 * Template Name: Hiring Team Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'hiringteam' );

	get_footer();
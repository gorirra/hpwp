<?php
	/**
	 * Template Name: Home Page - Book
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'home-book' );

	get_footer();
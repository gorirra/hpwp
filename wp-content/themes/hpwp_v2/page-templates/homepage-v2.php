<?php
	/**
	 * Template Name: Home Page - v2
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'home-v2' );

	get_footer();
<?php
	/**
	 * Template Name: High Performance Coaching
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'hp-coaching' );

	get_footer();
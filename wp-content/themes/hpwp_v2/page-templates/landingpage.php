<?php
	/**
	 * Template Name: Landing Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'carterville' );

	get_footer();
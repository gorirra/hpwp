<?php
	/**
	 * Template Name: Master Class Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'master-class' );

	get_footer();
<?php
	/**
	 * Template Name: Press Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'press' );

	get_footer();
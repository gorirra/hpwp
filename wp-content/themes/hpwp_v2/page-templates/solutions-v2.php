<?php
	/**
	 * Template Name: Solutions Page - v2
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'solutions-v2' );

	get_footer();
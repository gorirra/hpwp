<?php
	/**
	 * Template Name: Speaker Registration
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'speaker-registration' );

	get_footer();
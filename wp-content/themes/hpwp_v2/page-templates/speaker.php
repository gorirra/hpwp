<?php
	/**
	 * Template Name: Speaker Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'speaker' );

	get_footer();
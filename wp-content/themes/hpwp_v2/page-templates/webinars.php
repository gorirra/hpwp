<?php
	/**
	 * Template Name: Webinars Page
	 *
	 * @package hpwp_v2
	 */

	get_header();

	get_template_part( 'template-parts/content', 'webinars' );

	get_footer();
<?php
	/**
	 * The template for displaying all single posts
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
	 *
	 * @package hpwp_v2
	 */
 
	get_header(); ?>

<style>
	@media only screen and (max-width: 768px) {
		.single .col-sm-8 {
			margin-bottom: 40px;
		}
	}
</style>
	<?php

	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', get_post_type() );


		// the_post_navigation();

	endwhile; // End of the loop.

	get_footer();
<?php
	/**
	 * Template part for displaying page content in page-templates/about.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'aboutpage' ); ?>>
	<div class="container">
		<div class="module module-underjumbotron">

			<?php
				while( have_posts() ) {
					the_post();

					the_content();

				}

				$i			= 1;
				$objectives	= array_key_exists( $prefix . 'teamobjectives_group', $all_meta ) ? unserialize( $all_meta[$prefix . 'teamobjectives_group'] ) : '';

				if( $objectives != '' ) {

					echo '<div class="row">';

						foreach( $objectives as $key => $entry ) {

							echo '<div class="col-md-4">
								<div class="circled_numbers">'. $i .'</div>'.
								apply_filters( 'the_content', $entry['description'] ) .'
							</div>';

							$i++;

						}

					echo '</div>';

				}
			?>

		</div>

		<?php
			$team_query = hpwp_get_team();

			if( $team_query->have_posts() ) {

				echo '<div class="module-evenrow">
					<div class="module-aboutus row">';

						while( $team_query->have_posts() ) {
							$team_query->the_post();

							$personid	= get_the_ID();
							$title		= get_post_meta( $personid, $prefix . 'title', 1 ) != '' ? get_post_meta( $personid, $prefix . 'title', 1 ) : '';

							echo '<div class="col-sm-6 col-md-4">

								<p><a data-src="#about-'. $personid .'" href="javascript:;" data-fancybox>'. get_the_post_thumbnail( $personid, 'medium', array( 'class' => 'aligncenter img-responsive' ) ) .'</a></p>

								<h3>'. get_the_title() .'</h3>
								<p>'.
									( $title != '' ? $title . '<br>' : '' ) .'
									<small><a data-src="#about-'. $personid .'" href="javascript:;" data-fancybox class="orangetext">READ BIO</a></small>
								</p>

								<div id="about-'. $personid .'" class="media module-aboutus__modal" style="display: none;">
									<div class="media-left">'.
										get_the_post_thumbnail( $personid, 'medium', array( 'class' => 'media-object img-responsive' ) ) .'
									</div>
									<div class="media-body">
										<h3 class="media-heading">'. get_the_title() .'</h3>

										<p>'. $title .'</p>'.
										apply_filters( 'the_content', get_the_content() ).'

										<p class="module-aboutus__modal-social">'.

											( get_post_meta( $personid, $prefix . 'linkedin_url', 1 ) ? '<a href="'. get_post_meta( $personid, $prefix . 'linkedin_url', 1 ) .'"><i class="icon-linkedin-squared"></i></a>' : '' ) .
											( get_post_meta( $personid, $prefix . 'facebook_url', 1 ) ? '<a href="'. get_post_meta( $personid, $prefix . 'facebook_url', 1 ) .'"><i class="icon-facebook-official"></i></a>' : '' ) .
											( get_post_meta( $personid, $prefix . 'twitter_url', 1 ) ? '<a href="'. get_post_meta( $personid, $prefix . 'twitter_url', 1 ) .'"><i class="icon-twitter"></i></a>' : '' ) .
											( get_post_meta( $personid, $prefix . 'instagram_url', 1 ) ? '<a href="'. get_post_meta( $personid, $prefix . 'instagram_url', 1 ) .'"><i class="icon-instagram"></i></a>' : '' ) .'

										</p>
									</div>
								</div>
							</div>';

						}

					echo '</div>
				</div>';

				wp_reset_postdata();
			}
		?>

	</div>


	<div class="module module-white">
		<div class="container">
			<?php echo apply_filters( 'the_content', $all_meta[$prefix . 'clients_list_txt'] ); ?>
		</div>
	</div>


	<?php
		/**
		 * logo carousel
		 */

		$client_page_id = hpwp_get_option( 'hpwp_client_page_id' );
		$client_group = get_post_meta( $client_page_id, $prefix . 'client_group', 1 ) != '' ? get_post_meta( $client_page_id, $prefix . 'client_group', 1 ) : '';

		if( $client_group != '' ) {

			echo '<div class="module module-light">
				<div class="container">
					<div class="owl-carousel owl-carousel__five">';

						foreach( $client_group as $key => $entry ) {

							if( isset( $entry['image_id'] ) ) {
								$img = wp_get_attachment_image( $entry['image_id'], 'full', null, array( 'class' => 'img-responsive center-block' ) );
							}

							echo '<div class="tile-white">'.
								$img .'
							</div>';

						}

					echo '</div>
					<div class="owl-carousel__customnav"></div>
				</div>
			</div>';

		}
	?>

</div>

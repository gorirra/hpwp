<?php
	/**
	 * Template part for displaying page content in page-templates/alumni-landing.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>
<?php if($all_meta[$prefix . 'bg']): ?>
	<style>
		.page-template-alumni-landing {
			background-image: url(<?php echo $all_meta[$prefix . 'bg']; ?>);
		}
	</style>
<?php endif; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'alumni-landing' ); ?>>
	<div class="container">
		<div class="module">
			<div class="logo">
				<?php print display_svg(get_bloginfo( 'stylesheet_directory') . '/images/hpwp-logo-white.svg'); ?>
			</div>

			<p class="h1 top"><?php print $all_meta[$prefix . 'first']; ?></p>

			<?php if($all_meta[$prefix .'content']): ?>
				<div class="secondary">
					<?php print apply_filters('the_content', $all_meta[$prefix .'content']); ?>
				</div>
			<?php endif; ?>

			<div class="button-group">
				<?php if($all_meta[$prefix .'link1_url']): ?>
					<a class="btn btn-lg btn-alumni" href="<?php print $all_meta[$prefix .'link1_url']; ?>"><?php print $all_meta[$prefix .'link1_title']; ?></a>
				<?php endif; ?>

				<?php if($all_meta[$prefix .'link2_url']): ?>
					<a class="btn btn-lg btn-alumni-lt" href="<?php print $all_meta[$prefix .'link2_url']; ?>"><?php print $all_meta[$prefix .'link2_title']; ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

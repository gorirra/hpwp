<?php
	/**
	 * Template part for displaying page content in page-templates/about.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$paged	= get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$big	= 999999999; // need an unlikely integer for pagination

$category = get_queried_object();
echo $category->term_id;

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'blogpage' ); ?>>

	<?php

			$args = array(
				'post_type'					=> 'post',
				'cat' 						=> $category->term_id,
				'posts_per_page'			=> 9, 
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
				'orderby'					=> 'date',
				'no_found_rows'				=> false,
				'update_post_meta_cache'	=> false,
				'paged'						=> $paged,
			);

			$blog_query = new WP_Query( $args );
			

		

		if( $blog_query->have_posts() ) {

			echo '<section class="module module-underjumbotron module-press module-evenrow">
				<div class="container">
					<div class="row">';

						while( $blog_query->have_posts() ) {
							$blog_query->the_post();

							$blogid	    = get_the_ID();
							$date		= get_the_date( 'F j, Y' );

							echo '<div class="col-sm-6 col-md-4">

								<div class="tile-white">';

									$stock_img_id = get_the_post_thumbnail_url($blogid, 'tile-img');

									if( $stock_img_id != '' ) {

										echo '<div class="tile-img" style="background-image: url( '.  $stock_img_id .' );"></div>';

									}

									echo '<p><small>'. $date .'</small></p>
									<h4><a href="'. get_permalink( $blogid ) .'">'. get_the_title() .'</a></h4>';
									echo '<p>'. apply_filters( 'the_content', get_the_content() ) .'</p>
									<p><small><a class="orangetext" href="'. get_permalink( $blogid ) .'">Read More &raquo;</a></small></p>
								</div>

							</div>';

						}

					echo '</div>

					<nav class="alignright" aria-label="Page navigation">
						<ul class="pagination">';

							$pagination_array = paginate_links( array(
								'current'	=> max( 1, $paged ),
								'total'		=> $blog_query->max_num_pages,
								'type'		=> 'array',
							) );

							if( $pagination_array != '' ) {

								foreach( $pagination_array as $pagenumber ) {

									$current = strpos( $pagenumber, 'current' );

									if( $current === false ) {
										$active_class = '';
									} else {
										$active_class = ' class="active"';
									}

									echo '<li'. $active_class .'>'.
										$pagenumber .'
									</li>';

								}

							}

						echo '</ul>
					</nav>

				</div>
			</section>';

			wp_reset_postdata();
		}


	?>

</div>

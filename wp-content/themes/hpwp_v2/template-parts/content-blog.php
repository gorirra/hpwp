<?php
	/**
	 * Template part for displaying page content in page-templates/about.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$paged	= get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$big	= 999999999; // need an unlikely integer for pagination
// $content = apply_filters('the_content', get_the_content());
// $character_limit = 200;
// $trimmed_content = mb_substr($content, 0, $character_limit);
// // Append "Read More" link if the trimmed content is shorter than the original content
// if (mb_strlen($content) > $character_limit) {
//     $trimmed_content .= '... <a class="orangetext" href="' . get_permalink($blogid) . '">Read More &raquo;</a>';
// }
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'blogpage' ); ?>>

	<?php
		if( false === ( $blog_query = get_transient( 'blog_query_results_' . $paged ) ) ) {

			$args = array(
				'post_type'					=> 'post',
				'posts_per_page'			=> 9, 
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
				'orderby'					=> 'date',
				'no_found_rows'				=> false,
				'update_post_meta_cache'	=> false,
				'paged'						=> $paged,
			);

			$blog_query = new WP_Query( $args );
			set_transient( 'blog_query_results_' . $paged, $blog_query, 1 * MINUTE_IN_SECONDS );

		}

		if( $blog_query->have_posts() ) {

			echo '<section class="module module-underjumbotron module-press module-evenrow">
				<div class="container">
					<div class="row">';

						while( $blog_query->have_posts() ) {
							$blog_query->the_post();

							$blogid	    = get_the_ID();
							$date		= get_the_date( 'F j, Y' );

							echo '<div class="col-sm-6 col-md-4">

								<div class="tile-white">';

									$stock_img_id = get_the_post_thumbnail_url($blogid, 'tile-img');

									if( $stock_img_id != '' ) {

										echo '<div class="tile-img" style="background-image: url( '.  $stock_img_id .' );"></div>';

									}

									// <div class="row">
									// 	<div class="col-xs-6 col-md-6">
									// 		<p><small>'. $date .'</small></p>
									// 	</div>
									// 	<div class="col-xs-6 col-md-6">
									// 		<p class="alignright">'. wp_get_attachment_image( get_post_meta( $blogid, $prefix . 'press_logo_id', 1 ), 'thumbnail' ) .'</p>
									// 	</div>
									// </div>

									echo '<p><small>'. $date .'</small></p>
									<h4><a href="'. get_permalink( $blogid ) .'">'. get_the_title() .'</a></h4>';
									// Output the content
echo '<p>' . the_excerpt(). '</p>';
						

echo '<a class="orangetext" href="' . get_permalink($blogid) . '">Read More &raquo;</a>';
							echo '
								</div>

							</div>';

						}

					echo '</div>

					<nav class="alignright" aria-label="Page navigation">
						<ul class="pagination">';

							$pagination_array = paginate_links( array(
								'current'	=> max( 1, $paged ),
								'total'		=> $blog_query->max_num_pages,
								'type'		=> 'array',
							) );

							if( $pagination_array != '' ) {

								foreach( $pagination_array as $pagenumber ) {

									$current = strpos( $pagenumber, 'current' );

									if( $current === false ) {
										$active_class = '';
									} else {
										$active_class = ' class="active"';
									}

									echo '<li'. $active_class .'>'.
										$pagenumber .'
									</li>';

								}

							}

						echo '</ul>
					</nav>

				</div>
			</section>';

			wp_reset_postdata();
		}

		$press_content = get_post_meta( get_the_ID(), $prefix . 'press_contact_txt', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'press_contact_txt', 1 ) ) : '';

		if( $press_content != '' ) {

			echo '<section class="module module-light">
				<div class="container">
					<div class="narrow text-center">
						<h2>'. get_post_meta( get_the_ID(), $prefix . 'press_contact_title', 1 ) .'</h2>'.

						$press_content .'

					</div>
				</div>
			</section>';

		}
	?>

</div>

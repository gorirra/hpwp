<?php
	/**
	 * Template part for displaying page content in page-templates/book.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'bookpage' ); ?>>

	<span id="hero" class="anchor"></span>
	<section class="module module-white">
		<div class="container">
			<div class="homepage-book__banner-wrap hidden-md hidden-lg">
				<div class="homepage-book__banner">
					<?php echo $all_meta[$prefix . 'avail_date_txt']; ?>
				</div>
				<div class="homepage-book__banner-tri"></div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-pad-top">
					<img class="img-responsive img-book center-block" src="<?php echo $all_meta[$prefix . 'book_tilt']; ?>">
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-pull-6">
					<div class="homepage-book__banner-wrap hidden-xs hidden-sm">
						<div class="homepage-book__banner">
							<?php echo $all_meta[$prefix . 'avail_date_txt']; ?>
						</div>
						<div class="homepage-book__banner-tri"></div>
					</div>

					<div class="homepage-book__wordart">
						<img class="img-responsive center-block" src="<?php echo $all_meta[$prefix . 'book_word_art']; ?>" alt="">

						<div class="bookpage-wordart__txt">
							<?php echo apply_filters( 'the_content', $all_meta[$prefix . 'book_word_art_txt'] ); ?>
						</div>
					</div>

				</div>
			</div>

			<?php
				$stores = unserialize( $all_meta[$prefix . 'stores'] );

				if( $stores != '' ) {

					echo '<div class="row bookpage-storesrow">
						<div class="col-sm-6">
							<ul>';

								foreach( $stores as $key => $entry ) {

									echo '<li>
										<a href="'. $entry['url'] .'" target="_blank"><img src="'. $entry['image'] .'" alt=""></a>
									</li>';

								}

							echo '</ul>
						</div>
					</div>';

				}
			?>
		</div>
	</section>

	<span id="expect" class="anchor"></span>
	<section class="module module-white module-expect module-evenrow">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<?php
						echo apply_filters( 'the_content', $all_meta[$prefix . 'expect_txt'] );
					?>
				</div>
				<div class="col-sm-6">
					<?php
						echo wp_get_attachment_image( $all_meta[$prefix . 'expect_img_id'], 'full', false, array( 'class' => 'img-responsive aligncenter' ) );
					?>
				</div>
			</div>
		</div>
	</section>

	<span id="authors" class="anchor"></span>
	<section class="module module-red module-authors" style="background-color: #00a4b4;">
		<div class="container">
			<div class="row">
				<h2 class="text-center"><?php echo $all_meta[$prefix . 'authors_title']; ?></h2>
				<div class="col-md-3">
					<?php
						echo wp_get_attachment_image( $all_meta[$prefix . 'author_left_img_id'], 'full', false, array( 'class' => 'img-responsive aligncenter' ) );
					?>
                    <div class="text-center">Sue Bingham<br>Principal</div>
				</div>
				<div class="col-md-6">
					<?php
						echo apply_filters( 'the_content', $all_meta[$prefix . 'authors_txt'] );
					?>
				</div>
				<div class="col-md-3">
					<?php
						echo wp_get_attachment_image( $all_meta[$prefix . 'author_right_img_id'], 'full', false, array( 'class' => 'img-responsive aligncenter' ) );
					?>
                    <div class="text-center">Zia Fennell<br>Business Consultant</div>
				</div>
			</div>
		</div>
	</section>

		<?php
			$praise = unserialize( $all_meta[$prefix . 'praise'] );
			$count = 1;

			if( $praise != '' ) {

				foreach( $praise as $key => $entry ) {
					$evenodd_class = ( $count % 2 == 0 ) ? 'even' : 'odd';

					if( $evenodd_class == 'odd' ) {

						echo '<div class="module-praise__odd module-evenrow">
							<div class="container">
								<div class="row">
									<div class="col-md-9 module-praise__quote">
										<div class="narrow text-center">
											<span class="text-bigquote">&ldquo;</span>'. $entry['quote'] .'
										</div>
									</div>
									<div class="col-md-3">
										<figure>'.
											wp_get_attachment_image( $entry['image_id'], 'event-tiles', false, array( 'class' => 'img-responsive img-circle center-block module-praise__img' ) ) .'
										</figure>
										<p class="text-center"><strong>'. $entry['fullname'] .'</strong><br>'.
										$entry['jobtitle'] .'</p>
									</div>
								</div>
							</div>
						</div>';

					} else {

						echo '<div class="module-praise__even module-evenrow">
							<div class="container">
								<div class="row">
									<div class="col-md-3">
										<figure>'.
											wp_get_attachment_image( $entry['image_id'], 'event-tiles', false, array( 'class' => 'img-responsive img-circle center-block module-praise__img' ) ) .'
										</figure>
										<p class="text-center"><strong>'. $entry['fullname'] .'</strong><br>'.
										$entry['jobtitle'] .'</p>
									</div>
									<div class="col-md-9 module-praise__quote">
										<div class="narrow text-center">
											<span class="text-bigquote">&ldquo;</span>'. $entry['quote'] .'
										</div>
									</div>
								</div>
							</div>
						</div>';

					}

					$count++;

				}

			}
		?>
	</section>

	<span id="who" class="anchor"></span>
	<section class="module module-white module-whoneeds">
		<div class="container">
			<?php
				echo apply_filters( 'the_content', $all_meta[$prefix . 'who_txt'] );
			?>
		</div>
	</section>

</div>

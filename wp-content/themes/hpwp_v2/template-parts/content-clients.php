<?php
	/**
	 * Template part for displaying page content in page-templates/clients.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'clientspage' ); ?>>
	<section class="module module-white">
		<div class="container">

			<?php while( have_posts() ) {
				the_post();

				the_content();
			} ?>

		</div>
	</section>

	<?php
		/**
		 * clients
		 */
		$client_group = get_post_meta( $pageid, $prefix . 'client_group', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'client_group', 1 ) : '';

		if( $client_group != '' ) {

			echo '<section class="module module-light">
				<div class="container">
					<div class="row">';

						foreach( $client_group as $key => $entry ) {

							if( isset( $entry['image_id'] ) ) {
								$img = wp_get_attachment_image( $entry['image_id'], 'full', null, array( 'class' => 'img-responsive center-block' ) );
							}

							echo '<div class="col-special-client text-center">
								<div class="col-white">'.
									$img .'
								</div>
							</div>';

						}

					echo '</div>
				</div>
			</section>';

		}


		/**
		 * case studies
		 */
		$casestudy_group = get_post_meta( $pageid, $prefix . 'casestudy_group', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'casestudy_group', 1 ) : '';

		if( $casestudy_group != '' ) {

			echo '<section class="module module-white">
				<div class="container">';

					$casestudy_title = get_post_meta( $pageid, $prefix . 'casestudy_title', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'casestudy_title', 1 ) : '';

					if( $casestudy_title != '' ) {
						echo '<h2 class="text-center">'. $casestudy_title .'</h2>';
					}

					echo '<div class="module-evenrow">
						<div class="row">';

							foreach( $casestudy_group as $key => $entry ) {

								$name			= isset( $entry['name'] ) ? '<p class="text-bigger midtext">'. esc_html( $entry['name'] ) .'</p>' : '';
								$subtitle		= isset( $entry['subtitle'] ) ? '<p><strong>'. esc_html( $entry['subtitle'] ) .'</strong></p>' : '';
								$description	= isset( $entry['description'] ) ? apply_filters( 'the_content', ( $entry['description'] ) ) : '';
								$pdf			= isset( $entry['pdf'] ) ? '<p class="text-center tile-stretch__bottom"><a class="btn btn-orange" href="'. esc_html( $entry['pdf'] ) .'">DOWNLOAD</a></p>' : '';

								echo '<div class="col-md-4">
									<div class="tile-white tile-stretch">
										<div>'.
											$name .
											$subtitle .
											$description .'
										</div>
										<div>'.
											$pdf .'
										</div>
									</div>
								</div>';

							}

						echo '</div>
					</div>
				</div>
			</section>';

		}


		/**
		 * featured testimonials section
		 */
		$testi_query = hpwp_testimonial_query( 'leadership' );
		if( $testi_query->have_posts() ) {

			echo '<section class="module module-dark module-testimonials">
				<div class="container">
					<h2 class="text-center">'. hpwp_get_option( 'hpwp_testimonials_title' ) .'</h2>';

					$testimonials_txt = hpwp_get_option( 'hpwp_testimonials_text' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_testimonials_text' ) ) : '';

					if( $testimonials_txt != '' ) {

						echo '<div class="narrow">'.
							$testimonials_txt .'
						</div>';

					}

					hpwp_get_testimonials( $testi_query );

				echo '</div>
			</section>';

		}
	?>
</div>

<?php
/**
 * Template part for displaying page content in page-templates/events.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix			= '_hpwp_';
	$pageid			= get_the_ID();
	$all_meta		= get_meta( $pageid );

	$display_date	= get_display_date( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'eventcpt' ); ?>>
	<section class="module module-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<?php the_content(); ?>

				</div>
			</div>

			<?php
				$form_code = hpwp_get_option( 'hpwp_reg_page_form_coach' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_reg_page_form_coach' ) ) : '';
				$no_cancel = get_post_meta( $pageid, $prefix . 'no_cancel', 1 ) ? 1 : 0;

				if( $form_code != '' && $no_cancel == 0 ) {

					echo '<div class="form-stacked narrow">'. $form_code .'</div>';

				} else {

					echo '<div class="form-stacked narrow">'. apply_filters( 'the_content', hpwp_get_option( 'hpwp_reg_page_form_coach_nocancel' ) ) .'</div>';

				}
			?>
		</div>
	</section>
</div>

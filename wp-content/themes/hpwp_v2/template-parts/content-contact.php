<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'contactpage container' ); ?>>
	<div class="row module-underjumbotron">
		<div class="col-sm-6">

			<?php /*
				<form class="contactform" action="/">
					<div class="form-group">
						<label class="sr-only" for="name">Name</label>
						<input type="text" class="contactform__text form-control" id="name" placeholder="Name">
					</div>
					<div class="form-group">
						<label class="sr-only" for="company">Company</label>
						<input type="text" class="contactform__text form-control" id="company" placeholder="Company">
					</div>
					<div class="form-group">
						<label class="sr-only" for="email">Email</label>
						<input type="email" class="contactform__text form-control" id="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label class="sr-only" for="message">Email</label>
						<textarea class="contactform__textarea form-control" id="message" rows="3" placeholder="Message"></textarea>
					</div>
					<button type="submit" class="btn btn-orange">Submit</button>
				</form>
			*/ ?>

			<div class="form-stacked">

				<?php
					$form_code = hpwp_get_option( 'hpwp_contact_form' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_contact_form' ) ) : '';

					if( $form_code != '' ) {

						echo '<div class="form-stacked">'. $form_code .'</div>';

					}
				?>

			</div>
		</div>
		<div class="col-sm-5 col-sm-offset-1">
			<p><?php echo apply_filters( 'the_content', hpwp_get_option( 'address' ) ); ?></p>

			<?php
				$email = hpwp_get_option( 'hpwp_email' );
				$phone = hpwp_get_option( 'hpwp_phone' );
			?>

			<p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
			<p><?php echo $phone; ?></p>
			<p>Follow us:</p>

			<?php
				// social stuffs
				$social_array = implode( ' ', display_social_media() );

				echo '<p>'.
					$social_array .'
				</p>';
			?>

		</div>
	</div>
</div>

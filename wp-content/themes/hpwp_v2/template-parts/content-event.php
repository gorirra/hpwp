<?php
/**
 * Template part for displaying page content in page-templates/events.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix			= '_hpwp_';
	$pageid			= get_the_ID();
	$all_meta		= get_meta( $pageid );

	$display_date	= get_display_date( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'eventcpt' ); ?>>
	<section class="module module-white">
		<div class="container">
			<div class="row">

				<?php if( get_post_meta( $pageid, $prefix . 'event_img_id', 1 ) !== '' ) { ?>

					<div class="col-md-4">
						<div class="tile-bg text-center text-mid" style="background-image: url( <?php echo wp_get_attachment_image_url( get_post_meta( $pageid, $prefix . 'event_img_id', 1 ), 'event-tiles' ); ?> );">
							<p class="text-bigger"><strong><?php echo $display_date; ?></strong></p>
							<p><?php echo get_the_title(); ?><br>
							<?php echo get_post_meta( $pageid, $prefix . 'event_location', 1 ); ?></p>
						</div>
					</div>
					<div class="col-md-8">
						<?php the_content(); ?>
					</div>

				<?php } else { ?>

					<div class="col-md-12">
						<?php the_content(); ?>
					</div>

				<?php } ?>

			</div>

			<?php /*
				<form action="/" class="narrow">
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="firstname" class="sr-only">First Name</label>
							<input type="text" class="form-control" id="firstname" placeholder="First Name">
						</div>
						<div class="col-md-6 form-group">
							<label for="lastname" class="sr-only">Last Name</label>
							<input type="text" class="form-control" id="lastname" placeholder="Last Name">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="companyname" class="sr-only">Company Name</label>
							<input type="text" class="form-control" id="companyname" placeholder="Company Name">
						</div>
						<div class="col-md-6 form-group">
							<label for="title" class="sr-only">Title</label>
							<input type="text" class="form-control" id="title" placeholder="Title">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="companyaddress1" class="sr-only">Company Address 1</label>
							<input type="text" class="form-control" id="companyaddress1" placeholder="Company Address 1">
						</div>
						<div class="col-md-6 form-group">
							<label for="companyaddress2" class="sr-only">Company Address 2</label>
							<input type="text" class="form-control" id="companyaddress2" placeholder="Company Address 2">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="city" class="sr-only">City</label>
							<input type="text" class="form-control" id="city" placeholder="City">
						</div>
						<div class="col-md-3 form-group">
							<label for="state" class="sr-only">State</label>
							<input type="text" class="form-control" id="state" placeholder="State">
						</div>
						<div class="col-md-3 form-group">
							<label for="zip" class="sr-only">Zip</label>
							<input type="text" class="form-control" id="zip" placeholder="Zip">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="email" class="sr-only">Email Address</label>
							<input type="text" class="form-control" id="email" placeholder="Email Address">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 form-group">
							<label for="workphone" class="sr-only">Work Phone</label>
							<input type="text" class="form-control" id="workphone" placeholder="Work Phone">
						</div>
						<div class="col-md-6 form-group">
							<label for="mobilephone" class="sr-only">Mobile Phone</label>
							<input type="text" class="form-control" id="mobilephone" placeholder="Mobile Phone">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="checkbox">
								<label>
									<input type="checkbox"> * Please read our cancellation policy:<br>
									We will provide a 100% credit for workshop cancellations up to 30 days prior to an event (after that, there is no credit). Credits can be used for one full year at any public workshop. In the event that HPWP cancels a workshop, we will promptly refund all payments.
								</label>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="aligncenter btn btn-orange">Submit</button>
						</div>
					</div>
				</form>
			*/ ?>

			<?php
				$form_code = hpwp_get_option( 'hpwp_reg_page_form' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_reg_page_form' ) ) : '';
				$no_cancel = get_post_meta( $pageid, $prefix . 'no_cancel', 1 ) ? 1 : 0;

				if( $form_code != '' && $no_cancel == 0 ) {

					echo '<div class="form-stacked narrow">'. $form_code .'</div>';

				} else {

					echo '<div class="form-stacked narrow">'. apply_filters( 'the_content', hpwp_get_option( 'hpwp_reg_page_form_nocancel' ) ) .'</div>';

				}
			?>
		</div>
	</section>
</div>

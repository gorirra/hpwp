<?php
/**
 * Template part for displaying page content in page-templates/events.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'eventspage' ); ?>>
	<section class="module module-white">
		<div class="container">
			<div class="row-flex">
				<div class="col-md-6">
					<h2><?php echo $all_meta[$prefix . 'left_content_title']; ?></h2>

					<?php
						$left_content = array_key_exists( $prefix . 'left_content', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'left_content'] ) : '';

						if( $left_content != '' ) {
							echo $left_content;
						}

						$youtube = array_key_exists( $prefix . 'youtube_id', $all_meta ) ? $all_meta[$prefix . 'youtube_id'] : '';

						if( $youtube != '' ) { ?>

							<div class="embed-responsive embed-responsive-16by9 embed-margins">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>

						<?php }
					?>

				</div>


				<div class="col-md-5">

					<?php
						$right_content = array_key_exists( $prefix . 'right_content', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'right_content'] ) : '';

						if( $right_content != '' ) {
							echo $right_content;
						}
					?>

				</div>
			</div>
		</div>
	</section>

	<?php
		$sub_hero_mobile	= get_post_meta( $pageid, $prefix . 'subheromobile_img', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'subheromobile_img', 1 ) : '';
		$sub_hero_desktop	= get_post_meta( $pageid, $prefix . 'subhero_img', 1 ) != '' ? get_post_meta( $pageid, $prefix . 'subhero_img', 1 ) : '';
	?>

	<style>

		<?php if( $sub_hero_mobile != '' ) { ?>

			.jumbotron-sub {
				background-image: url( <?php echo $sub_hero_mobile; ?> );
			}
			@media screen and (min-width: 640px ) {
				.jumbotron-sub {
					background-image: url( <?php echo $sub_hero_desktop; ?> );
				}
			}

		<?php } else { ?>

			.jumbotron-sub {
				background-image: url( <?php echo $sub_hero_desktop; ?> );
			}

		<?php } ?>

	</style>

	<section class="jumbotron jumbotron-sub">
		<div class="container">
			<div class="content">

				<?php
					echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'subhero_headline', 1 ) );
					echo apply_filters( 'the_content', get_post_meta( $pageid, $prefix . 'subhero_txt', 1 ) );
				?>

			</div>
		</div>
	</section>


	<section class="module module-white">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<?php
						$cta_left = array_key_exists( $prefix . 'cta_left', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'cta_left'] ) : '';

						echo $cta_left;
					?>

				</div>
				<div class="col-md-6">

					<?php
						$cta_right = array_key_exists( $prefix . 'cta_right', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'cta_right'] ) : '';

						echo $cta_right;
					?>

				</div>
			</div>
		</div>
	</section>


	<?php
		$events_query		= hpwp_events_query();
		$events_query_empty	= array_filter( $events_query->posts );

		echo '<section class="module module-light">
			<div class="container">
				<h2 class="text-center">'. $all_meta[$prefix . 'event_dates_title'] .'</h2>';

				if( !empty( $events_query_empty ) ) {

					hpwp_get_events( $events_query );

				} else {

					echo '<p class="text-center">There are no scheduled events at this time.</p>';

				}

			echo '</div>
		</section>';
	?>


	<section class="module module-dark module-testimonials">
		<div class="container">
			<h2 class="text-center"><?php echo $all_meta[$prefix . 'testi_title']; ?></h2>

			<?php
				$testimonial_txt = array_key_exists( $prefix . 'testi_txt', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'testi_txt'] ) : '';

				if( $testimonial_txt != '' ) {

					echo '<div class="narrow">'.
						$testimonial_txt .'
					</div>';

				}

				$testi_query = hpwp_testimonial_query( 'leadership' );
				hpwp_get_testimonials( $testi_query );
			?>

		</div>
	</section>
</div>

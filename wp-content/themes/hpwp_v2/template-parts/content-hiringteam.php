<?php
/**
 * Template part for displaying page content in page-templates/events.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>
<div id="post-<?php the_ID(); ?>" <?php post_class( 'hiringpage' ); ?>>
	
	<?php
		/**
		 * section 1
		 */
	?>
	<section class="module module-white">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<?php
						$left_content_s1 = array_key_exists( $prefix . 'left_content_s1', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'left_content_s1'] ) : '';

						if( $left_content_s1 != '' ) {
							echo $left_content_s1;
						}
					?>

				</div>
				<div class="col-md-6">

					<?php
						$right_img_s1 = array_key_exists( $prefix . 'right_img_s1', $all_meta ) ? wp_get_attachment_image( $all_meta[$prefix . 'right_img_s1_id'], 'full', null, array( 'class' => 'img-responsive center-block' ) ) : '';
						echo $right_img_s1;
					?>

				</div>
			</div>
		</div>
	</section>

	<?php
		/**
		 * section 2
		 */
	?>
	<section class="module module-light module-marginbottom">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					
					<?php
						$left_img_s2 = array_key_exists( $prefix . 'left_img_s2', $all_meta ) ? wp_get_attachment_image( $all_meta[$prefix . 'left_img_s2_id'], 'full', null, array( 'class' => 'img-responsive center-block' ) ) : '';
						echo $left_img_s2;
					?>

				</div>
				<div class="col-md-6 hiringpage-bullets">
					
					<?php
						$right_content_s2 = array_key_exists( $prefix . 'right_content_s2', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'right_content_s2'] ) : '';

						if( $right_content_s2 != '' ) {
							echo $right_content_s2;
						}
					?>

				</div>
			</div>
		</div>
	</section>

	<?php 
		/**
		 * section 3
		 */
	?>
	<a id="hiringteam-learnmore" class="anchor"></a>
	<section class="module module-gradient">
		<div class="container">
			<div class="row row-flex__aligncenter">
				<div class="col-md-6">
					
					<?php
						$left_content_s3 = array_key_exists( $prefix . 'left_content_s3', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'left_content_s3'] ) : '';

						if( $left_content_s3 != '' ) {
							echo $left_content_s3;
						}
					?>

				</div>
				<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-0">
					<h2>CONTACT US</h2>

					<?php 
						$form_s3 = array_key_exists( $prefix . 'form_s3', $all_meta ) ? $all_meta[$prefix . 'form_s3'] : '';

						if( $form_s3 != '' ) {

							echo '<div class="form-stacked form-widesubmit">'.
								apply_filters( 'the_content', $form_s3 ) .'
							</div>';
						}
					?>

				</div>
			</div>
		</div>
	</section>

	<?php 
		/**
		 * section 4
		 */
	?>
	<section class="module module-white">
		<div class="container">
			<div class="row">

				<?php 
					$title_s4 = array_key_exists( $prefix . 'title_s4', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'title_s4'] ) : '';

					if( $title_s4 != '' ) {
						echo '<h2 class="text-center">'. $title_s4 .'</h2>';
					}

					$group_s4 = array_key_exists( $prefix . 'group_s4', $all_meta ) ? unserialize( $all_meta[$prefix . 'group_s4'] ) : '';

					if( $group_s4 != '' ) {
						foreach( $group_s4 as $key => $entry ) {

							echo '<div class="col-sm-4">
								<figure>'. wp_get_attachment_image( $entry['image_id'], 'full', null, array( 'class' => 'img-responsive center-block' ) ) .'</figure>' .

								apply_filters( 'the_content', $entry['column_txt'] ) .'

							</div>';

						}
					}
				?>

			</div>
		</div>
	</section>

</div>
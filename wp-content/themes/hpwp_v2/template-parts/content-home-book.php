<?php
    /**
     * Template part for displaying page content in page-templates/homepage.php
     *
     * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
     *
     * @package hpwp_v2
     */

    $prefix        = '_hpwp_';
    $pageid        = get_the_ID();
    $all_meta    = get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'homepage-book' ); ?>>
    <?php
        if(array_key_exists($prefix . 'banner_txt', $all_meta)) {
            $banner_url = array();

            if(array_key_exists($prefix . 'banner_url', $all_meta)) {
                $banner_url_cta = '<a class="btn btn-blue" href="'. $all_meta[$prefix . 'banner_url'] .'">'. $all_meta[$prefix . 'banner_cta'] .'</a>';
            }

            echo '<div class="homepage-book__announcement">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">'.
                            $all_meta[$prefix . 'banner_txt'] .
                            $banner_url_cta .'
                        </div>
                    </div>
                </div>
            </div>';
        }
    ?>
    <section class="module module-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="homepage-book__wordart">
                        <img class="img-responsive center-block" src="<?php echo $all_meta[$prefix . 'book_word_art']; ?>" alt="">
                    </div>
                </div>

                <div class="col-md-6"><style>
    .youtube-player {
        position: relative;
        padding-bottom: 56.23%;
        /* Use 75% for 4:3 videos */
        height: 0;
        overflow: hidden;
        max-width: 100%;
        background: #000;
        margin: 5px;
    }

    .youtube-player iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 100;
        background: transparent;
    }

    .youtube-player img {
        bottom: 0;
        display: block;
        left: 0;
        margin: auto;
        max-width: 100%;
        width: 100%;
        position: absolute;
        right: 0;
        top: 0;
        border: none;
        height: auto;
        cursor: pointer;
        -webkit-transition: .4s all;
        -moz-transition: .4s all;
        transition: .4s all;
    }

    .youtube-player img:hover {
        -webkit-filter: brightness(75%);
    }

    .youtube-player .play {
        height: 80px;
        width: 80px;
        left: 50%;
        top: 50%;
        margin-left: -40px;
        margin-top: -40px;
        position: absolute;
        background: url("<?php print get_stylesheet_directory_uri(); ?>/images/owl.video.play.png") no-repeat;
        cursor: pointer;
    }

</style>

                    <?php
                        $youtube = array_key_exists( $prefix . 'youtube_id', $all_meta ) ? $all_meta[$prefix . 'youtube_id'] : '';

                        if( $youtube != '' ) { ?>

                            <!-- More Efficient YouTube Embeds --->
                            <div class="embed-responsive embed-responsive-16by9"><div class="youtube-player" data-id="<?php echo $youtube; ?>"></div></div>
                            <script>

                                /* Light YouTube Embeds by @labnol */
                                /* Web: http://labnol.org/?p=27941 */

                                document.addEventListener("DOMContentLoaded",
                                    function() {
                                        var div, n,
                                            v = document.getElementsByClassName("youtube-player");
                                        for (n = 0; n < v.length; n++) {
                                            div = document.createElement("div");
                                            div.setAttribute("data-id", v[n].dataset.id);
                                            div.innerHTML = labnolThumb(v[n].dataset.id);
                                            div.onclick = labnolIframe;
                                            v[n].appendChild(div);
                                        }
                                    });

                                function labnolThumb(id) {
                                    var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
                                        play = '<div class="play"></div>';
                                    return thumb.replace("ID", id) + play;
                                }

                                function labnolIframe() {
                                    var iframe = document.createElement("iframe");
                                    var embed = "https://www.youtube.com/embed/ID?autoplay=1";
                                    iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
                                    iframe.setAttribute("frameborder", "0");
                                    iframe.setAttribute("allowfullscreen", "1");
                                    this.parentNode.replaceChild(iframe, this);
                                }

                            </script>
                            
                            <?php /*
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            */ ?>
                        <?php }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <?php
        /**
         * what we do section
         */
    ?>
    <section class="module module-light">
        <div class="container">
            <h2 class="text-center"><?php echo $all_meta[$prefix . 'whatwedo_txt']; ?></h2>

            <?php
                $specialty    = array_key_exists( $prefix . 'whatwedo_group', $all_meta ) ? unserialize( $all_meta[$prefix . 'whatwedo_group'] ) : '';
                $count        = 1;

                if( $specialty != '' ) {

                    foreach( $specialty as $key => $entry ) {

                        echo '<div class="media narrow">';

                            $img = array_key_exists( 'image', $entry ) && $entry['image'] != '' ? '<div class="media-left media-middle">'. display_svg( $entry['image'] ).'</div>' : '';

                            echo $img .'
                            <div class="media-body media-middle">'.
                                apply_filters( 'the_content', $entry['description'] ) .'
                            </div>';

                        echo '</div>';

                    }

                }
            ?>

        </div>
    </section>


    <?php
        /**
         * results you can expect section
         */
        $results = array_key_exists( $prefix . 'results', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'results'] ): '';

        if( $results != '' ) {

            echo '<section class="module module-duo">
                <div class="container-fluid">
                    <div class="row module-duo__row">';

                        $results_img = array_key_exists( $prefix . 'results_img', $all_meta ) ? 'style="background-image: url('. $all_meta[$prefix . 'results_img'] .');"' : '';

                        echo '<div class="odd col-md-6 module-duo__img" '. $results_img .'></div>
                        <div class="even col-md-6">
                            <div class="module-duo__column">'.
                                $results .'
                            </div>
                        </div>
                    </div>
                </div>
            </section>';

        }


        /**
         * featured testimonials section
         */
        /*
        $testi_query = hpwp_testimonial_query( 'home' );

        if( $testi_query->have_posts() ) {

            echo '<section class="module module-dark module-testimonials">
                <div class="container">
                    <h2 class="text-center">'. hpwp_get_option( 'hpwp_testimonials_title' ) .'</h2>';

                    $testimonials_txt = hpwp_get_option( 'hpwp_testimonials_text' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_testimonials_text' ) ) : '';

                    if( $testimonials_txt != '' ) {

                        echo '<div class="narrow">'.
                            $testimonials_txt .'
                        </div>';

                    }

                    hpwp_get_testimonials( $testi_query );

                echo '</div>
            </section>';

        }
        */


        /**
         * book section
         */
    ?>
    <section class="module module-white module-no-padding">
        <div class="container">
            <div class="homepage-book__banner-wrap hidden-sm hidden-md hidden-lg">
                <div class="homepage-book__banner">
                    <?php echo $all_meta[$prefix . 'avail_date_txt']; ?>
                </div>
                <div class="homepage-book__banner-tri"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-pad-top">
                    <img class="img-responsive img-book center-block" src="<?php echo $all_meta[$prefix . 'book_tilt']; ?>">
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-pull-6">
                    <div class="homepage-book__banner-wrap hidden-xs">
                        <div class="homepage-book__banner">
                            <?php echo $all_meta[$prefix . 'avail_date_txt']; ?>
                        </div>
                        <div class="homepage-book__banner-tri"></div>
                    </div>

                    <div class="homepage-book__wordart">
                        <div class="bookpage-wordart__txt">
                            <?php echo apply_filters( 'the_content', $all_meta[$prefix . 'book_word_art_txt'] ); ?>
                        </div>
                    </div>

                    <?php
                        $stores = unserialize( $all_meta[$prefix . 'stores'] );

                        if( $stores != '' ) {

                            echo '<div class="row bookpage-storesrow">
                                <div class="col-lg-10">
                                    <ul>';

                                        foreach( $stores as $key => $entry ) {

                                            echo '<li>
                                                <a href="'. $entry['url'] .'" target="_blank"><img src="'. $entry['image'] .'" alt=""></a>
                                            </li>';

                                        }

                                    echo '</ul>
                                </div>
                            </div>';

                        }
                    ?>

                </div>
            </div>
        </div>
    </section>


    <?php
        /**
         * team section
         * grabs content from the team cpt
         */

        $team_query = hpwp_get_team();

        if( $team_query->have_posts() ) {

            echo '<section class="module module-white module-teamlist__less">
                <div class="container-fluid">
                    <div class="row">
                        <h2 class="text-center">'. $all_meta[$prefix . 'team_title'] .'</h2>
                        <div class="owl-carousel__three-padding owl-carousel">';

                            while( $team_query->have_posts() ) {
                                $team_query->the_post();

                                echo '<div>'.
                                    get_the_post_thumbnail() .'
                                    <p>'. get_the_title() .'</p>
                                </div>';

                            }

                        echo '</div>
                    </div>
                </div>
                <div class="container">
                    <p class="text-right"><a href="'. site_url( '/about/' ) .'" class="orangetext">SEE THE ENTIRE TEAM &raquo;</a></p>
                </div>
            </section>';

            wp_reset_postdata();

        }

    ?>


    <!-- cta -->
    <?php
        $bottom_cta_txt = array_key_exists( $prefix . 'bottom_cta', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'bottom_cta'] ) : '';

        if( $bottom_cta_txt != '' ) {

            $cta_bg = array_key_exists( $prefix . 'bottom_bg', $all_meta ) ? $all_meta[$prefix . 'bottom_bg'] : '';

            echo '<section class="module module-background" style="background-image: url( '. $cta_bg .' );">
                <div class="container">'.
                    $bottom_cta_txt .'
                </div>
            </section>';

        }
    ?>

</div>

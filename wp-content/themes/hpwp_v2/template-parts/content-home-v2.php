<?php
	/**
	 * Template part for displaying page content in page-templates/homepage.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'homepage' ); ?>>
	<?php
		/**
		 * what we do section
		 */
	?>
	<section class="module module-white">
		<div class="container">
			<h2 class="text-center"><?php echo $all_meta[$prefix . 'whatwedo_txt']; ?></h2>

			<?php
				$specialty	= array_key_exists( $prefix . 'whatwedo_group', $all_meta ) ? unserialize( $all_meta[$prefix . 'whatwedo_group'] ) : '';
				$count		= 1;

				if( $specialty != '' ) {

					foreach( $specialty as $key => $entry ) {

						echo '<div class="media narrow">';

							// $img = array_key_exists( 'image', $entry ) ? '<img class="aligncenter" src="'. $entry['image'] .'" alt="">' : '';
							$img = array_key_exists( 'image', $entry ) && $entry['image'] != '' ? '<div class="media-left media-middle">'. display_svg( $entry['image'] ).'</div>' : '';

							echo $img .'
							<div class="media-body media-middle">'.
								apply_filters( 'the_content', $entry['description'] ) .'
							</div>';

						echo '</div>';

					}

				}
			?>

		</div>
	</section>


	<?php
		/**
		 * results you can expect section
		 */
		$results = array_key_exists( $prefix . 'results', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'results'] ): '';

		if( $results != '' ) {

			echo '<section class="module module-duo">
				<div class="container-fluid">
					<div class="row module-duo__row">';

						$results_img = array_key_exists( $prefix . 'results_img', $all_meta ) ? 'style="background-image: url('. $all_meta[$prefix . 'results_img'] .');"' : '';

						echo '<div class="odd col-md-6 module-duo__img" '. $results_img .'></div>
						<div class="even col-md-6">
							<div class="module-duo__column">'.
								$results .'
							</div>
						</div>
					</div>
				</div>
			</section>';

		}


		/**
		 * featured testimonials section
		 */
		$testi_query = hpwp_testimonial_query( 'home' );

		if( $testi_query->have_posts() ) {

			echo '<section class="module module-dark module-testimonials">
				<div class="container">
					<h2 class="text-center">'. hpwp_get_option( 'hpwp_testimonials_title' ) .'</h2>';

					$testimonials_txt = hpwp_get_option( 'hpwp_testimonials_text' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_testimonials_text' ) ) : '';

					if( $testimonials_txt != '' ) {

						echo '<div class="narrow">'.
							$testimonials_txt .'
						</div>';

					}

					hpwp_get_testimonials( $testi_query );

				echo '</div>
			</section>';

		}



		/**
		 * team section
		 * grabs content from the team cpt
		 */

		$team_query = hpwp_get_team();

		if( $team_query->have_posts() ) {

			echo '<section class="module module-white module-teamlist">
				<div class="container-fluid">
					<div class="row">
						<h2 class="text-center">'. $all_meta[$prefix . 'team_title'] .'</h2>
						<div class="owl-carousel__three-padding owl-carousel">';

							while( $team_query->have_posts() ) {
								$team_query->the_post();

								echo '<div>'.
									get_the_post_thumbnail() .'
									<p>'. get_the_title() .'</p>
								</div>';

							}

						echo '</div>
					</div>
				</div>
				<div class="container">
					<p class="text-right"><a href="'. site_url( '/about/' ) .'" class="orangetext">SEE THE ENTIRE TEAM &raquo;</a></p>
				</div>
			</section>';

			wp_reset_postdata();

		}

	?>


	<!-- cta -->
	<?php
		$bottom_cta_txt = array_key_exists( $prefix . 'bottom_cta', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'bottom_cta'] ) : '';

		if( $bottom_cta_txt != '' ) {

			$cta_bg = array_key_exists( $prefix . 'bottom_bg', $all_meta ) ? $all_meta[$prefix . 'bottom_bg'] : '';

			echo '<section class="module module-background" style="background-image: url( '. $cta_bg .' );">
				<div class="container">'.
					$bottom_cta_txt .'
				</div>
			</section>';

		}
	?>
</div>

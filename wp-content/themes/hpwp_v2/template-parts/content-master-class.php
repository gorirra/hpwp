<?php
/**
 * Template part for displaying page content in page-templates/master-class.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'masterclasspage' ); ?>>
	<section class="module module-gradient">
		<div class="container">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2 col-md-5 col-md-offset-1 text-center">
                    <?php
                        $left_stat = array_key_exists( $prefix . 'left_stat', $all_meta ) ? $all_meta[$prefix . 'left_stat'] : '';
                        $left_stat_content = array_key_exists( $prefix . 'left_stat_content', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'left_stat_content'] ) : '';

                        echo '
                            <p class="text-huge">'. $left_stat .'</p>
                            <div class="text-bigger">'. $left_stat_content .'</div>
                        ';
                    ?>
                </div>

				<div class="col-xs-8 col-xs-offset-2 col-md-5 col-md-offset-1 col-md-offset-0 text-center">
                    <?php
                        $right_stat = array_key_exists( $prefix . 'right_stat', $all_meta ) ? $all_meta[$prefix . 'right_stat'] : '';
                        $right_stat_content = array_key_exists( $prefix . 'right_stat_content', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'right_stat_content'] ) : '';

                        echo '
                            <p class="text-huge">'. $right_stat .'</p>
                            <div class="text-bigger">'. $right_stat_content .'</div>
                        ';
                    ?>
				</div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-bigger margin-top-medium">
                    <?php
                        $under_stats_copy = array_key_exists( $prefix . 'under_stats_copy', $all_meta ) ? apply_filters('the_content', $all_meta[$prefix . 'under_stats_copy']) : '';

                        echo $under_stats_copy;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="module module-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                                $quotes = array_key_exists( $prefix . 'quotes', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'quotes'] ) : '';

                                if($quotes != '') {
                                    echo $quotes;
                                }
                            ?>
                        </div>
                        <div class="col-md-8">
                            <?php
                                $placeholder_img = array_key_exists( $prefix . 'placeholder_img_id', $all_meta ) ? $all_meta[$prefix . 'placeholder_img_id'] : '';
                                $youtube = array_key_exists( $prefix . 'youtube_id', $all_meta ) ? $all_meta[$prefix . 'youtube_id'] : '';

                                if($placeholder_img != '') {
                                    echo wp_get_attachment_image( $placeholder_img, 'full', '', array( "class" => "img-responsive" ));
                                } elseif( $youtube != '' ) { ?>

                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>

                                <?php }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="module module-light">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <?php
                        $rhetorical_questions = array_key_exists( $prefix . 'rhetorical_questions', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'rhetorical_questions'] ) : '';

                        if($rhetorical_questions != '') {
                            echo $rhetorical_questions;
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="module module-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php
                        $offset_left_content = array_key_exists( $prefix . 'offset_left_content', $all_meta ) ? $all_meta[$prefix . 'offset_left_content'] : '';

                        echo '<h3 class="margin-top-none whitetext text-lighter">'. $offset_left_content .'</h3>';
                    ?>
                </div>
                <div class="col-md-9">
                    <?php
                        $offset_right_content = array_key_exists( $prefix . 'offset_right_content', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'offset_right_content']) : '';

                        echo $offset_right_content;
                    ?>
                </div>
                <div class="col-md-12">
                    <?php
                        $agenda = array_key_exists( $prefix . 'agenda_pdf', $all_meta ) ? $all_meta[$prefix . 'agenda_pdf'] : '';

                        if($agenda != '') {
                            echo '<p class="margin-top-medium text-center"><a href="'. $agenda .'" class="btn btn-orange btn-lg" target="_blank">Download the Agenda</a></p>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'err-404' ); ?>>
	<section class="no-results not-found">
		<h1><?php esc_html_e( 'Nothing Found', 'hpwp_v2' ); ?></h1>

		<div class="page-content">
			<?php
				if ( is_search() ) : ?>

					<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'hpwp_v2' ); ?></p>
					<?php
						get_search_form();

				else : ?>

					<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'hpwp_v2' ); ?></p>
					<?php
						get_search_form();

				endif;
			?>
		</div><!-- .page-content -->
	</section><!-- .no-results -->
</div>
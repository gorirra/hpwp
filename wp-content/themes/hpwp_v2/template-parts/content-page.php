<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

$prefix = '_hpwp_';

?>

<div id="post-<?php the_ID(); ?>">
	<section class="module module-white">
		<div class="container">

			<?php
				$hero_desktop = get_post_meta( get_the_ID(), $prefix . 'hero_img', 1 ) != '' ? get_post_meta( get_the_ID(), $prefix . 'hero_img', 1 ) : '';

				if( $hero_desktop == '' ) {

					the_title( '<h1>', '</h1>' );

				}

				the_content();
			?>

		</div>
	</section>
</div>
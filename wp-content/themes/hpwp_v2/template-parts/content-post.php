<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

$prefix = '_hpwp_';

?>

<div id="post-<?php the_ID(); ?>">
	<section class="module module-white">
		<div class="container">

			<div class="col-sm-8">

				<?php 
					the_content();
				?>

			</div>

			<div class="col-sm-4">

				<div class="recent-posts widget">
					<?php // Recent Posts
				   
				    // the query
				    $recentposts_query = new WP_Query( array(
				      'posts_per_page' => 3,
				    )); 
				
					if ( $recentposts_query->have_posts() ) : 

					echo "<h4>Recent Posts</h4>";

					echo "<ul>";

					   while ( $recentposts_query->have_posts() ) : $recentposts_query->the_post(); ?>
					   	<li>
						   	<a href="<?php the_permalink();?>">
							    <?php 
								    the_title(); 
								    echo '<br/><sub>' . get_the_date() . '</sub>'; 
							    ?>
							</a>
						</li>
					    
					  <?php endwhile; 

					echo "</ul>"; ?>

					  <?php wp_reset_postdata(); ?>

					<?php else : ?>
					  <p><?php __('No News'); ?></p>
					<?php endif; ?>
				</div>
				<br/>
				<div class="categories widget">
					<?php 
						
						  $atts = shortcode_atts(
					      array(
					        'child_of'            => 0,
					        'current_category'    => 0,
					        'depth'               => 0,
					        'echo'                => 1,
					        'hide_empty'          => 1,
					        'hide_title_if_empty' => false,
					        'hierarchical'        => 1,
					        'include'             => '',
					        'number'              => null,
					        'order'               => 'ASC',
					        'orderby'             => 'name',
					        'pad_counts'          => 0,
					        'show_count'          => 0,
					        'show_option_all'     => '',
					        'show_option_none'    => __( 'No categories' ),
					        'style'               => 'list',
					        'taxonomy'            => 'category',
					        'title_li'            => __( '' ),
					        'use_desc_for_title'  => 1,
					        'walker'              => null,
					      ), $atts
					    );
						echo "<h4>Blog Categories</h4>";
						echo "<ul>";
					    wp_list_categories($atts);
					    echo "</ul>";
						
					?>
				</div>
		</div>
	</section>
</div>
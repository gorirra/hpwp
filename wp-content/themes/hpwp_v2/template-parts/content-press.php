<?php
	/**
	 * Template part for displaying page content in page-templates/about.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix	= '_hpwp_';
	$paged	= get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$big	= 999999999; // need an unlikely integer for pagination
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'presspage' ); ?>>

	<section class="module module-white">
		<div class="container">
			<div class="row">

				<?php
					$logo_group = get_post_meta( get_the_ID(), $prefix . 'logo_group', 1 ) ? get_post_meta( get_the_ID(), $prefix . 'logo_group', 1 ) : '';

					if( $logo_group != '' ) {

						echo '<section class="module module-white">
							<div class="container">
								<p class="text-center">As seen in:</p>

								<div class="owl-carousel owl-carousel__five">';

									foreach( $logo_group as $key => $entry ) {

										$name = array_key_exists( 'name', $entry) ? $entry['name'] : '';

										echo '<div>
											<img class="img-responsive aligncenter" src="'. wp_get_attachment_image_url( $entry['image_id'], 'thumbnail' ) .'" alt="'. $name .'">
										</div>';

									}

								echo '</div>
								<div class="owl-carousel__customnav"></div>
							</div>
						</section>';

					}
				?>

			</div>
		</div>
	</section>


	<?php
		if( false === ( $press_query = get_transient( 'press_query_results_' . $paged ) ) ) {

			$args = array(
				'post_type'					=> 'press',
				'posts_per_page'			=> 9,
				'post_status'				=> 'publish',
				'order'						=> 'DESC',
				'orderby'					=> 'meta_value_num',
				'meta_key'					=> $prefix . 'press_date',
				'ignore_sticky_posts'		=> true,
				'no_found_rows'				=> false,
				'update_post_meta_cache'	=> false,
				'paged'						=> $paged,
			);

			$press_query = new WP_Query( $args );
			set_transient( 'press_query_results_' . $paged, $press_query, 1 * MINUTE_IN_SECONDS );

		}

		if( $press_query->have_posts() ) {

			echo '<section class="module module-underjumbotron module-press module-evenrow">
				<div class="container">
					<div class="row">';

						while( $press_query->have_posts() ) {
							$press_query->the_post();

							$pressid	= get_the_ID();
							$date		= date( 'F j, Y', get_post_meta( $pressid, $prefix . 'press_date', 1 ) );
							$press_name	= get_post_meta( $pressid, $prefix . 'press_name', 1 ) ? '<p><small>'. get_post_meta( $pressid, $prefix . 'press_name', 1 ) .'</small></p>' : '';

							echo '<div class="col-sm-6 col-md-4">

								<div class="tile-white">';

									$stock_img_id = get_post_meta( $pressid, $prefix . 'press_stock_id', 1 ) != '' ? get_post_meta( $pressid, $prefix . 'press_stock_id', 1 ) : '';

									if( $stock_img_id != '' ) {

										echo '<div class="tile-img" style="background-image: url( '. wp_get_attachment_image_url( $stock_img_id, 'tile-img' ) .' );"></div>';

									}

									// <div class="row">
									// 	<div class="col-xs-6 col-md-6">
									// 		<p><small>'. $date .'</small></p>
									// 	</div>
									// 	<div class="col-xs-6 col-md-6">
									// 		<p class="alignright">'. wp_get_attachment_image( get_post_meta( $pressid, $prefix . 'press_logo_id', 1 ), 'thumbnail' ) .'</p>
									// 	</div>
									// </div>

									echo '<p><small>'. $date .'</small></p>
									<h4><a target="_blank" href="'. get_post_meta( $pressid, $prefix . 'press_url', 1 ) .'">'. get_the_title() .'</a></h4>'.
									$press_name .'
									<p>'. apply_filters( 'the_content', get_the_content() ) .'</p>
									<p><small><a class="orangetext" target="_blank" href="'. get_post_meta( $pressid, $prefix . 'press_url', 1 ) .'">Read More &raquo;</a></small></p>
								</div>

							</div>';

						}

					echo '</div>

					<nav class="alignright" aria-label="Page navigation">
						<ul class="pagination">';

							$pagination_array = paginate_links( array(
								'current'	=> max( 1, $paged ),
								'total'		=> $press_query->max_num_pages,
								'type'		=> 'array',
							) );

							if( $pagination_array != '' ) {

								foreach( $pagination_array as $pagenumber ) {

									$current = strpos( $pagenumber, 'current' );

									if( $current === false ) {
										$active_class = '';
									} else {
										$active_class = ' class="active"';
									}

									echo '<li'. $active_class .'>'.
										$pagenumber .'
									</li>';

								}

							}

						echo '</ul>
					</nav>

				</div>
			</section>';

			wp_reset_postdata();
		}

		$press_content = get_post_meta( get_the_ID(), $prefix . 'press_contact_txt', 1 ) != '' ? apply_filters( 'the_content', get_post_meta( get_the_ID(), $prefix . 'press_contact_txt', 1 ) ) : '';

		if( $press_content != '' ) {

			echo '<section class="module module-light">
				<div class="container">
					<div class="narrow text-center">
						<h2>'. get_post_meta( get_the_ID(), $prefix . 'press_contact_title', 1 ) .'</h2>'.

						$press_content .'

					</div>
				</div>
			</section>';

		}
	?>

</div>

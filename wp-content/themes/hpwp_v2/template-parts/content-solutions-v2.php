<?php
	/**
	 * Template part for displaying page content in page-templates/solutions.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'solutionspage' ); ?>>

	<span class="anchor" id="solution-1" name="solution-1"></span>

	<section class="module module-texttoppadding">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><?php echo $all_meta[$prefix . 'solution1_title']; ?></h2>

					<?php
						$left_content1 = array_key_exists( $prefix . 'solution1_left', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'solution1_left'] ) : '';

						if( $left_content1 != '' ) {
							echo $left_content1;
						}
					?>

				</div>
				<div class="col-md-5 col-md-offset-1 col-dark">

					<?php
						$right_content1 = array_key_exists( $prefix . 'solution1_right', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'solution1_right'] ) : '';

						if( $right_content1 != '' ) {
							echo $right_content1;
						}
					?>

				</div>
			</div>
		</div>
	</section>


	<section class="module module-white">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<?php
						$cta_left = array_key_exists( $prefix . 'cta_left', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'cta_left'] ) : '';

						echo $cta_left;
					?>

				</div>
				<div class="col-md-6">

					<?php
						$cta_right = array_key_exists( $prefix . 'cta_right', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'cta_right'] ) : '';

						echo $cta_right;
					?>

				</div>
			</div>
		</div>
	</section>

	<?php if( get_field( 'extra_content' ) ) : ?>
		<section class="module module-light" style="background:#EFF4F4;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php echo the_field('extra_content'); ?>
					</div><!-- end div col-md-12 -->
				</div><!-- end div row -->				
			</div><!-- end div container -->
		</section><!-- end section module module-light -->
	<?php endif; ?>

	<?php
		echo '<section class="module module-light">
			<div class="container">
				<h2 class="text-center">'. hpwp_get_option( 'hpwp_event_dates_title' ) .'</h2>';

				$events_query		= hpwp_events_query();
				$events_query_empty	= array_filter( $events_query->posts );

				if( !empty( $events_query_empty ) ) {

					hpwp_get_events( $events_query, true );

				} else {

					echo '<p class="text-center">There are no scheduled events at this time.</p>';

				}

			echo '</div>
		</section>';


		/**
		 * featured testimonials section
		 */
		$testi_query = hpwp_testimonial_query( 'leadership' );
		if( $testi_query->have_posts() ) {

			echo '<section class="module module-dark module-testimonials">
				<div class="container">
					<h2 class="text-center">'. hpwp_get_option( 'hpwp_testimonials_title' ) .'</h2>';

					$testimonials_txt = hpwp_get_option( 'hpwp_testimonials_text' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_testimonials_text' ) ) : '';

					if( $testimonials_txt != '' ) {

						echo '<div class="narrow">'.
							$testimonials_txt .'
						</div>';

					}

					hpwp_get_testimonials( $testi_query );

				echo '</div>
			</section>';

		}
	?>

	<span class="anchor" id="solution-2" name="solution-2"></span>

	<section class="module module-texttoppadding">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><?php echo $all_meta[$prefix . 'solution2_title']; ?></h2>

					<?php
						$left_content2 = array_key_exists( $prefix . 'solution2_left', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'solution2_left'] ) : '';

						if( $left_content2 != '' ) {
							echo $left_content2;
						}
					?>

				</div>
				<div class="col-md-5 col-md-offset-1 col-dark">

					<?php
						$right_content2 = array_key_exists( $prefix . 'solution2_right', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'solution2_right'] ) : '';

						if( $right_content2 != '' ) {
							echo $right_content2;
						}
					?>
				</div>
			</div>
		</div>
	</section>

	<section class="module module-white">
		<div class="container">
			<h2 class="text-center text-lighter"><?php echo $all_meta[$prefix . 'contact_title']; ?></h2>

			<div class="form-inline">
				<?php echo apply_filters( 'the_content', $all_meta[$prefix . 'book_form'] ); ?>
			</div>

		</div>
	</section>
</div>

<?php
/**
 * Template part for displaying page content in page-templates/speaker-registration.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

    $prefix   = '_hpwp_';
    $pageid   = get_the_ID();
    $all_meta = get_meta( $pageid );

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'speakerreg' ); ?>>
	<section class="module module-white">
		<div class="container">

			<?php
				$hero_desktop = get_post_meta( get_the_ID(), $prefix . 'hero_img', 1 ) != '' ? get_post_meta( get_the_ID(), $prefix . 'hero_img', 1 ) : '';

				if( $hero_desktop == '' ) {

					the_title( '<h1>', '</h1>' );

                }

                while ( have_posts() ) {
                    the_post();
                    the_content();
                }

                $form_code = array_key_exists( $prefix . 'reg_form_shortcode', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'reg_form_shortcode'] ) : '';

                if($form_code != '') {
                    echo '<div class="form-stacked narrow">'.
                        $form_code .'
                    </div>';
                }
			?>

		</div>
	</section>
</div>
<?php
	/**
	 * Template part for displaying page content in page-templates/speaker.php
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 *
	 * @package hpwp_v2
	 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'speakerpage' ); ?>>
    <?php
        if(array_key_exists($prefix . 'banner_txt', $all_meta)) {
            $banner_url = array();

            if(array_key_exists($prefix . 'banner_url', $all_meta)) {
                $banner_url = array(
                    '<a href="'. $all_meta[$prefix . 'banner_url'] .'">',
                    '</a>',
                );
            }

            echo '<div class="speakerpage_banner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">'.
                            $banner_url[0] .
                            $all_meta[$prefix . 'banner_txt'] .
                            $banner_url[1] .'
                        </div>
                    </div>
                </div>
            </div>';
        }

        /**
         * speakers section
         */
    ?>
    <section class="module module-light">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        $speaking_section_title = array_key_exists( $prefix . 'speaking_section_title', $all_meta ) ? $all_meta[$prefix . 'speaking_section_title'] : '';

                        if( $speaking_section_title != '' ) {
                            echo '<h2>'. $speaking_section_title .'</h2>';
                        }

                        $speaking_section_intro = array_key_exists( $prefix . 'speaking_section_intro', $all_meta ) ? apply_filters('the_content', $all_meta[$prefix . 'speaking_section_intro']) : '';

                        if($speaking_section_intro != '') {
                            echo $speaking_section_intro;
                        }
                    ?>
                </div>
            </div>

			<div class="row">
                <?php
                    for($i = 1; $i < 3; $i++) {
                        ${'speaker-' . $i} = array_key_exists( $prefix . 'speaker'. $i .'_name', $all_meta ) ? explode(' ', $all_meta[$prefix . 'speaker'. $i .'_name']) : '';
                        $img = array_key_exists( $prefix . 'speaker'. $i .'_img', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_img'] : '';
                        $content = array_key_exists( $prefix . 'speaker'. $i .'_content', $all_meta ) ? apply_filters('the_content', $all_meta[$prefix . 'speaker'. $i .'_content']) : '';
                        $email = array_key_exists( $prefix . 'speaker'. $i .'_email', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_email'] : '';
                        $twitter = array_key_exists( $prefix . 'speaker'. $i .'_twitter', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_twitter'] : '';
                        $linkedin = array_key_exists( $prefix . 'speaker'. $i .'_linkedin', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_linkedin'] : '';
                        $facebook = array_key_exists( $prefix . 'speaker'. $i .'_facebook', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_facebook'] : '';
                        $designations = array_key_exists( $prefix . 'speaker'. $i .'_designations', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_designations'] : '';

                        echo '<div class="col-md-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3>'. ${'speaker-' . $i}[0] .' '. ${'speaker-' . $i}[1] .' '. $designations .'</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <img class="img-responsive" src="'. $img .'" alt="'. ${'speaker-' . $i}[0] .' '. ${'speaker-' . $i}[1] .' headshot.">

                                    <h4>Contact '. ${'speaker-' . $i}[0] .':</h4>
                                    <ul class="list-unstyled list-inline">
                                        <li>
                                            <a href="mailto:'. $email .'"><i class="icon-mail-alt"></i></a>
                                        </li>';

                                        if($twitter != '') {
                                            echo '<li>
                                                <a href="'. $twitter .'" target="_blank"><i class="icon-twitter"></i></a></li>
                                            </li>';
                                        }

                                        if($linkedin != '') {
                                            echo '<li>
                                                <a href="'. $linkedin .'" target="_blank"><i class="icon-linkedin-squared"></i></a></li>
                                            </li>';
                                        }

                                        if($facebook != '') {
                                            echo '<li>
                                                <a href="'. $facebook .'" target="_blank"><i class="icon-facebook-official"></i></a></li>
                                            </li>';
                                        }

                                    echo '</ul>
                                </div>
                                <div class="col-sm-8">'.
                                    $content .'
                                </div>
                            </div>
                        </div>';
                    }
                ?>
			</div>
		</div>
	</section>


    <?php
        /**
         * videos section
         */
    ?>
    <section class="module module-dark">
        <div class="container">
            <div class="row">
                <?php
                    for($i = 1; $i < 3; $i++) {
                        $vimeo_id = array_key_exists( $prefix . 'speaker'. $i .'_vimeo', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_vimeo'] : '';
                        $youtube_id = array_key_exists( $prefix . 'speaker'. $i .'_youtube', $all_meta ) ? $all_meta[$prefix . 'speaker'. $i .'_youtube'] : '';

                        echo '<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-0">
                            <h2>'. ${'speaker-'. $i}[0] .' '. ${'speaker-'. $i}[1] .'</h2>

                            <div class="embed-responsive embed-responsive-16by9 embed-margins-bottom">';

                                if($vimeo_id != '') {
                                    echo '<iframe class="embed-responsive-item" title="vimeo-player" src="https://player.vimeo.com/video/'. $vimeo_id .'" width="640" height="360" frameborder="0" allowfullscreen></iframe>';
                                } elseif($youtube_id != '') {
                                    echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'. $youtube_id .'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                                }

                            echo '</div>
                        </div>';
                    }
                ?>
            </div>
        </div>
    </section>


    <?php
        /**
         * book us cta
         */
    ?>
    <section class="module module-light">
        <div class="container">
            <div class="row">
                <?php
                    $book_us_content = array_key_exists( $prefix . 'book_us_content', $all_meta ) ? apply_filters('the_content', $all_meta[$prefix . 'book_us_content']) : '';
                    $book_us_txt = array_key_exists( $prefix . 'book_us_txt', $all_meta ) ? $all_meta[$prefix . 'book_us_txt'] : '';
                    $book_us_url = array_key_exists( $prefix . 'book_us_url', $all_meta ) ? $all_meta[$prefix . 'book_us_url'] : '';

                    if($book_us_url != '') {
                        echo '<div class="col-md-8 col-md-offset-2 text-center">'.
                            $book_us_content.'

                            <p>
                                <a class="btn btn-orange btn-lg" href="'. $book_us_url .'">'. $book_us_txt .'</a>
                            </p>
                        </div>';
                    }
                ?>
            </div>
        </div>
    </section>


    <?php
        /**
         * speaking events
         */
    ?>
    <section id="speaking-events" class="module module-white module-evenrow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        $speaking_events_title = array_key_exists( $prefix . 'speaking_events_title', $all_meta ) ? apply_filters( 'the_content', $all_meta[$prefix . 'speaking_events_title'] ) : '';

                        if( $speaking_events_title != '' ) {
                            echo '<h2 class="text-center">'. $speaking_events_title .'</h2>';
                        }
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <img class="img-responsive center-block margin-bottom-medium" src="<?php echo $all_meta[$prefix . 'book_tilt']; ?>">
                </div>

                <div class="col-sm-8">
                    <?php
                        $events = unserialize( $all_meta[$prefix . 'events'] );

                        if( $events != '' ) {

                            echo '<ul class="list-nopadding">';

                                foreach( $events as $key => $entry ) {

                                    $start_date = $entry['date_start'];
                                    $start_date_m = date('m', $start_date);
                                    $start_date_y = date('Y', $start_date);

                                    $end_date = $entry['date_end'];
                                    $end_date_m = date('m', $end_date);
                                    $end_date_y = date('Y', $end_date);

                                    $location_url = isset($entry['location_url']) ? $entry['location_url'] : '';

                                    if($end_date == '') {
                                        // single day
                                        $display_date = date('M. j, Y', $start_date);
                                    } elseif($start_date_y != $end_date_y) {
                                        // different years
                                        $display_date = date( 'M. j, Y', $start_date ) .' &ndash; '. date( 'M. j, Y', $end_date );
                                    } elseif( $start_date_m == $end_date_m ) {
                                        // same month
                                        $display_date = date( 'M. j', $start_date ) .' &ndash; '. date( 'j, Y', $end_date );
                                    } elseif( $start_date_m != $end_date_m ) {
                                        // different months
                                        $display_date = date( 'M. j', $start_date ) .' &ndash; '. date( 'M. j, Y', $end_date );
                                    }

                                    if($location_url != '') {
                                        echo '<li>'. $display_date .' &ndash; <a href="'. $location_url .'" target="_blank">'. $entry['location'] .'</a></li>';
                                    } else {
                                        echo '<li>'. $display_date .' &ndash; '. $entry['location'] .'</li>';
                                    }

                                }

                            echo '</ul>';

                        }
                    ?>
                </div>
            </div>
        </div>
    </section>


    <?php
        /**
         * book us cta
         */
    ?>
    <section class="module module-light">
        <div class="container">
            <div class="row">
                <?php
                    if($book_us_url != '') {
                        $book_us_content2 = array_key_exists( $prefix . 'book_us_content2', $all_meta ) ? apply_filters('the_content', $all_meta[$prefix . 'book_us_content2']) : '';

                        echo '<div class="col-md-8 col-md-offset-2 text-center">'.
                            $book_us_content2.'

                            <p>
                                <a class="btn btn-orange btn-lg" href="'. $book_us_url .'">'. $book_us_txt .'</a>
                            </p>
                        </div>';
                    }
                ?>
            </div>
        </div>
    </section>
</div>

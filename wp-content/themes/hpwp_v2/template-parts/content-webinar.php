<?php
/**
 * Template part for displaying page content for single webinars
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix			= '_hpwp_';
	$pageid			= get_the_ID();
	$all_meta		= get_meta( $pageid );

	$display_date	= get_display_date( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'eventcpt' ); ?>>
	<section class="module module-white">
		<div class="container">
			<div class="row">

				<?php if( get_post_meta( $pageid, $prefix . 'event_img_id', 1 ) !== '' ) { ?>

					<div class="col-md-4">
						<div class="tile-bg text-center text-mid" style="background-image: url( <?php echo wp_get_attachment_image_url( get_post_meta( $pageid, $prefix . 'event_img_id', 1 ), 'event-tiles' ); ?> );">
							<p class="text-bigger"><strong><?php echo $display_date; ?></strong></p>
							<p><?php echo get_the_title(); ?><br>
							<?php echo get_post_meta( $pageid, $prefix . 'event_location', 1 ); ?></p>
						</div>
					</div>
					<div class="col-md-8">
						<?php the_content(); ?>
					</div>

				<?php } else { ?>

					<div class="col-md-8 col-md-offset-2">
						<?php
							$display_date = get_display_date( $pageid );

							echo '<h2>'. get_the_title() .'</h2>';
							echo '<p class="text-bigger">'. $display_date .'</p>';

							echo '<div class="margin-top-medium">';
								the_content();
							echo '</div>';

						?>
					</div>

				<?php } ?>

			</div>

			<?php
				$form_code = hpwp_get_option( 'hpwp_webinar_reg_page_form' ) != '' ? apply_filters( 'the_content', hpwp_get_option( 'hpwp_webinar_reg_page_form' ) ) : '';

				if( $form_code ) {

					echo '<div class="row">
						<div class="form-stacked col-md-8 col-md-offset-2">'. $form_code .'</div>
					</div>';

				}
			?>
		</div>
	</section>
</div>

<?php
/**
 * Template part for displaying page content in page-templates/webinars.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hpwp_v2
 */

	$prefix		= '_hpwp_';
	$pageid		= get_the_ID();
	$all_meta	= get_meta( $pageid );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'webinarspage' ); ?>>
	<?php
		$webinars_query		= hpwp_webinars_query();
		$webinars_query_empty	= array_filter( $webinars_query->posts );

		echo '<section class="module module-light">
			<div class="container">
				<h2 class="text-center">'. $all_meta[$prefix . 'event_dates_title'] .'</h2>
				<p class="text-center margin-bottom-medium">'. $all_meta[$prefix . 'subtitle'] .'</p>';

				if( !empty( $webinars_query_empty ) ) {

					hpwp_get_webinars( $webinars_query );

				} else {

					echo '<p class="text-center">There are no scheduled webinars at this time.</p>';

				}

			echo '</div>
		</section>';
	?>

</div>
